﻿/*  ----------------------------------------------------------------------------
 *  Ambros Markus Ing.
 *  ----------------------------------------------------------------------------
 *  Draft Purpose for PDMS Design
 *  ----------------------------------------------------------------------------
 *  File:           DraPurpAddin.cs
 *  Author:         Markus Ambros
 *  Initial Date:   01.06.2010
 *  ----------------------------------------------------------------------------
 *  Version:        PDMS 12 SP5.2
 *  Namespace:      Ami.Design.DraPurpAddin
 *  ----------------------------------------------------------------------------
 */

using System;
using System.Text;
using Aveva.ApplicationFramework;
using Aveva.ApplicationFramework.Presentation;

namespace Ami.Design.PipeAtt
{
    public class PipeAttAddin : IAddin
    {
        private DockedWindow PipeAttWindow;
        private PipeAttControl PipeAttControl;

        #region IAddin Members

        void Aveva.ApplicationFramework.IAddin.Stop()
        {

        }

        public string Description
        {
            get
            {
                return "Provides the Pipe Attribute Addin";
            }
        }

        public string Name
        {
            get
            {
                return "PipeAttAddin";
            }
        }

        public void Start(ServiceManager serviceManager)
        {
            /// <summary>
            // Create Addins Windows
            // Get the WindowManager service
            /// </summary>
            /// 
            WindowManager windowManager = (WindowManager)serviceManager.GetService(typeof(WindowManager));
            PipeAttControl = new PipeAttControl();

            // Create a docked window
            PipeAttWindow = windowManager.CreateDockedWindow("Ami.PipeAtt.List", "Pipe Attributes", PipeAttControl, DockedPosition.Right);
            PipeAttWindow.Width = 400;

            // Docked windows created at addin start should ensure their layout is saved between sessions.
            PipeAttWindow.SaveLayout = true;

            // Create and register addins commands
            // Get the CommandManager
            CommandManager commandManager = (CommandManager)serviceManager.GetService(typeof(CommandManager));
            ShowPipeAttCommand showCommand = new ShowPipeAttCommand(PipeAttWindow);
            commandManager.Commands.Add(showCommand);

            // Load a UIC file for the DraPurpAddin.
            //CommandBarManager commandBarManager = (CommandBarManager)serviceManager.GetService(typeof(CommandBarManager));
            //commandBarManager.AddUICustomizationFile("DraPurpAddin.uic", "DraPurpAddin");
        }
        #endregion
    }
}






