﻿/*  ----------------------------------------------------------------------------
 *  Ambros Markus Ing.
 *  ----------------------------------------------------------------------------
 *  Draft Purpose for PDMS Design
 *  ----------------------------------------------------------------------------
 *  File:           DraPurpControl.cs
 *  Author:         Markus Ambros
 *  Initial Date:   01.06.2010
 *  ----------------------------------------------------------------------------
 *  Version:        PDMS 12 SP5.2
 *  Namespace:      Ami.Design.DraPurpAddin
 *  ----------------------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Aveva.ApplicationFramework;
using Aveva.ApplicationFramework.Presentation;
using Aveva.Pdms.Shared;
using Aveva.Pdms.Utilities.Messaging;
using Aveva.Pdms.Database;
using Aveva.PDMS.Database.Filters;
using ATT = Aveva.Pdms.Database.DbAttributeInstance;
using NOUN = Aveva.Pdms.Database.DbElementTypeInstance;
using Ps = Aveva.Pdms.Database.DbPseudoAttribute;


namespace Ami.Design.PipeAtt
{
    public partial class PipeAttControl : UserControl
    {
        public PipeAttControl()
        {
            InitializeComponent();
        }
    }
}
