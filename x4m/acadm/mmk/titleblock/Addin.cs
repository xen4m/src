﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using System.Xml;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;



using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows;
using Autodesk.AutoCAD.Ribbon;
using Autodesk.Windows;
using Autodesk.AutoCAD.GraphicsInterface;




using AcAp = Autodesk.AutoCAD.ApplicationServices.Application;


namespace titleblock
{

    public class Images
    {
        public static BitmapImage getBitmap(Bitmap image)
        {
            MemoryStream stream = new MemoryStream();
            image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = stream;
            bmp.EndInit();

            return bmp;
        }

    }

    public class Addin
    {


        [CommandMethod("TITLEBLOCK")]
        public void Test()
        {
            System.Windows.MessageBox.Show("TEST MessageBox");
        }
    }

    public class Ribbon
    {

        public RibbonCombo pan1ribcombo1 = new RibbonCombo();
        public RibbonCombo pan3ribcombo = new RibbonCombo();


        //public Ribbon()
        //{
        //    pan3ribcombo.CurrentChanged += new EventHandler<RibbonPropertyChangedEventArgs>(pan3ribcombo_CurrentChanged);
        //}

        //private void pan3ribcombo_CurrentChanged(object sender, RibbonPropertyChangedEventArgs e)
        //{
        //    RibbonButton but = pan3ribcombo.Current as RibbonButton;
        //    AcAp.ShowAlertDialog(but.Text);
        //}

        //[CommandMethod("MyRibbonCombotest")]
        //public void RibbonSplitButtontest()
        //{
        //    RibbonButton commandlinebutton = new RibbonButton();
        //    commandlinebutton.Text = "Newly Added Button with command: MyRibbonTestCombo";
        //    commandlinebutton.ShowText = true;
        //    commandlinebutton.ShowImage = true;
        //    commandlinebutton.Image = Images.getBitmap(Properties.Resources.Small);
        //    commandlinebutton.LargeImage = Images.getBitmap(Properties.Resources.Large);
        //    commandlinebutton.CommandHandler = new RibbonCommandHandler();
        //    pan1ribcombo1.Items.Add(commandlinebutton);
        //    pan1ribcombo1.Current = commandlinebutton;
        //}



        [CommandMethod("MyRibbon")]
        public void MyRibbon()
        {
            Autodesk.Windows.RibbonControl ribbonControl = Autodesk.Windows.ComponentManager.Ribbon;
            RibbonTab Tab = new RibbonTab();
            Tab.Title = "Autodesk .NET forum Ribbon Sample";
            Tab.Id = "RibbonSample_TAB_ID";

            ribbonControl.Tabs.Add(Tab);

            // create Ribbon panels
            Autodesk.Windows.RibbonPanelSource panel1Panel = new RibbonPanelSource();
            panel1Panel.Title = "Panel1";
            RibbonPanel Panel1 = new RibbonPanel();
            Panel1.Source = panel1Panel;
            Tab.Panels.Add(Panel1);

            RibbonButton pan1button1 = new RibbonButton();
            pan1button1.Text = "Button1";
            pan1button1.ShowText = true;
            pan1button1.ShowImage = true;
            pan1button1.Image = Images.getBitmap(Properties.Resources.Small);
            pan1button1.LargeImage = Images.getBitmap(Properties.Resources.Large);
            pan1button1.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan1button1.Size = RibbonItemSize.Large;
            pan1button1.CommandHandler = new RibbonCommandHandler();

            RibbonButton pan1button2 = new RibbonButton();
            pan1button2.Text = "Button2";
            pan1button2.ShowText = true;
            pan1button2.ShowImage = true;
            pan1button2.Image = Images.getBitmap(Properties.Resources.Small);
            pan1button2.LargeImage = Images.getBitmap(Properties.Resources.Large);
            pan1button2.CommandHandler = new RibbonCommandHandler();

            RibbonButton pan1button3 = new RibbonButton();
            pan1button3.Text = "Button3";
            pan1button3.ShowText = true;
            pan1button3.ShowImage = true;
            pan1button3.Image = Images.getBitmap(Properties.Resources.Small);
            pan1button3.LargeImage = Images.getBitmap(Properties.Resources.Large);
            pan1button3.CommandHandler = new RibbonCommandHandler();

            // Set te propperties for the RibbonCombo
            // Te ribboncombo control does not listen to the command handler
            pan1ribcombo1.Text = " ";
            pan1ribcombo1.ShowText = true;
            pan1ribcombo1.MinWidth = 150;

            RibbonRowPanel pan1row1 = new RibbonRowPanel();
            pan1row1.Items.Add(pan1button2);
            pan1row1.Items.Add(new RibbonRowBreak());
            pan1row1.Items.Add(pan1button3);
            pan1row1.Items.Add(new RibbonRowBreak());
            pan1row1.Items.Add(pan1ribcombo1);

            panel1Panel.Items.Add(pan1button1);
            panel1Panel.Items.Add(new RibbonSeparator());
            panel1Panel.Items.Add(pan1row1);

            //RibbonPanelSource panel2Panel = new RibbonPanelSource();
            //panel2Panel.Title = "Panel2";
            //RibbonPanel panel2 = new RibbonPanel();
            //panel2.Source = panel2Panel;
            //Tab.Panels.Add(panel2);

            //RibbonSplitButton pan2splitButton = new RibbonSplitButton();
            //pan2splitButton.Text = "SplitButton"; //Required not to crash AutoCAD when using cmd locators 
            //pan2splitButton.CommandHandler = new RibbonCommandHandler();
            //pan2splitButton.ShowText = true;
            //pan2splitButton.ShowImage = true;
            //pan2splitButton.Image = Images.getBitmap(Properties.Resources.Small);
            //pan2splitButton.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan2splitButton.IsSplit = true;
            //pan2splitButton.Size = RibbonItemSize.Large;

            //RibbonButton pan2button1 = new RibbonButton();
            //pan2button1.Text = "Button1";
            //pan2button1.ShowText = true;
            //pan2button1.ShowImage = true;
            //pan2button1.Image = Images.getBitmap(Properties.Resources.Small);
            //pan2button1.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan2button1.Size = RibbonItemSize.Large;
            //pan2button1.Orientation = System.Windows.Controls.Orientation.Vertical;
            //pan2button1.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan2button2 = new RibbonButton();
            //pan2button2.Text = "Button2";
            //pan2button2.ShowText = true;
            //pan2button2.ShowImage = true;
            //pan2button2.Image = Images.getBitmap(Properties.Resources.Small);
            //pan2button2.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan2button2.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan2button3 = new RibbonButton();
            //pan2button3.Text = "Button3";
            //pan2button3.ShowText = true;
            //pan2button3.ShowImage = true;
            //pan2button3.Image = Images.getBitmap(Properties.Resources.Small);
            //pan2button3.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan2button3.CommandHandler = new RibbonCommandHandler();

            //pan2splitButton.Items.Add(pan2button1);
            //pan2splitButton.Items.Add(pan2button2);

            //RibbonRowPanel pan2row1 = new RibbonRowPanel();
            //pan2row1.Items.Add(pan2button2);
            //pan2row1.Items.Add(new RibbonRowBreak());
            //pan2row1.Items.Add(pan2button3);
            //pan2row1.Items.Add(new RibbonRowBreak());
            //pan2row1.Items.Add(new RibbonCombo());

            //panel2Panel.Items.Add(pan2splitButton);
            //panel2Panel.Items.Add(pan2row1);

            //RibbonPanelSource panel3Panel = new RibbonPanelSource();
            //panel3Panel.Title = "Panel3";
            //RibbonPanel panel3 = new RibbonPanel();
            //panel3.Source = panel3Panel;
            //Tab.Panels.Add(panel3);

            //RibbonButton pan3button1 = new RibbonButton();
            //pan3button1.Text = "Button1";
            //pan3button1.ShowText = true;
            //pan3button1.ShowImage = true;
            //pan3button1.Image = Images.getBitmap(Properties.Resources.Small);
            //pan3button1.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan3button1.Size = RibbonItemSize.Large;
            //pan3button1.Orientation = System.Windows.Controls.Orientation.Vertical;
            //pan3button1.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan3button2 = new RibbonButton();
            //pan3button2.Text = "Button2";
            //pan3button2.ShowText = true;
            //pan3button2.ShowImage = true;
            //pan3button2.Image = Images.getBitmap(Properties.Resources.Small);
            //pan3button2.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan3button2.Size = RibbonItemSize.Large;
            //pan3button2.Orientation = System.Windows.Controls.Orientation.Vertical;
            //pan3button2.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan3button3 = new RibbonButton();
            //pan3button3.Text = "Button3";
            //pan3button3.ShowText = true;
            //pan3button3.ShowImage = true;
            //pan3button3.Image = Images.getBitmap(Properties.Resources.Small);
            //pan3button3.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan3button3.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan3button4 = new RibbonButton();
            //pan3button4.Text = "Button4";
            //pan3button4.ShowText = true;
            //pan3button4.ShowImage = true;
            //pan3button4.Image = Images.getBitmap(Properties.Resources.Small);
            //pan3button4.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan3button4.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan3button5 = new RibbonButton();
            //pan3button5.Text = "Button5";
            //pan3button5.ShowText = true;
            //pan3button5.ShowImage = true;
            //pan3button5.Image = Images.getBitmap(Properties.Resources.Small);
            //pan3button5.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan3button5.CommandHandler = new RibbonCommandHandler();

            //RibbonRowPanel pan3row1 = new RibbonRowPanel();
            //pan3row1.Items.Add(pan3button3);
            //pan3row1.Items.Add(new RibbonRowBreak());
            //pan3row1.Items.Add(pan3button4);
            //pan3row1.Items.Add(new RibbonRowBreak());
            //pan3row1.Items.Add(pan3button5);

            //panel3Panel.Items.Add(pan3button1);
            //panel3Panel.Items.Add(pan3row1);

            //RibbonPanelSource pan4Panel = new RibbonPanelSource();
            //pan4Panel.Title = "Panel4";
            //RibbonPanel Panel4 = new RibbonPanel();
            //Panel4.Source = pan4Panel;
            //Tab.Panels.Add(Panel4);

            //RibbonButton pan4button1 = new RibbonButton();
            //pan4button1.Text = "Button1";
            //pan4button1.ShowText = true;
            //pan4button1.ShowImage = true;
            //pan4button1.Image = Images.getBitmap(Properties.Resources.Small);
            //pan4button1.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan4button1.Size = RibbonItemSize.Large;
            //pan4button1.Orientation = System.Windows.Controls.Orientation.Vertical;
            //pan4button1.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan4button2 = new RibbonButton();
            //pan4button2.Text = "Button2";
            //pan4button2.ShowText = true;
            //pan4button2.ShowImage = true;
            //pan4button2.Image = Images.getBitmap(Properties.Resources.Small);
            //pan4button2.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan4button2.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan4button3 = new RibbonButton();
            //pan4button3.Text = "Button3";
            //pan4button3.ShowText = true;
            //pan4button3.ShowImage = true;
            //pan4button3.Image = Images.getBitmap(Properties.Resources.Small);
            //pan4button3.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan4button3.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan4button4 = new RibbonButton();
            //pan4button4.Text = "Button4";
            //pan4button4.ShowText = true;
            //pan4button4.ShowImage = true;
            //pan4button4.Image = Images.getBitmap(Properties.Resources.Small);
            //pan4button4.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan4button4.Size = RibbonItemSize.Large;
            //pan4button4.Orientation = System.Windows.Controls.Orientation.Vertical;
            //pan4button4.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan4ribcombobutton1 = new RibbonButton();
            //pan4ribcombobutton1.Text = "Button1";
            //pan4ribcombobutton1.ShowText = true;
            //pan4ribcombobutton1.ShowImage = true;
            //pan4ribcombobutton1.Image = Images.getBitmap(Properties.Resources.Small);
            //pan4ribcombobutton1.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan4ribcombobutton1.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan4ribcombobutton2 = new RibbonButton();
            //pan4ribcombobutton2.Text = "Button2";
            //pan4ribcombobutton2.ShowText = true;
            //pan4ribcombobutton2.ShowImage = true;
            //pan4ribcombobutton2.Image = Images.getBitmap(Properties.Resources.Small);
            //pan4ribcombobutton2.LargeImage = Images.getBitmap(Properties.Resources.Large);
            //pan4ribcombobutton2.CommandHandler = new RibbonCommandHandler();

            //pan3ribcombo.Width = 150;
            //pan3ribcombo.Items.Add(pan4ribcombobutton1);
            //pan3ribcombo.Items.Add(pan4ribcombobutton2);
            //pan3ribcombo.Current = pan4ribcombobutton1;

            //RibbonRowPanel vvorow1 = new RibbonRowPanel();
            //vvorow1.Items.Add(pan4button2);
            //vvorow1.Items.Add(new RibbonRowBreak());
            //vvorow1.Items.Add(pan4button3);
            //vvorow1.Items.Add(new RibbonRowBreak());
            //vvorow1.Items.Add(pan3ribcombo);

            //pan4Panel.Items.Add(pan4button1);
            //pan4Panel.Items.Add(vvorow1);
            //pan4Panel.Items.Add(new RibbonSeparator());
            //pan4Panel.Items.Add(pan4button4);

            Tab.IsActive = true;
        }

        public class RibbonCommandHandler : System.Windows.Input.ICommand
        {
            public bool CanExecute(object parameter)
            {
                return true;
            }

            public event EventHandler CanExecuteChanged;

            public void Execute(object parameter)
            {
                Document doc = AcAp.DocumentManager.MdiActiveDocument;

                if (parameter is RibbonButton)
                {
                    RibbonButton button = parameter as RibbonButton;
                    doc.Editor.WriteMessage("\nExecuted: " + button.Text + "\n");
                }
            }
        }
    }








}
