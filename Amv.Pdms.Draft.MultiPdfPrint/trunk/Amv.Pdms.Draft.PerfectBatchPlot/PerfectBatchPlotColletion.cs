﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//AVEVA related usings
using Aveva.Pdms.Database;
using Aveva.PDMS.Database.Filters;
using PML = Aveva.Pdms.Utilities.CommandLine;
using ATT = Aveva.Pdms.Database.DbAttributeInstance;
using NOUN = Aveva.Pdms.Database.DbElementTypeInstance;
using Ps = Aveva.Pdms.Database.DbPseudoAttribute;


namespace Amv.Pdms.Draft.PerfectBatchPlot
{
    class PerfectBatchPlotColletion
    {
        //public CollectedValues()
        //{
    
        //}
        
        
        
        
        public static PerfectBatchPlotColletion Instance = new PerfectBatchPlotColletion();

        //Create and iterate through collections

        public static void Run(DbElement CE, PerfectBatchPlotControl MultiPdfPrintControl)
        {
            MultiPdfPrintControl.DataGridClear();

            //Scan SHEEts below CE
            //:TITLENAME

            TypeFilter filter1 = new TypeFilter(NOUN.SHEET);
            TypeFilter filterNote = new TypeFilter(NOUN.NOTE);
            DBElementCollection collection = new DBElementCollection(CE, filter1);

            DbAttribute refNo = DbAttributeInstance.REF;
            DbAttribute name = DbAttributeInstance.NAME;
            DbAttribute namn = DbAttributeInstance.NAMN;

            DbAttribute udaDrwDescL1EN = DbAttribute.GetDbAttribute(":AES_DRW_DESCR_L1_EN");
            DbAttribute udaDrwDescL2EN = DbAttribute.GetDbAttribute(":AES_DRW_DESCR_L2_EN");
            DbAttribute udaRevision = DbAttribute.GetDbAttribute(":AES_REVISION");
            DbAttribute udaDrawingNumber = DbAttribute.GetDbAttribute(":AES_NAME");






            foreach (DbElement ele in collection)
            {
                string drawingNumber = "";
                string revision = "";
                string drwDescL1EN = "";
                string drwDescL2EN = "";

                DBElementCollection colNote = new DBElementCollection(ele, filterNote);
                DbAttribute nameNote = DbAttributeInstance.NAME;

                int i = 0;
                foreach (DbElement note in colNote)
                {

                    if (note.GetAsString(nameNote).Contains("/TITLEBLOCK"))
                    {
                        i++;
                        if (i == 1)
                        {
                            drawingNumber = note.GetAsString(udaDrawingNumber);
                            revision = note.GetAsString(udaRevision);
                            drwDescL1EN = note.GetAsString(udaDrwDescL1EN);
                            drwDescL2EN = note.GetAsString(udaDrwDescL2EN);
                            //MessageBox.Show("Note found");

                        }
                        else if (i > 1)
                        {


                            string CmdString = "$P More than one NOTE with name */TITLEBLOCK found.";
                            PML.Command PmlCommand = PML.Command.CreateCommand(CmdString);
                            PmlCommand.RunInPdms();

                        }
                        else
                        {



                            string CmdString = "$P No NOTE with name */TITLEBLOCK found.";
                            PML.Command PmlCommand = PML.Command.CreateCommand(CmdString);
                            PmlCommand.RunInPdms();

                        }
                    }
                }

                MultiPdfPrintControl.AddCollectedCE(ele.GetAsString(refNo), ele.GetAsString(name), ele.GetAsString(namn), drawingNumber, revision, drwDescL1EN, drwDescL2EN);
            }
        }
    }
}
