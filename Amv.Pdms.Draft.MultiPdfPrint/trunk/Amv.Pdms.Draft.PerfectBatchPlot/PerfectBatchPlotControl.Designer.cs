﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//AVEVA related usings
using Aveva.ApplicationFramework.Presentation;



namespace Amv.Pdms.Draft.PerfectBatchPlot
{
    partial class PerfectBatchPlotControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonGetCE = new System.Windows.Forms.Button();
            this.textBoxCE = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxConfigFile = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxDxfDwgConfiguration = new System.Windows.Forms.ComboBox();
            this.comboBoxDxfDwgFileType = new System.Windows.Forms.ComboBox();
            this.checkBoxOverwrite = new System.Windows.Forms.CheckBox();
            this.comboBoxOutputType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxOutputColor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSuffix = new System.Windows.Forms.TextBox();
            this.checkBoxSuffix = new System.Windows.Forms.CheckBox();
            this.textBoxPrefix = new System.Windows.Forms.TextBox();
            this.checkBoxPrefix = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonAddCE = new System.Windows.Forms.Button();
            this.buttonClearDataGridView = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gotoRefNoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportListToxlsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importListToxlsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonGetCE
            // 
            this.buttonGetCE.Location = new System.Drawing.Point(4, 4);
            this.buttonGetCE.Name = "buttonGetCE";
            this.buttonGetCE.Size = new System.Drawing.Size(53, 23);
            this.buttonGetCE.TabIndex = 0;
            this.buttonGetCE.Text = "Get CE";
            this.buttonGetCE.UseVisualStyleBackColor = true;
            this.buttonGetCE.Click += new System.EventHandler(this.buttonGetCE_Click);
            // 
            // textBoxCE
            // 
            this.textBoxCE.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCE.Location = new System.Drawing.Point(122, 6);
            this.textBoxCE.Name = "textBoxCE";
            this.textBoxCE.Size = new System.Drawing.Size(352, 20);
            this.textBoxCE.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Location = new System.Drawing.Point(4, 578);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Select All";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button3.Location = new System.Drawing.Point(84, 578);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "Unselect All";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button4.Location = new System.Drawing.Point(0, 819);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(156, 23);
            this.button4.TabIndex = 6;
            this.button4.Text = "Start";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.comboBoxConfigFile);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBoxDxfDwgConfiguration);
            this.groupBox1.Controls.Add(this.comboBoxDxfDwgFileType);
            this.groupBox1.Controls.Add(this.checkBoxOverwrite);
            this.groupBox1.Controls.Add(this.comboBoxOutputType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBoxOutputColor);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxSuffix);
            this.groupBox1.Controls.Add(this.checkBoxSuffix);
            this.groupBox1.Controls.Add(this.textBoxPrefix);
            this.groupBox1.Controls.Add(this.checkBoxPrefix);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Location = new System.Drawing.Point(3, 607);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(470, 206);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Output settings";
            // 
            // comboBoxConfigFile
            // 
            this.comboBoxConfigFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxConfigFile.Location = new System.Drawing.Point(82, 131);
            this.comboBoxConfigFile.Name = "comboBoxConfigFile";
            this.comboBoxConfigFile.Size = new System.Drawing.Size(156, 21);
            this.comboBoxConfigFile.TabIndex = 21;
            this.comboBoxConfigFile.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Config File";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Configuration";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "File type";
            // 
            // comboBoxDxfDwgConfiguration
            // 
            this.comboBoxDxfDwgConfiguration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDxfDwgConfiguration.Location = new System.Drawing.Point(81, 104);
            this.comboBoxDxfDwgConfiguration.Name = "comboBoxDxfDwgConfiguration";
            this.comboBoxDxfDwgConfiguration.Size = new System.Drawing.Size(156, 21);
            this.comboBoxDxfDwgConfiguration.TabIndex = 17;
            this.comboBoxDxfDwgConfiguration.TabStop = false;
            // 
            // comboBoxDxfDwgFileType
            // 
            this.comboBoxDxfDwgFileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDxfDwgFileType.Location = new System.Drawing.Point(81, 77);
            this.comboBoxDxfDwgFileType.Name = "comboBoxDxfDwgFileType";
            this.comboBoxDxfDwgFileType.Size = new System.Drawing.Size(156, 21);
            this.comboBoxDxfDwgFileType.TabIndex = 16;
            this.comboBoxDxfDwgFileType.TabStop = false;
            // 
            // checkBoxOverwrite
            // 
            this.checkBoxOverwrite.AutoSize = true;
            this.checkBoxOverwrite.Checked = true;
            this.checkBoxOverwrite.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxOverwrite.Location = new System.Drawing.Point(6, 183);
            this.checkBoxOverwrite.Name = "checkBoxOverwrite";
            this.checkBoxOverwrite.Size = new System.Drawing.Size(90, 17);
            this.checkBoxOverwrite.TabIndex = 15;
            this.checkBoxOverwrite.Text = "Overwrite File";
            this.checkBoxOverwrite.UseVisualStyleBackColor = true;
            // 
            // comboBoxOutputType
            // 
            this.comboBoxOutputType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOutputType.Location = new System.Drawing.Point(81, 23);
            this.comboBoxOutputType.Name = "comboBoxOutputType";
            this.comboBoxOutputType.Size = new System.Drawing.Size(156, 21);
            this.comboBoxOutputType.TabIndex = 14;
            this.comboBoxOutputType.TabStop = false;
            this.comboBoxOutputType.SelectedIndexChanged += new System.EventHandler(this.comboBoxOutputType_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Output type";
            // 
            // comboBoxOutputColor
            // 
            this.comboBoxOutputColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOutputColor.Location = new System.Drawing.Point(81, 50);
            this.comboBoxOutputColor.Name = "comboBoxOutputColor";
            this.comboBoxOutputColor.Size = new System.Drawing.Size(156, 21);
            this.comboBoxOutputColor.TabIndex = 11;
            this.comboBoxOutputColor.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Color";
            // 
            // textBoxSuffix
            // 
            this.textBoxSuffix.Location = new System.Drawing.Point(261, 99);
            this.textBoxSuffix.Name = "textBoxSuffix";
            this.textBoxSuffix.Size = new System.Drawing.Size(176, 20);
            this.textBoxSuffix.TabIndex = 12;
            // 
            // checkBoxSuffix
            // 
            this.checkBoxSuffix.AutoSize = true;
            this.checkBoxSuffix.Location = new System.Drawing.Point(261, 76);
            this.checkBoxSuffix.Name = "checkBoxSuffix";
            this.checkBoxSuffix.Size = new System.Drawing.Size(109, 17);
            this.checkBoxSuffix.TabIndex = 11;
            this.checkBoxSuffix.Text = "Suffix for filename";
            this.checkBoxSuffix.UseVisualStyleBackColor = true;
            this.checkBoxSuffix.CheckedChanged += new System.EventHandler(this.checkBoxSuffix_CheckedChanged);
            // 
            // textBoxPrefix
            // 
            this.textBoxPrefix.Location = new System.Drawing.Point(261, 50);
            this.textBoxPrefix.Name = "textBoxPrefix";
            this.textBoxPrefix.Size = new System.Drawing.Size(176, 20);
            this.textBoxPrefix.TabIndex = 10;
            // 
            // checkBoxPrefix
            // 
            this.checkBoxPrefix.AutoSize = true;
            this.checkBoxPrefix.Location = new System.Drawing.Point(261, 25);
            this.checkBoxPrefix.Name = "checkBoxPrefix";
            this.checkBoxPrefix.Size = new System.Drawing.Size(109, 17);
            this.checkBoxPrefix.TabIndex = 9;
            this.checkBoxPrefix.Text = "Prefix for filename";
            this.checkBoxPrefix.UseVisualStyleBackColor = true;
            this.checkBoxPrefix.CheckedChanged += new System.EventHandler(this.checkBoxPrefix_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(103, 183);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(103, 17);
            this.checkBox1.TabIndex = 7;
            this.checkBox1.Text = "Multi-Sheet PDF";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Location = new System.Drawing.Point(4, 33);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(469, 539);
            this.dataGridView1.TabIndex = 10;
            this.dataGridView1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_MouseDown);
            // 
            // buttonAddCE
            // 
            this.buttonAddCE.Location = new System.Drawing.Point(63, 4);
            this.buttonAddCE.Name = "buttonAddCE";
            this.buttonAddCE.Size = new System.Drawing.Size(53, 23);
            this.buttonAddCE.TabIndex = 11;
            this.buttonAddCE.Text = "Add CE";
            this.buttonAddCE.UseVisualStyleBackColor = true;
            this.buttonAddCE.Click += new System.EventHandler(this.buttonAddCE_Click);
            // 
            // buttonClearDataGridView
            // 
            this.buttonClearDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonClearDataGridView.Location = new System.Drawing.Point(165, 578);
            this.buttonClearDataGridView.Name = "buttonClearDataGridView";
            this.buttonClearDataGridView.Size = new System.Drawing.Size(75, 23);
            this.buttonClearDataGridView.TabIndex = 12;
            this.buttonClearDataGridView.Text = "Clear All";
            this.buttonClearDataGridView.UseVisualStyleBackColor = true;
            this.buttonClearDataGridView.Click += new System.EventHandler(this.buttonClearDataGridView_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gotoRefNoToolStripMenuItem,
            this.exportListToxlsToolStripMenuItem,
            this.importListToxlsToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(155, 70);
            // 
            // gotoRefNoToolStripMenuItem
            // 
            this.gotoRefNoToolStripMenuItem.Name = "gotoRefNoToolStripMenuItem";
            this.gotoRefNoToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.gotoRefNoToolStripMenuItem.Text = "Goto Sheet";
            this.gotoRefNoToolStripMenuItem.Click += new System.EventHandler(this.gotoRefNoToolStripMenuItem_Click);
            // 
            // exportListToxlsToolStripMenuItem
            // 
            this.exportListToxlsToolStripMenuItem.Name = "exportListToxlsToolStripMenuItem";
            this.exportListToxlsToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.exportListToxlsToolStripMenuItem.Text = "Export List";
            this.exportListToxlsToolStripMenuItem.Click += new System.EventHandler(this.exportListToxlsToolStripMenuItem_Click);
            // 
            // importListToxlsToolStripMenuItem
            // 
            this.importListToxlsToolStripMenuItem.Name = "importListToxlsToolStripMenuItem";
            this.importListToxlsToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.importListToxlsToolStripMenuItem.Text = "Import RefNo\'s";
            this.importListToxlsToolStripMenuItem.Visible = false;
            // 
            // MultiPdfPrintControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonAddCE);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonClearDataGridView);
            this.Controls.Add(this.textBoxCE);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.buttonGetCE);
            this.Name = "MultiPdfPrintControl";
            this.Size = new System.Drawing.Size(477, 845);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGetCE;
        private System.Windows.Forms.TextBox textBoxCE;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBoxSuffix;
        private System.Windows.Forms.CheckBox checkBoxPrefix;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxOverwrite;
        public System.Windows.Forms.ComboBox comboBoxOutputColor;
        public System.Windows.Forms.ComboBox comboBoxOutputType;
        public System.Windows.Forms.TextBox textBoxSuffix;
        public System.Windows.Forms.TextBox textBoxPrefix;
        private System.Windows.Forms.Button buttonAddCE;
        private System.Windows.Forms.Button buttonClearDataGridView;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exportListToxlsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importListToxlsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gotoRefNoToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox comboBoxDxfDwgConfiguration;
        public System.Windows.Forms.ComboBox comboBoxDxfDwgFileType;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.ComboBox comboBoxConfigFile;
    }
}
