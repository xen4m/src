﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Amv.Pdms.Draft.PerfectBatchPlot
{
    public class PerfectBatchPlotXml
    {
        public string OutputPath;
        public string OutputPrefix;
        public string OutputSuffix;

        public void ReadXmlFile(string XmlPath)
        {
        
            XmlReader reader = XmlReader.Create(XmlPath);

            while (reader.Read())
            {

                if (reader.NodeType == XmlNodeType.Entity && reader.Name == "output")
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "path")
                    {
                        Console.WriteLine("XML Output Path: " + reader.Value);
                        this.OutputPath = reader.Value;

                    }

                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "prefix")
                    {
                        Console.WriteLine("XML Prefix: " + reader.Value);
                        this.OutputPrefix = reader.Prefix;
                    }

                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "suffix")
                    {
                        Console.WriteLine("XML Suffix: " + reader.Value);
                        this.OutputSuffix = reader.Value;
                    }
                }
            }
        }

        public void WriteXmlFile()
        { 
        
        }
    }
}
