﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Reflection;

//AVEVA related usings
using Aveva.ApplicationFramework;
using Aveva.ApplicationFramework.Presentation;
using Aveva.Pdms.Shared;
using Aveva.Pdms.Utilities.Messaging;
using Aveva.Pdms.Database;
using Aveva.PDMS.Database.Filters;
using PML = Aveva.Pdms.Utilities.CommandLine;
using ATT = Aveva.Pdms.Database.DbAttributeInstance;
using NOUN = Aveva.Pdms.Database.DbElementTypeInstance;
using Ps = Aveva.Pdms.Database.DbPseudoAttribute;

//Excel related usings
using Microsoft.Office.Interop;
using System.Threading;     // For setting the Localization of the thread to fit
using System.Globalization; // the of the MS Excel localization, because of the MS bug



namespace Amv.Pdms.Draft.PerfectBatchPlot
{
    public partial class PerfectBatchPlotControl : UserControl
    {
        public string[,] DataArray = new string[1,2];

        public PerfectBatchPlotControl()
        {
            InitializeComponent();
            dataGridView1.DataSource = null;

            //dataGridView1.Columns.Add("RefNo","RefNo");
            dataGridView1.Columns.Add("sheetName", "Sheet Name");
            dataGridView1.Columns.Add("drawingNumber", "Drw. No.");
            dataGridView1.Columns.Add("revision", "Rev.");
            dataGridView1.Columns.Add("descL1EN", "Descr. L1 EN");
            dataGridView1.Columns.Add("descL2EN", "Descr. L2 EN");

            //dataGridView1.Columns.Add("Value", ":SHEE", "");

            comboBoxOutputType.Items.Add("PDF");
            comboBoxOutputType.Items.Add("TIF");
            comboBoxOutputType.Items.Add("EMF");
            comboBoxOutputType.Items.Add("DXF/DWG");
            comboBoxOutputType.SelectedIndex = 0;

            comboBoxOutputColor.Items.Add("COL BW");
            comboBoxOutputColor.Items.Add("COL GRAYSCALE");
            comboBoxOutputColor.Items.Add("COL COLOURPLUS");
            comboBoxOutputColor.SelectedIndex = 0;



            comboBoxDxfDwgFileType.Items.Add("DXF");
            comboBoxDxfDwgFileType.Items.Add("DWG");
            //comboBoxDxfDwgFileType.SelectedIndex = 0;

            //comboBoxDxfDwgConfiguration.Items.Clear();
            comboBoxDxfDwgConfiguration.Items.Add("Standard DXF");
            comboBoxDxfDwgConfiguration.Items.Add("Config. DXF");

            //comboBoxDxfDwgConfiguration.Items.Clear();

            comboBoxDxfDwgConfiguration.Items.Add("Config. DWG (Default)");
            comboBoxDxfDwgConfiguration.Items.Add("Config. DWG (v2000)");
            comboBoxDxfDwgConfiguration.Items.Add("Config. DWG (v2004)");
            comboBoxDxfDwgConfiguration.Items.Add("Config. DWG (v2007)");
            comboBoxDxfDwgConfiguration.Items.Add("Config. DWG (v2010)");
            //comboBoxDxfDwgConfiguration.SelectedIndex = 0;


            // ****************************************************************
            // Find all available .mac files in project folder "***mac"
            // -> Problem to solve: find automatic project path!!!!
            // ****************************************************************
            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            DirectoryInfo dir = new DirectoryInfo(assemblyFolder);
            //MessageBox.Show("The assembly path is: " + dir.ToString());

            //string searchKeyword = "eng";
            //string fileName = "Some file name here";
            //string[] textLines = File.ReadAllLines(fileName);
            //List<string> results = new List<string>();

            //foreach (string line in textLines)
            //{
            //    if (line.Contains(searchKeyword))
            //    {
            //        results.Add(line);
            //    }
            //}

            IEnumerable<System.IO.FileInfo> filelist = dir.GetFiles("*.mac");
            List<string> results = new List<string>();

            //foreach (var file in filelist)
            //{
            //    string searchKeyword = "dlicon";
            //    string filelist = file.ToString();
            //    string[] textLines = File.ReadAllLines(filelist);


            //    foreach (string line in textLines)
            //    {
            //        if (line.Contains(searchKeyword))
            //        {
            //            results.Add(line);
            //            MessageBox.Show(line);
            //        }
            //    }
            //}

            comboBoxConfigFile.DataSource = results;
            comboBoxConfigFile.SelectedIndex = -1;


            textBoxPrefix.Enabled = false;
            textBoxSuffix.Enabled = false;

            comboBoxDxfDwgConfiguration.Enabled = false;
            comboBoxDxfDwgFileType.Enabled = false;
            comboBoxOutputColor.Enabled = false;
            comboBoxConfigFile.Enabled = false;
        }







        public DbElement ceName;

        public void DataGridClear()
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.DataSource = null;
        }

        //============================================================
        //Public Booleans for options
        //============================================================
        public bool OptionOverwriteSelected
        {
            get { return checkBoxOverwrite.Checked; }
        }

        public bool OptionPrefixSelected
        {
            get { return checkBoxPrefix.Checked; }
        }

        public bool OptionSuffixSelected
        {
            get { return checkBoxSuffix.Checked; }
        }



        public void AddCollectedCE(string refno, string name, string namn, string value1, string value2, string value3, string value4)
        {
            // string namn not used at the moment
            //string refno not used at the moment
            int i = dataGridView1.RowCount;
                dataGridView1.Rows.Add(new object[] { name, value1, value2, value3, value4 });

        }

        public static DataSet MultidimensionalArrayToDataSet(string[,] input)
        {
            var dataSet = new DataSet();
            var dataTable = dataSet.Tables.Add();
            var iFila = input.GetLongLength(0);
            var iCol = input.GetLongLength(1);

            //Fila
            for (var f = 1; f < iFila; f++)
            {
                var row = dataTable.Rows.Add();
                //Columna
                for (var c = 0; c < iCol; c++)
                {
                    if (f == 1) dataTable.Columns.Add(input[0, c]);
                    row[c] = input[f, c];
                }
            }
            return dataSet;
        }

        private void buttonGetCE_Click(object sender, EventArgs e)
        {
            textBoxCE.Clear();
            this.dataGridView1.Rows.Clear();
            
            string ce = CurrentElement.Element.GetAsString(DbAttributeInstance.NAME);
            this.textBoxCE.Text = ce;
            ceName = CurrentElement.Element;
            
            PerfectBatchPlotColletion.Run(ceName, this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.dataGridView1.SelectAll();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.dataGridView1.ClearSelection();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            foreach(DataGridViewRow row in dataGridView1.SelectedRows)
            {
                //column index is 0 for first row (name of SHEEt)
                string name = row.Cells[0].Value.ToString();

                CommandLine command1 = new CommandLine();
                command1.DoCommandLine(name, this);
            }
        }

        private void checkBoxPrefix_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPrefix.Checked == true)
                textBoxPrefix.Enabled = true;
            else
                textBoxPrefix.Enabled = false;
        }

        private void checkBoxSuffix_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSuffix.Checked == true)
                textBoxSuffix.Enabled = true;
            else
                textBoxSuffix.Enabled = false;
        }

        private void buttonAddCE_Click(object sender, EventArgs e)
        {
            textBoxCE.Clear();
            //this.dataGridView1.Rows.Clear();

            string ce = CurrentElement.Element.GetAsString(DbAttributeInstance.NAME);
            this.textBoxCE.Text = ce;
            ceName = CurrentElement.Element;

            PerfectBatchPlotColletion.Run(ceName, this);
        }




        private void exportListToxlsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            //excelFileName = System.IO.Path.Combine(excelPath, "Ziperty Buy Model for Web 11_11_2011.xlsm");


            // In try catch finally Anweisung integrieren!!

            //MessageBox.Show("Start Export");
                // creating Excel Application 
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                //MessageBox.Show("Starting Excel");  
            app.Visible = false;
                // creating new WorkBook within Excel application 
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add();

                // creating new Excelsheet in workbook 
                Microsoft.Office.Interop.Excel._Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.ActiveSheet;
                //MessageBox.Show("Sheet selected");
                // see the excel sheet behind the program 
                

                // get the reference of first sheet. By default its name is Sheet1. 
                // store its reference to worksheet 

                worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets["Sheet1"];
                worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.ActiveSheet;


                //MessageBox.Show("Start Export 2");
                // changing the name of active sheet 
                worksheet.Name = "Exported from gridview";


                // storing header part in Excel 
                for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
                {
                    worksheet.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
                }


                // storing Each row and column value to excel sheet 

                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < dataGridView1.Columns.Count; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                    }
                }


                // save the application 
                workbook.SaveAs("c:\\temp\\output.xlsx", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);


                //// Exit from the application 
                //app.Quit();


                //                }
                //    else
                //        return;               

                //    // Exit from the application
                //    //app.Quit();
                //}
                //catch (System.Exception ex)
                //{

                //}
                //finally
                //{
                app.Quit();
                workbook = null;
                app = null;
                //MessageBox.Show("Export done!");
                if (MessageBox.Show("Export has been done! Open the list now?", "List Export", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start("c:\\temp\\output.xlsx");
                    }
                //} 


        }
     
        private void buttonClearDataGridView_Click(object sender, EventArgs e)
        {
            textBoxCE.Clear();
            this.dataGridView1.Rows.Clear();
        }


        private void dataGridView1_MouseDown(object sender, System.Windows.Forms.DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    //MessageBox.Show("Right mouse down!!");

                    contextMenuStrip1.Show(MousePosition);
                    //row.Selected = false;
                }
            }
        }

        private void gotoRefNoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            try
            {
                Int32 selectedRowCount = dataGridView1.Rows.GetRowCount(DataGridViewElementStates.Selected);
                
                //Check if only one row is selected
                if (selectedRowCount == 1)
                {
                    //Find out what refno is in current selected list
                    int rowIndex = dataGridView1.CurrentCell.RowIndex;
                    int cellIndex = 0;
                    
                    string CmdString = dataGridView1.Rows[rowIndex].Cells[cellIndex].Value.ToString();
                    //MessageBox.Show(CmdString);

                    //Goto selected refno
                    //PMLCommand pml = new PMLCommand();
                    //try
                    //{
                    //    pml.CreatePmlCommand(CmdString);
                    //}
                    //catch (System.Exception ex)
                    //{
                    //    Console.WriteLine("Error: {0}", ex.ToString());
                    //}

                    try
                    {
                        
                        PML.Command PmlCommand = PML.Command.CreateCommand(CmdString);
                        PmlCommand.RunInPdms();

                    }
                    catch (System.Exception ex)
                    {
                        Console.WriteLine("Error: {0}", ex.ToString());
                    }
                }
            }
            catch
            {
                MessageBox.Show("More than one row is selected. Cannot goto RefNo.");
            }
        }

        private void comboBoxOutputType_SelectedValueChanged(object sender, EventArgs e)
        {



            if (comboBoxOutputType.SelectedItem.ToString() == "PDF" |
                comboBoxOutputType.SelectedItem.ToString() == "TIF" |
                comboBoxOutputType.SelectedItem.ToString() == "EMF")
            {
                comboBoxOutputColor.Enabled = true;
                ///comboBoxOutputColor.SelectedIndex = 0;
                comboBoxDxfDwgFileType.Enabled = false;
                comboBoxDxfDwgConfiguration.Enabled = false;
                comboBoxConfigFile.Enabled = false;
            }
            else
            {
                comboBoxOutputColor.Enabled = false;
            }

            //MessageBox.Show("Selected value: " );
            if (comboBoxOutputType.SelectedItem.ToString() == "DXF/DWG")
            {
                comboBoxOutputColor.SelectedIndex = -1;
                comboBoxOutputColor.Enabled = false;

                comboBoxDxfDwgFileType.Enabled = true;
                comboBoxDxfDwgFileType.SelectedIndex = 1;

                comboBoxDxfDwgConfiguration.Enabled = true;
                comboBoxDxfDwgConfiguration.SelectedIndex = 2;

                comboBoxConfigFile.Enabled = true;
                comboBoxConfigFile.SelectedIndex = 0;
                //button4.Text = "Start DXF/DWG";
            }
            else
            {
                comboBoxDxfDwgFileType.Enabled = false;
                comboBoxDxfDwgConfiguration.Enabled = false;
                comboBoxConfigFile.Enabled = false;
                //button4.Text = "Start PDF";
            }




            //string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            //    public Form1()
            //{
            //    InitializeComponent();
            //}

            //private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
            //{

            //    var pfadZuDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            //    DirectoryInfo dir = new DirectoryInfo(pfadZuDesktop);

            //    IEnumerable<System.IO.FileInfo> fileList = dir.GetFiles("*.*");

            //comboBox1.DataSource = fileList;



        }




        //private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        //{
        //    if (e.Button == MouseButtons.Right)
        //    {
        //        //var ht = dataGridView1.HitTest(e.X, e.Y);
        //        // Place your condition HERE !!!
        //        // Currently it allow right click on last column only
        //        //if ((ht.ColumnIndex == dataGridView1.Columns.Count - 1)
        //        //     && (ht.Type == DataGridViewHitTestType.ColumnHeader))
        //        //{
        //            // This positions the menu at the mouse's location
        //        MessageBox.Show("Right mouse down!!");
        //            contextMenuStrip1.Show(MousePosition);
        //        //}
        //    }
        //}


        //private void dataGridView1_CellMouseDown(object sender, MouseEventArgs e)
        //{
        //    //if (e.Button == MouseButtons.Right)
        //    //{
        //    //    contextMenuStrip1.Show(dataGridView1, e.Location);
        //    //    //contextMenu.Show(datagridview, e.Location);
        //    //}

        //    // Load context menu on right mouse click
        //    DataGridView.HitTestInfo hitTestInfo;
        //    if (e.Button == MouseButtons.Right)
        //    {
        //        hitTestInfo = dataGridView1.HitTest(e.X, e.Y);
        //        // If column is first column
        //        //if (hitTestInfo.Type == DataGridViewHitTestType.Cell && hitTestInfo.ColumnIndex == 0)
        //            contextMenuStrip1.Show(dataGridView1, e.Location);
        //            //contextMenuForColumn1.Show(dataGridView1, new Point(e.X, e.Y));
        //        // If column is second column
        //        //if (hitTestInfo.Type == DataGridViewHitTestType.Cell && hitTestInfo.ColumnIndex == 1)
        //        //    contextMenuForColumn2.Show(dataGridView1, new Point(e.X, e.Y));
        //    }

        //} 


    }
}


                //Console.WriteLine("PLOT SHEE PDF " + row.Cells[i].Value.ToString());
                //MessageBox.Show("ttt " + row.Cells[i].Value.ToString());
                //CMD.CreateCommand("PLOT SHEE PDF " + filename + ".pdf");



                //Aveva.Pdms.Utilities.CommandLine.Command.CreateCommand("$P HALLO");
                //RunCMD("@C:\\AVEVA\\Plant\\Pdms12.0.SP6\\plot\\plot.exe PDF %file% -0 AUTO");
                //RunCMD("C:\\temp\\print.bat");
                // Aveva.Pdms.Utilities.CommandLine.Command.CreateCommand("PLOT SHEE");




                //RunCMD("C:\\AVEVA\\Plant\\Pdms12.0.SP6\\plot\\plot.exe", "screen *  -0 AUTO");

                //CreateCommand("PLOT SHEE PDF " + row.Cells[i].Value.ToString())




            //public static void RunCMD(string filename, string arguments)
            //{
            //ProcessStartInfo proc1 = new ProcessStartInfo();
            //string anyCommand;
            //proc1.UseShellExecute = true;

            //proc1.WorkingDirectory = @"C:\Windows\System32";

            //proc1.FileName = @"C:\Windows\System32\cmd.exe";
            //proc1.Verb = "runas";
            //proc1.Arguments = "/c " + anyCommand;
            //proc1.WindowStyle = ProcessWindowStyle.Hidden;
            //Process.Start(proc1);

            //System.Diagnostics.Process process = new System.Diagnostics.Process();

            //Process process = new Process();
            //System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //startInfo.FileName = "cmd.exe";
            //startInfo.Arguments = "/C copy /b Image1.jpg + Archive.rar Image2.jpg";
            //process.StartInfo = startInfo;
            //process.Start();

            //ProcessStartInfo startInfo = new ProcessStartInfo();
            //startInfo.FileName = @"C:\etc\Program Files\ProgramFolder\Program.exe";
            //startInfo.Arguments = @"C:\etc\desktop\file.spp C:\etc\desktop\file.txt";
            //Process.Start(startInfo);

            //ProcessStartInfo process1 = new ProcessStartInfo();
            //process1.FileName = filename;
            ////process1.UseShellExecute = true;
            ////process1.WorkingDirectory = @"C:\Windows\System32";
            ////process1.FileName = @"C:\Windows\System32\cmd.exe";
            ////process1.Verb = "runas";
            //process1.Arguments = arguments;
            ////process1.WindowStyle = ProcessWindowStyle.Hidden;
            //Process.Start(process1);


//public void ResizeArray(ref string[,] original, int cols, int rows)
//{
//    //create a new 2 dimensional array with
//    //the size we want
//    string[,] newArray = new string[cols, rows];
//    //copy the contents of the old array to the new one
//    Array.Copy(original, newArray, original.Length);
//    //set the original to the new array
//    original = newArray;
//}




