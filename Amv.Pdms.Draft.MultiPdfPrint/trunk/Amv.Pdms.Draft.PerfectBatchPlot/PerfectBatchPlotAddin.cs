﻿/*  ----------------------------------------------------------------------------
 *  Ambros Markus
 *  ----------------------------------------------------------------------------
 *  
 *  ----------------------------------------------------------------------------
 *  File:           MultiPdfPrintAddin.cs
 *  Author:         Markus Ambros
 *  Initial Date:   2014-XX-XX
 *  ----------------------------------------------------------------------------
 *  Version:        PDMS12.1SP4
 *  Namespace:      Andritz.Pdms.Draft.MultiPdfPrint
 *  ----------------------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;

//AVEVA related usings
using Aveva.ApplicationFramework;
using Aveva.ApplicationFramework.Presentation;
using System.Reflection;

namespace Amv.Pdms.Draft.PerfectBatchPlot
{
    public class PerfectBatchPlotAddin : IAddin
    {
        //private DockedWindow DraPurpWindow;
        //private DraPurpControl DraPurpControl;

        private DockedWindow PerfectBatchPlottWindow;
        private PerfectBatchPlotControl PerfectBatchPlotControl;

        #region IAddin Members

        void Aveva.ApplicationFramework.IAddin.Stop()
        {

        }

        public string Description
        {
            get
            {
                return "Provides the ... Addin";
            }
        }

        public string Name
        {
            get
            {
                return "PerfectBatchPlotAddin";
            }
        }

        public void Start(ServiceManager serviceManager)
        {
            /// <summary>
            // Create Addins Windows
            // Get the WindowManager service
            /// </summary>
            /// 
            

            WindowManager windowManager = (WindowManager)serviceManager.GetService(typeof(WindowManager));
            //Console.WriteLine("Addin: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

            string ver = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            Console.WriteLine("Addin Amv.Pdms.Draft.PerfectBatchPlot, Version {0} loaded", ver);
            PerfectBatchPlotControl = new PerfectBatchPlotControl();

            // Create a docked window

            PerfectBatchPlottWindow = windowManager.CreateDockedWindow("Amv.Pdms.Draft.PerfectBatchPlot", "Perfect Batch Plot", PerfectBatchPlotControl, DockedPosition.Right);
            PerfectBatchPlottWindow.Width = 400;

            // Docked windows created at addin start should ensure their layout is saved between sessions.
            PerfectBatchPlottWindow.SaveLayout = true;

            // Create and register addins commands
            // Get the CommandManager
            CommandManager commandManager = (CommandManager)serviceManager.GetService(typeof(CommandManager));
            ShowPerfectBatchPlotCommand showCommand = new ShowPerfectBatchPlotCommand(PerfectBatchPlottWindow);
            commandManager.Commands.Add(showCommand);



            //StatusBar statusBar = windowManager.StatusBar;
            //statusBar.Visible = true;
            //StatusBarTextPanel projectNamePanel = statusBar.Panels.AddTextPanel("Andritz.AddinText", "MultiPDFPrint");
            //statusBar.Progress = 0;
            //statusBar.ProgressText = "MultiPdfPrint active";


            // Load a UIC file.
            //CommandBarManager commandBarManager = (CommandBarManager)serviceManager.GetService(typeof(CommandBarManager));
            //commandBarManager.AddUICustomizationFile("DraPurpAddin.uic", "DraPurpAddin");




            //MultiPdfPrintXml xml = new MultiPdfPrintXml();
            //try
            //{
            //    xml.ReadXmlFile("Andritz.Pdms.Draft.MultiPdfPrint.xml");
            //}
            //catch
            //{
            //    //MessageBox.Show("Error on loading XML-File...");
            //    //Console.WriteLine("No XML-File available...");
            //}
            

        }
        #endregion   

    }
}
