﻿/*  ----------------------------------------------------------------------------
 *  Ambros Markus
 *  ----------------------------------------------------------------------------
 *  
 *  ----------------------------------------------------------------------------
 *  File:           MultiPdfPrintAddin.cs
 *  Author:         Markus Ambros
 *  Initial Date:   2014-XX-XX
 *  ----------------------------------------------------------------------------
 *  Version:        PDMS12.1SP4
 *  Namespace:      Andritz.Pdms.Draft.MultiPdfPrint
 *  ----------------------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;

//AVEVA related usings
using Aveva.ApplicationFramework;
using Aveva.ApplicationFramework.Presentation;
using System.Reflection;

namespace Pdmsat.Pdms.Draft.MultiPdfPrint
{
    public class MultiPdfPrintAddin : IAddin
    {
        //private DockedWindow DraPurpWindow;
        //private DraPurpControl DraPurpControl;

        private DockedWindow MultiPdfPrintWindow;
        private MultiPdfPrintControl MultiPdfPrintControl;

        #region IAddin Members

        void Aveva.ApplicationFramework.IAddin.Stop()
        {

        }

        public string Description
        {
            get
            {
                return "Provides the ... Addin";
            }
        }

        public string Name
        {
            get
            {
                return "MultiPdfPrintAddin";
            }
        }

        public void Start(ServiceManager serviceManager)
        {
            /// <summary>
            // Create Addins Windows
            // Get the WindowManager service
            /// </summary>
            /// 
            

            WindowManager windowManager = (WindowManager)serviceManager.GetService(typeof(WindowManager));
            //Console.WriteLine("Addin: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

            string ver = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            Console.WriteLine("Addin Pdmsat.Pdms.Draft.MultiPdfPrint, Version {0} loaded", ver);
            MultiPdfPrintControl = new MultiPdfPrintControl();

            // Create a docked window

            MultiPdfPrintWindow = windowManager.CreateDockedWindow("Pdmsat.Pdms.Draft.MultiPdfPrint", "Multi PDF Print", MultiPdfPrintControl, DockedPosition.Right);
            MultiPdfPrintWindow.Width = 400;

            // Docked windows created at addin start should ensure their layout is saved between sessions.
            MultiPdfPrintWindow.SaveLayout = true;

            // Create and register addins commands
            // Get the CommandManager
            CommandManager commandManager = (CommandManager)serviceManager.GetService(typeof(CommandManager));
            ShowMultiPdfPrintCommand showCommand = new ShowMultiPdfPrintCommand(MultiPdfPrintWindow);
            commandManager.Commands.Add(showCommand);



            //StatusBar statusBar = windowManager.StatusBar;
            //statusBar.Visible = true;
            //StatusBarTextPanel projectNamePanel = statusBar.Panels.AddTextPanel("Andritz.AddinText", "MultiPDFPrint");
            //statusBar.Progress = 0;
            //statusBar.ProgressText = "MultiPdfPrint active";


            // Load a UIC file.
            //CommandBarManager commandBarManager = (CommandBarManager)serviceManager.GetService(typeof(CommandBarManager));
            //commandBarManager.AddUICustomizationFile("DraPurpAddin.uic", "DraPurpAddin");




            //MultiPdfPrintXml xml = new MultiPdfPrintXml();
            //try
            //{
            //    xml.ReadXmlFile("Andritz.Pdms.Draft.MultiPdfPrint.xml");
            //}
            //catch
            //{
            //    //MessageBox.Show("Error on loading XML-File...");
            //    //Console.WriteLine("No XML-File available...");
            //}
            

        }
        #endregion   

    }
}
