﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

//AVEVA related usings
using Aveva.ApplicationFramework.Presentation;

namespace Pdmsat.Pdms.Draft.MultiPdfPrint
{
    public class ShowMultiPdfPrintCommand : Command
    {
    /// <summary>
    /// Class to manage the visibility state of the DraPurp docked window
    /// This command should be associated with a StateButtonTool.
    /// </summary>
    /// 

        private DockedWindow _window;
        /// <summary>
        /// Constructor for ShowAttributeBrowserCommand
        /// </summary>
        /// <param name="window">The docked window whose visibilty state will be managed.</param>
        /// 



        public ShowMultiPdfPrintCommand(DockedWindow window)
        {
            // Set the command key
            this.Key = "Amv.ShowMultiPdfPrint";

            // Save the docked window
            _window = window;

            // Create an event handler for the window closed event
            _window.Closed += new EventHandler(_window_Closed);

            // Create an event handler for the WindowLayoutLoaded event
            WindowManager.Instance.WindowLayoutLoaded += new EventHandler(Instance_WindowLayoutLoaded);
        }

        void Instance_WindowLayoutLoaded(object sender, EventArgs e)
        {
            // Update the command state to match initial window visibility
            this.Checked = _window.Visible;
        }

        void _window_Closed(object sender, EventArgs e)
        {
            // Update the command state when the window is closed
            this.Checked = false;
        }

        /// <summary>
        /// Override the base class Execute method to show and hide the window
        /// </summary>
        public override void Execute()
        {
            if (this.Checked)
            {
                _window.Show();
            }
            else
            {
                _window.Hide();
            }
        }

    }
}

