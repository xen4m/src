﻿// (C) Copyright 2015 by  
//
using System;
using System.Text;
using System.IO;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows;
using System.Reflection;
using System.Xaml;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Windows;
using Autodesk.AutoCAD.Ribbon;
using Autodesk.Windows;

using acadApp = Autodesk.AutoCAD.ApplicationServices.Application;


// This line is not mandatory, but improves loading performances
[assembly: ExtensionApplication(typeof(Ami.AcadM.Tools.Plugin))]

namespace Ami.AcadM.Tools
{

    // This class is instantiated by AutoCAD once and kept alive for the 
    // duration of the session. If you don't do any one time initialization 
    // then you should remove this class.
    public static class ImgConversion
    {
        public static ImageSource ToImageSource(this Icon icon)
        {
            ImageSource imageSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(
                icon.Handle,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            return imageSource;
        }
    }



    public class Plugin : IExtensionApplication
    {

        //public static PaletteSet _ps = null;
        //static Icon myIcon = Properties.Resources.Aveva;
        
        public static void PdmsPalette()
        {
            PaletteSet _ps = null;
            Icon myIcon = Properties.Resources.Aveva;

            if (_ps == null)
            {
                _ps = new PaletteSet("Acad PDMS Tool");
                //_ps.Size = new Size(400, 600);
                _ps.DockEnabled = (DockSides)((int)DockSides.Left + (int)DockSides.Right);
                _ps.Icon = myIcon;

                UserControl1 uc1 = new UserControl1();
                UserControl2 uc2 = new UserControl2();

                _ps.AddVisual("PANEL", uc1);
                _ps.AddVisual("EXTRUSION", uc2);
            }
            //MessageBox.Show("hallo");

            _ps.KeepFocus = true;
            _ps.Visible = true;
        }




        //public RibbonCombo pan1ribcombo1 = new RibbonCombo();
        //public RibbonCombo pan3ribcombo = new RibbonCombo();

        //public void Ribbon()
        //{
        //    pan3ribcombo.CurrentChanged += new EventHandler<RibbonPropertyChangedEventArgs>(pan3ribcombo_CurrentChanged);
        //}

        //private void pan3ribcombo_CurrentChanged(object sender, RibbonPropertyChangedEventArgs e)
        //{
        //    RibbonButton but = pan3ribcombo.Current as RibbonButton;
        //    acadApp.ShowAlertDialog(but.Text);
        //}

        //[CommandMethod("MyRibbonCombotest")]
        //public void RibbonSplitButtontest()
        //{
        //    RibbonButton commandlinebutton = new RibbonButton();
        //    commandlinebutton.Text = "Newly Added Button with command: MyRibbonTestCombo";
        //    commandlinebutton.ShowText = true;
        //    commandlinebutton.ShowImage = true;
        //    commandlinebutton.Image = Images.getBitmap(Properties.Resources.Small);
        //    commandlinebutton.LargeImage = Images.getBitmap(Properties.Resources.large);
        //    commandlinebutton.CommandHandler = new RibbonCommandHandler();
        //    pan1ribcombo1.Items.Add(commandlinebutton);
        //    pan1ribcombo1.Current = commandlinebutton;
        //}

        //Icon large = Properties.Resources.large;
        //Icon small = Properties.Resources.small;



        [CommandMethod("testmyRibbon", CommandFlags.Transparent)]
        public void Testme()
        {
            RibbonControl ribbon = ComponentManager.Ribbon;
            if (ribbon != null)
            {
                RibbonTab rtab = ribbon.FindTab("TESTME");
                if (rtab != null)
                {
                    ribbon.Tabs.Remove(rtab);
                }
                rtab = new RibbonTab();
                rtab.Title = "TEST ME";
                rtab.Id = "Testing";
                //Add the Tab
                ribbon.Tabs.Add(rtab);
                addContent(rtab);
            }
        }




        static void addContent(RibbonTab rtab)
        {
            rtab.Panels.Add(AddOnePanel());
        }

        static RibbonPanel AddOnePanel()
        {
            RibbonButton rb;
            RibbonPanelSource rps = new RibbonPanelSource();
            rps.Title = "Test One";
            RibbonPanel rp = new RibbonPanel();
            rp.Source = rps;
            //Create a Command Item that the Dialog Launcher can use,
            // for this test it is just a place holder.
            RibbonButton rci = new RibbonButton();
            rci.Name = "TestCommand";
            //assign the Command Item to the DialgLauncher which auto-enables
            // the little button at the lower right of a Panel
            rps.DialogLauncher = rci;
            rb = new RibbonButton();
            rb.Name = "Test Button";
            rb.ShowText = true;
            rb.Text = "Test Button";
            //Add the Button to the Tab
            rps.Items.Add(rb);
            return rp;
        }

        public static RibbonCombo pan1ribcombo1 = new RibbonCombo();
        public static RibbonCombo pan3ribcombo = new RibbonCombo();
        

        //public Ribbon()
        //{
        //    pan3ribcombo.CurrentChanged +=new EventHandler<RibbonPropertyChangedEventArgs>(pan3r​ibcombo_CurrentChanged);
        //}

        private void pan3ribcombo_CurrentChanged(object sender, RibbonPropertyChangedEventArgs e)
        {
            RibbonButton but = pan3ribcombo.Current as RibbonButton;
            acadApp.ShowAlertDialog(but.Text);
        }
        
        //[CommandMethod("MyRibbonCombotest")]
        //public void RibbonSplitButtontest()
        //{
        //    RibbonButton commandlinebutton = new RibbonButton();
        //    commandlinebutton.Text = "Newly Added Button with command: MyRibbonTestCombo";
        //    commandlinebutton.ShowText = true;
        //    commandlinebutton.ShowImage = true;
        //    //commandlinebutton.Image = Images.getBitmap(Properties.Resources.Small);
        //    //commandlinebutton.LargeImage = Images.getBitmap(Properties.Resources.large);
        //    //commandlinebutton.CommandHandler = new RibbonCommandHandler();
        //    pan1ribcombo1.Items.Add(commandlinebutton);
        //    pan1ribcombo1.Current = commandlinebutton;
        //}







        public static void PdmsTestRibbon()
        {


            //Icon myIconSmall = Properties.Resources.small;
            //Icon myIconLarge = Properties.Resources.large;
            //IntPtr myHiconTest = Properties.Resources.g4744.GetHicon();
            //pan1button1.Image = ImgConversion.ToImageSource(myIconSmall);
            //pan1button1.LargeImage = ImgConversion.ToImageSource(myIconLarge);
            //pan1button1.Image = ImgConversion.ToImageSource(Icon.FromHandle(myHiconTest));
            //pan1button1.Image = Images.getBitmap(Properties.Resources.Small);
            //pan1button1.CommandHandler = new RibbonCommandHandler();

            Autodesk.Windows.RibbonControl ribbonControl = Autodesk.Windows.ComponentManager.Ribbon;
            RibbonTab Tab = new RibbonTab();
            Tab.Title = "PDMS Tools";
            Tab.Id = "RibbonPdms_TAB_ID";

            ribbonControl.Tabs.Add(Tab);



            //RibbonButton pan1button1 = new RibbonButton();
            //pan1button1.Text = "Extrusion";
            //pan1button1.ShowText = true;
            //pan1button1.ShowImage = true;
            //pan1button1.Image = Images.getBitmap(Properties.Resources.extrusion16x16);
            //pan1button1.LargeImage = Images.getBitmap(Properties.Resources.extrusion32x32);
            //pan1button1.Orientation = System.Windows.Controls.Orientation.Vertical;
            //pan1button1.Size = RibbonItemSize.Large;


            //RibbonButton pan1button2 = new RibbonButton();
            //pan1button2.Text = "Neg. Extrusion";
            //pan1button2.ShowText = true;
            //pan1button2.ShowImage = true;
            //pan1button2.Image = Images.getBitmap(Properties.Resources.nextrusion16x16);
            //pan1button2.LargeImage = Images.getBitmap(Properties.Resources.nextrusion32x32);
            ////pan1button2.CommandHandler = new RibbonCommandHandler();
            ////button.CommandHandler = new AdskCommandHandler("AddRibbonTab.dll", "AddRibbonTab.Command");

            //RibbonButton pan1button3 = new RibbonButton();
            //pan1button3.Text = "Revolution";
            //pan1button3.ShowText = true;
            //pan1button3.ShowImage = true;

            ////pan1button3.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan1button4 = new RibbonButton();
            //pan1button4.Text = "Neg. Revolution";
            //pan1button4.ShowText = true;
            //pan1button4.ShowImage = true;

            ////pan1button4.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan1button5 = new RibbonButton();
            //pan1button5.Text = "Floor";
            //pan1button5.ShowText = true;
            //pan1button5.ShowImage = true;

            ////pan1button5.CommandHandler = new RibbonCommandHandler();

            //RibbonButton pan1button6 = new RibbonButton();
            //pan1button6.Text = "Floor";
            //pan1button6.ShowText = true;
            //pan1button6.ShowImage = true;

            //pan1button6.CommandHandler = new RibbonCommandHandler();

            //    // Set te propperties for the RibbonCombo
            //    // Te ribboncombo control does not listen to the command handler
            //   pan1ribcombo1.Text = " ";
            //   pan1ribcombo1.ShowText = true;
            //  pan1ribcombo1.MinWidth = 150;

            //RibbonRowPanel pan1row1 = new RibbonRowPanel();
            //pan1row1.Items.Add(pan1button2);
            //pan1row1.Items.Add(new RibbonRowBreak());
            //pan1row1.Items.Add(pan1button3);
            //pan1row1.Items.Add(new RibbonRowBreak());
            //pan1row1.Items.Add(pan1ribcombo1);

            //panel1Panel.Items.Add(pan1button1);
            //panel1Panel.Items.Add(new RibbonSeparator());
            //panel1Panel.Items.Add(pan1row1);



            // create Ribbon panels "General"
            // ==================================================
            RibbonPanelSource panel1Panel = new RibbonPanelSource();
            panel1Panel.Title = "General";
            RibbonPanel Panel1 = new RibbonPanel();
            Panel1.Source = panel1Panel;
            Tab.Panels.Add(Panel1);

            RibbonSplitButton pan1splitButton10 = new RibbonSplitButton();
            pan1splitButton10.Text = "Measure"; //Required not to crash AutoCAD when using cmd locators 
            pan1splitButton10.CommandHandler = new RibbonCommandHandler();
            pan1splitButton10.ShowText = true;
            pan1splitButton10.ShowImage = true;
            pan1splitButton10.IsSplit = true;
            pan1splitButton10.IsSynchronizedWithCurrentItem = true;
            pan1splitButton10.Size = RibbonItemSize.Large;

            RibbonButton pan1button1 = new RibbonButton();
            pan1button1.Text = "Distance";
            pan1button1.CommandParameter = "MEASUREGEOM Distance ";
            pan1button1.ShowText = true;
            pan1button1.ShowImage = true;
            //pan1button1.Image = Images.getBitmap(Properties.Resources.extrusion16x16);
            pan1button1.LargeImage = Images.getBitmap(Properties.Resources.RCDATA_32_DIST);
            pan1button1.Size = RibbonItemSize.Large;
            pan1button1.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan1button1.CommandHandler = new RibbonCommandHandler();

            RibbonButton pan1button2 = new RibbonButton();
            pan1button2.Text = "Radius";
            pan1button2.CommandParameter = "MEASUREGEOM Radius ";
            pan1button2.ShowText = true;
            pan1button2.ShowImage = true;
            //pan1button2.Image = Images.getBitmap(Properties.Resources.extrusion16x16);
            pan1button2.LargeImage = Images.getBitmap(Properties.Resources.RCDATA_32_DIAM);
            pan1button2.Size = RibbonItemSize.Large;
            pan1button2.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan1button2.CommandHandler = new RibbonCommandHandler();

            RibbonButton pan1button3 = new RibbonButton();
            pan1button3.Text = "Angle";
            pan1button3.CommandParameter = "MEASUREGEOM Angle ";
            pan1button3.ShowText = true;
            pan1button3.ShowImage = true;
            //pan1button3.Image = Images.getBitmap(Properties.Resources.extrusion16x16);
            pan1button3.LargeImage = Images.getBitmap(Properties.Resources.RCDATA_32_ANGLE);
            pan1button3.Size = RibbonItemSize.Large;
            pan1button3.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan1button3.CommandHandler = new RibbonCommandHandler();

            RibbonButton pan1button4 = new RibbonButton();
            pan1button4.Text = "Area";
            pan1button4.CommandParameter = "MEASUREGEOM Area ";
            pan1button4.ShowText = true;
            pan1button4.ShowImage = true;
            //pan1button4.Image = Images.getBitmap(Properties.Resources.extrusion16x16);
            pan1button4.LargeImage = Images.getBitmap(Properties.Resources.RCDATA_32_ANGLE);
            pan1button4.Size = RibbonItemSize.Large;
            pan1button4.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan1button4.CommandHandler = new RibbonCommandHandler();

            pan1splitButton10.Items.Add(pan1button1);
            pan1splitButton10.Items.Add(pan1button2);
            pan1splitButton10.Items.Add(pan1button3);
            pan1splitButton10.Items.Add(pan1button4);

            RibbonRowPanel pan1row1 = new RibbonRowPanel();
            pan1row1.Items.Add(pan1splitButton10);
            panel1Panel.Items.Add(pan1row1);



            // create Ribbon panels "Tools"
            // ==================================================
            RibbonPanelSource panel2Panel = new RibbonPanelSource();
            panel2Panel.Title = "Tools";
            RibbonPanel Panel2 = new RibbonPanel();
            Panel2.Source = panel2Panel;
            Tab.Panels.Add(Panel2);

            RibbonButton pan2button1 = new RibbonButton();
            pan2button1.Text = "Acad2PDMS";
            pan2button1.CommandParameter = "AAG_ACAD_2_PDMS ";
            pan2button1.ShowText = true;
            pan2button1.ShowImage = true;
            pan2button1.Image = Images.getBitmap(Properties.Resources.Acad2pdms);
            pan2button1.LargeImage = Images.getBitmap(Properties.Resources.Acad2pdms);
            pan2button1.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan2button1.Size = RibbonItemSize.Large;
            pan2button1.CommandHandler = new RibbonCommandHandler();

            RibbonRowPanel pan2row1 = new RibbonRowPanel();
            pan2row1.Items.Add(pan2button1);

            panel2Panel.Items.Add(pan2row1);


            //RibbonButton pan3button7 = new RibbonButton();
            //pan3button7.Text = "Panel";
            //pan3button7.CommandParameter = "PDMSEXPPANEL ";
            //pan3button7.ShowText = true;
            //pan3button7.ShowImage = true;
            //pan3button7.Image = Images.getBitmap(Properties.Resources.panel16x16);
            //pan3button7.LargeImage = Images.getBitmap(Properties.Resources.panel32x32);
            //pan3button7.Orientation = System.Windows.Controls.Orientation.Vertical;
            //pan3button7.Size = RibbonItemSize.Large;
            //pan3button7.CommandHandler = new RibbonCommandHandler();







            // create Ribbon panels "Export Polylines"
            // ==================================================
            RibbonPanelSource panel3Panel = new RibbonPanelSource();
            panel3Panel.Title = "Export Polylines";
            RibbonPanel panel3 = new RibbonPanel();
            panel3.Source = panel3Panel;
            Tab.Panels.Add(panel3);

            RibbonSplitButton pan3splitButton10 = new RibbonSplitButton();
            pan3splitButton10.Text = "SplitButton310"; //Required not to crash AutoCAD when using cmd locators 
            pan3splitButton10.CommandHandler = new RibbonCommandHandler();
            pan3splitButton10.ShowText = true;
            pan3splitButton10.ShowImage = true;
            pan3splitButton10.IsSplit = true;
            pan3splitButton10.Size = RibbonItemSize.Large;


            RibbonSplitButton pan3splitButton11 = new RibbonSplitButton();
            pan3splitButton11.Text = "SplitButton311"; //Required not to crash AutoCAD when using cmd locators 
            pan3splitButton11.CommandHandler = new RibbonCommandHandler();
            pan3splitButton11.ShowText = true;
            pan3splitButton11.ShowImage = true;
            pan3splitButton11.IsSplit = true;
            pan3splitButton11.Size = RibbonItemSize.Large;


            RibbonButton pan3button1 = new RibbonButton();
            pan3button1.Text = "Extrusion";
            pan3button1.CommandParameter = "PDMSEXPEXTR ";
            pan3button1.ShowText = true;
            pan3button1.ShowImage = true;
            pan3button1.Image = Images.getBitmap(Properties.Resources.extrusion16x16);
            pan3button1.LargeImage = Images.getBitmap(Properties.Resources.extrusion32x32);
            pan3button1.Size = RibbonItemSize.Large;
            pan3button1.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan3button1.CommandHandler = new RibbonCommandHandler();

            //Add custom ribbon tooltip
            Autodesk.Windows.RibbonToolTip pan3button1tt = new RibbonToolTip();
            pan3button1tt.Title = "Export extrusion";
            pan3button1tt.Content = "Exports a 2D-Polyline to .mac file.";
            pan3button1tt.ExpandedContent = "Make sure that all vertex points of the polyline has positiv values.";
            pan3button1.ToolTip = pan3button1tt;

            RibbonButton pan3button2 = new RibbonButton();
            pan3button2.Text = "Neg. Extrusion";
            pan3button2.CommandParameter = "PDMSEXPNEXTR ";
            pan3button2.ShowText = true;
            pan3button2.ShowImage = true;
            pan3button2.Image = Images.getBitmap(Properties.Resources.nextrusion16x16);
            pan3button2.LargeImage = Images.getBitmap(Properties.Resources.nextrusion32x32);
            pan3button2.Size = RibbonItemSize.Large;
            pan3button2.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan3button2.CommandHandler = new RibbonCommandHandler();

            //Add custom ribbon tooltip
            Autodesk.Windows.RibbonToolTip pan3button2tt = new RibbonToolTip();
            pan3button2tt.Title = "Export negative extrusion";
            pan3button2tt.Content = "Exports a 2D-Polyline to .mac file.";
            pan3button2tt.ExpandedContent = "Make sure that all vertex points of the polyline has positiv values.";
            pan3button2.ToolTip = pan3button2tt;



            RibbonButton pan3button3 = new RibbonButton();
            pan3button3.Text = "Revolution";
            pan3button3.CommandParameter = "PDMSEXPREVOL ";
            pan3button3.ShowText = true;
            pan3button3.ShowImage = true;
            pan3button3.Image = Images.getBitmap(Properties.Resources.revolution16x16);
            pan3button3.LargeImage = Images.getBitmap(Properties.Resources.revolution32x32);
            pan3button3.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan3button3.CommandHandler = new RibbonCommandHandler();

            //Add custom ribbon tooltip
            Autodesk.Windows.RibbonToolTip pan3button3tt = new RibbonToolTip();
            pan3button3tt.Title = "Export revolution";
            pan3button3tt.Content = "Exports a 2D-Polyline to .mac file.";
            pan3button3tt.ExpandedContent = "Make sure that all vertex points of the polyline has positiv values.";
            pan3button3.ToolTip = pan3button3tt;



            RibbonButton pan3button4 = new RibbonButton();
            pan3button4.Text = "Neg. Revolution";
            pan3button4.CommandParameter = "PDMSEXPNREVOL ";
            pan3button4.ShowText = true;
            pan3button4.ShowImage = true;
            pan3button4.Image = Images.getBitmap(Properties.Resources.nrevolution16x16);
            pan3button4.LargeImage = Images.getBitmap(Properties.Resources.nrevolution32x32);
            pan3button4.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan3button4.CommandHandler = new RibbonCommandHandler();

            //Add custom ribbon tooltip
            Autodesk.Windows.RibbonToolTip pan3button4tt = new RibbonToolTip();
            pan3button4tt.Title = "Export negative revolution";
            pan3button4tt.Content = "Exports a 2D-Polyline to .mac file.";
            pan3button4tt.ExpandedContent = "Make sure that all vertex points of the polyline has positiv values.";
            pan3button4.ToolTip = pan3button4tt;

            RibbonButton pan3button5 = new RibbonButton();
            pan3button5.Text = "Floor";
            pan3button5.CommandParameter = "PDMSEXPFLOOR ";
            pan3button5.ShowText = true;
            pan3button5.ShowImage = true;
            pan3button5.Image = Images.getBitmap(Properties.Resources.floor16x16);
            pan3button5.LargeImage = Images.getBitmap(Properties.Resources.floor32x32);
            pan3button5.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan3button5.Size = RibbonItemSize.Large;
            pan3button5.CommandHandler = new RibbonCommandHandler();

            //Add custom ribbon tooltip
            Autodesk.Windows.RibbonToolTip pan3button5tt = new RibbonToolTip();
            pan3button5tt.Title = "Export floor";
            pan3button5tt.Content = "Exports a 2D-Polyline to .mac file.";
            pan3button5tt.ExpandedContent = "Make sure that all vertex points of the polyline has positiv values.";
            pan3button5.ToolTip = pan3button5tt;

            //RibbonButton pan3button6 = new RibbonButton();
            //pan3button6.Text = "Neg. Floor";
            //pan3button6.CommandParameter = "PDMSEXPFLOOR";
            //pan3button6.ShowText = true;
            //pan3button6.ShowImage = true;
            //pan3button6.Image = Images.getBitmap(Properties.Resources.floor16x16);
            //pan3button6.LargeImage = Images.getBitmap(Properties.Resources.floor32x32);
            //pan3button6.Orientation = System.Windows.Controls.Orientation.Vertical;
            //pan3button6.CommandHandler = new RibbonCommandHandler();

            RibbonButton pan3button7 = new RibbonButton();
            pan3button7.Text = "Panel";
            pan3button7.CommandParameter = "PDMSEXPPANEL ";
            pan3button7.ShowText = true;
            pan3button7.ShowImage = true;
            pan3button7.Image = Images.getBitmap(Properties.Resources.panel16x16);
            pan3button7.LargeImage = Images.getBitmap(Properties.Resources.panel32x32);
            pan3button7.Orientation = System.Windows.Controls.Orientation.Vertical;
            pan3button7.Size = RibbonItemSize.Large;
            pan3button7.CommandHandler = new RibbonCommandHandler();

            //Add custom ribbon tooltip
            Autodesk.Windows.RibbonToolTip pan3button7tt = new RibbonToolTip();
            pan3button7tt.Title = "Export panel";
            pan3button7tt.Content = "Exports a 2D-Polyline to .mac file.";
            pan3button7tt.ExpandedContent = "Make sure that all vertex points of the polyline has positiv values.";
            pan3button7.ToolTip = pan3button7tt;


            pan3splitButton10.Items.Add(pan3button1);
            pan3splitButton10.Items.Add(pan3button2);

            pan3splitButton11.Items.Add(pan3button3);
            pan3splitButton11.Items.Add(pan3button4);


            RibbonRowPanel pan3row1 = new RibbonRowPanel();
            pan3row1.Items.Add(pan3button5);
            pan3row1.Items.Add(new RibbonSeparator());
            pan3row1.Items.Add(pan3splitButton10);
            pan3row1.Items.Add(new RibbonSeparator());
            pan3row1.Items.Add(pan3splitButton11);
            pan3row1.Items.Add(new RibbonSeparator());
            pan3row1.Items.Add(pan3button7);

            panel3Panel.Items.Add(pan3row1);
                                    

            //Tab.IsActive = true;
























        }


        public class RibbonCommandHandler : System.Windows.Input.ICommand
        {
            public bool CanExecute(object parameter)
            {
                return true;
            }

            public event EventHandler CanExecuteChanged;

            public void Execute(object parameter)
            {
                Document doc = acadApp.DocumentManager.MdiActiveDocument;

                if (parameter is RibbonButton)
                {
                    RibbonButton button = parameter as RibbonButton;
                    //doc.Editor.WriteMessage("\nRibbonButton Executed: " + button.Text + "\n");

                    //Execute command specified in ribbon button parameter
                    doc.SendStringToExecute((String)button.CommandParameter, true, false, true);


                }

            }
        }

        public class Images
        {
            public static BitmapImage getBitmap(Bitmap image)
            {
                MemoryStream stream = new MemoryStream();
                image.Save(stream, ImageFormat.Png);
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = stream;
                bmp.EndInit();

                return bmp;
            }
        }


        void IExtensionApplication.Initialize()
        {
            // Add one time initialization here
            // One common scenario is to setup a callback function here that 
            // unmanaged code can call. 
            // To do this:
            // 1. Export a function from unmanaged code that takes a function
            //    pointer and stores the passed in value in a global variable.
            // 2. Call this exported function in this function passing delegate.
            // 3. When unmanaged code needs the services of this managed module
            //    you simply call acrxLoadApp() and by the time acrxLoadApp 
            //    returns  global function pointer is initialized to point to
            //    the C# delegate.
            // For more info see: 
            // http://msdn2.microsoft.com/en-US/library/5zwkzwf4(VS.80).aspx
            // http://msdn2.microsoft.com/en-us/library/44ey4b32(VS.80).aspx
            // http://msdn2.microsoft.com/en-US/library/7esfatk4.aspx
            // as well as some of the existing AutoCAD managed apps.

            // Initialize your plug-in application here
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;

            settings.GetXmlSettings();

            string version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();


            ed.WriteMessage("(c)2015 Acad PDMS Tool loading, version " + version + "\n");
            ed.WriteMessage("Use PDMSEXPPANEL for Panel Export\n");
            ed.WriteMessage("Use PDMSEXPEXTR for Extrusion Export\n");
            ed.WriteMessage("Use PDMSEXPNEXTR for Negativ Extrusion Export\n");
            ed.WriteMessage("Use PDMSEXPFLOOR for Floor Export\n");
            ed.WriteMessage("Use PDMSEXPREVOL for Revolution Export\n");
            ed.WriteMessage("Use PDMSEXPNREVOL for Negative Revolution Export\n");
            //ed.WriteMessage("You can find the export file under temp-folder\n\n");




            //ed = appy.DocumentManager.MdiActiveDocument.Editor;
            //ed.WriteMessage("\n This is Created By ESOL Engineering Buruea \nType EXPS and hit Enter Key for using the tool");
            ////throw new NotImplementedException(); 
            //if (Autodesk.Windows.ComponentManager.Ribbon == null)
            //{
            //    //load the custom Ribbon on startup, but at this point
            //    //the Ribbon control is not available, so register for
            //    //an event and wait
            //    Autodesk.Windows.ComponentManager.ItemInitialized +=
            //        new EventHandler<RibbonItemEventArgs>
            //          (ComponentManager_ItemInitialized);
            //}
            //else
            //{
            //    //the assembly was loaded using NETLOAD, so the ribbon
            //    //is available and we just create the ribbon
            //    cr();
            //}

            if (Autodesk.Windows.ComponentManager.Ribbon == null)
            {
                //load the custom Ribbon on startup, but at this point
                //the Ribbon control is not available, so register for
                //an event and wait
                Autodesk.Windows.ComponentManager.ItemInitialized += new EventHandler<RibbonItemEventArgs>(ComponentManager_ItemInitialized);
            }
            else 
            {
                PdmsTestRibbon();
            }


            ed.WriteMessage("(c)2015 Acad PDMS Tool loaded\n");

        }

        private void ComponentManager_ItemInitialized(object sender, RibbonItemEventArgs e)
        {
            //now one Ribbon item is initialized, but the Ribbon control
            //may not be available yet, so check if before
            if (Autodesk.Windows.ComponentManager.Ribbon != null)
            {
                //ok, create Ribbon
                PdmsTestRibbon();
                //and remove the event handler
                Autodesk.Windows.ComponentManager.ItemInitialized -=
                    new EventHandler<RibbonItemEventArgs>
                      (ComponentManager_ItemInitialized);
            }

        }

        void IExtensionApplication.Terminate()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            // Do plug-in application clean up here
            ed.WriteMessage("(c) Acad PDMS Tools...Good Bye !\n");
        }

    }

}



//RibbonRowPanel pan3row2 = new RibbonRowPanel();

//pan3row1.Items.Add(pan3button3);
//pan3row2.Items.Add(new RibbonSeparator());
//pan3row1.Items.Add(pan3button4);
//pan3row2.Items.Add(new RibbonSeparator());
//pan3row1.Items.Add(pan3button5);
//panel3Panel.Items.Add(pan3row2);

//panel3Panel.Items.Add(pan3button1);
//panel3Panel.Items.Add(pan3button2);









//RibbonPanelSource pan4Panel = new RibbonPanelSource();
//pan4Panel.Title = "Panel4";
//RibbonPanel Panel4 = new RibbonPanel();
//Panel4.Source = pan4Panel;
//Tab.Panels.Add(Panel4);

//RibbonButton pan4button1 = new RibbonButton();
//pan4button1.Text = "Button1";
//pan4button1.ShowText = true;
//pan4button1.ShowImage = true;
////    pan4button1.Image = Images.getBitmap(Properties.Resources.Small);
////    pan4button1.LargeImage = Images.getBitmap(Properties.Resources.large);
//pan4button1.Size = RibbonItemSize.Large;
//pan4button1.Orientation = System.Windows.Controls.Orientation.Vertical;
//pan4button1.CommandHandler = new RibbonCommandHandler();

//RibbonButton pan4button2 = new RibbonButton();
//pan4button2.Text = "Button2";
//pan4button2.ShowText = true;
//pan4button2.ShowImage = true;
////    pan4button2.Image = Images.getBitmap(Properties.Resources.Small);
////    pan4button2.LargeImage = Images.getBitmap(Properties.Resources.large);
//pan4button2.CommandHandler = new RibbonCommandHandler();

//RibbonButton pan4button3 = new RibbonButton();
//pan4button3.Text = "Button3";
//pan4button3.ShowText = true;
//pan4button3.ShowImage = true;
////    pan4button3.Image = Images.getBitmap(Properties.Resources.Small);
////    pan4button3.LargeImage = Images.getBitmap(Properties.Resources.large);
//pan4button3.CommandHandler = new RibbonCommandHandler();

//RibbonButton pan4button4 = new RibbonButton();
//pan4button4.Text = "Button4";
//pan4button4.ShowText = true;
//pan4button4.ShowImage = true;
////    pan4button4.Image = Images.getBitmap(Properties.Resources.Small);
////    pan4button4.LargeImage = Images.getBitmap(Properties.Resources.large);
//pan4button4.Size = RibbonItemSize.Large;
//pan4button4.Orientation = System.Windows.Controls.Orientation.Vertical;
//pan4button4.CommandHandler = new RibbonCommandHandler();

//RibbonButton pan4ribcombobutton1 = new RibbonButton();
//pan4ribcombobutton1.Text = "Button1";
//pan4ribcombobutton1.ShowText = true;
//pan4ribcombobutton1.ShowImage = true;
////    pan4ribcombobutton1.Image = Images.getBitmap(Properties.Resources.Small);
////    pan4ribcombobutton1.LargeImage = Images.getBitmap(Properties.Resources.large);
//pan4ribcombobutton1.CommandHandler = new RibbonCommandHandler();

//RibbonButton pan4ribcombobutton2 = new RibbonButton();
//pan4ribcombobutton2.Text = "Button2";
//pan4ribcombobutton2.ShowText = true;
//pan4ribcombobutton2.ShowImage = true;
////    pan4ribcombobutton2.Image = Images.getBitmap(Properties.Resources.Small);
////    pan4ribcombobutton2.LargeImage = Images.getBitmap(Properties.Resources.large);
//pan4ribcombobutton2.CommandHandler = new RibbonCommandHandler();

//pan3ribcombo.Width = 150;
//pan3ribcombo.Items.Add(pan4ribcombobutton1);
//pan3ribcombo.Items.Add(pan4ribcombobutton2);
//pan3ribcombo.Current = pan4ribcombobutton1;

//RibbonRowPanel vvorow1 = new RibbonRowPanel();
//vvorow1.Items.Add(pan4button2);
//vvorow1.Items.Add(new RibbonRowBreak());
//vvorow1.Items.Add(pan4button3);
//vvorow1.Items.Add(new RibbonRowBreak());
//vvorow1.Items.Add(pan3ribcombo);

//pan4Panel.Items.Add(pan4button1);
//pan4Panel.Items.Add(vvorow1);
//pan4Panel.Items.Add(new RibbonSeparator());
//pan4Panel.Items.Add(pan4button4);                                   




//[CommandMethod("CreateMyGallery")]

//public void CreateMyGallery()
//{

//    RibbonControl ribbon = ComponentManager.Ribbon;



//    RibbonTab tab = new RibbonTab();

//    tab.Name = "MyTab";

//    tab.Title = "My Tab";

//    tab.Id = "MyTabId";

//    ribbon.Tabs.Add(tab);



//    RibbonPanelSource panelSrc = new RibbonPanelSource();

//    panelSrc.Name = "MyPanel";

//    panelSrc.Title = "My Panel";

//    panelSrc.Id = "MyPanelId";



//    RibbonGallery gallery = new RibbonGallery();

//    gallery.Name = "MyGallery";

//    gallery.Id = "MyGalleryId";



//    RibbonButton button1 = new RibbonButton();

//    button1.Name = "MyButton";

//    button1.Text = "My Button";

//    button1.Id = "MyButtonId";

//    gallery.Items.Add(button1);



//    RibbonButton button2 = new RibbonButton();

//    button2.Name = "MyOtherButton";

//    button2.Text = "My Other Button";

//    button2.Id = "MyOtherButtonId";

//    gallery.Items.Add(button2);



//    gallery.DisplayMode = GalleryDisplayMode.ComboBox;



//    panelSrc.Items.Add(gallery);



//    RibbonPanel panel = new RibbonPanel();

//    panel.Source = panelSrc;



//    tab.Panels.Add(panel);

//}
