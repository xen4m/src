﻿/*  ----------------------------------------------------------------------------
 *  Ambros Markus Ing.
 *  ----------------------------------------------------------------------------
 *  Draft Purpose for PDMS Design
 *  ----------------------------------------------------------------------------
 *  File:           TemplateAddin.cs
 *  Author:         Markus Ambros
 *  Initial Date:   14.02.2011
 *  ----------------------------------------------------------------------------
 *  Version:        PDMS 12 SP6.11
 *  Namespace:      Ami.Template
 *  ----------------------------------------------------------------------------
 */

using System;
using System.Text;
using Aveva.ApplicationFramework;
using Aveva.ApplicationFramework.Presentation;

namespace Ami.Template
{
    public class TemplateAddin : IAddin
    {
        private DockedWindow TemplateWindow;
        private TemplateControl Template;

        #region IAddin Members

        void Aveva.ApplicationFramework.IAddin.Stop()
        {

        }

        public string Description
        {
            get
            {
                return "Provides the Draft Purpose Addin";
            }
        }

        public string Name
        {
            get
            {
                return "DraPurpAddin";
            }
        }

        public void Start(ServiceManager serviceManager)
        {
            /// <summary>
            // Create Addins Windows
            // Get the WindowManager service
            /// </summary>
            /// 
            WindowManager windowManager = (WindowManager)serviceManager.GetService(typeof(WindowManager));
            Template = new TemplateControl();

            // Create a docked window
            TemplateWindow = windowManager.CreateDockedWindow("Ami.Template", "Template Window", Template, DockedPosition.Right);
            TemplateWindow.Width = 400;

            // Docked windows created at addin start should ensure their layout is saved between sessions.
            TemplateWindow.SaveLayout = true;

            // Create and register addins commands
            // Get the CommandManager
            CommandManager commandManager = (CommandManager)serviceManager.GetService(typeof(CommandManager));
            ShowTemplateCommand showCommand = new ShowTemplateCommand(TemplateWindow);
            commandManager.Commands.Add(showCommand);
        }
        #endregion
    }
}