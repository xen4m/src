﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataGridView1
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            dataGridView1.RowsDefaultCellStyle.BackColor = Color.LightBlue;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.RaisedHorizontal;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //public static DataTable GetTable()
            //DataTable dt = new DataTable();

            dataGridView1.DataSource = DataGridView1.Program.GetTable();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
        }




        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu m = new ContextMenu();
                m.MenuItems.Add(new MenuItem("Cut"));
                m.MenuItems.Add(new MenuItem("Copy"));
                m.MenuItems.Add(new MenuItem("Paste"));

                int currentMouseOverRow = dataGridView1.HitTest(e.X, e.Y).RowIndex;

                if (currentMouseOverRow >= 0)
                {
                    m.MenuItems.Add(new MenuItem(string.Format("Do something to row {0}", currentMouseOverRow.ToString())));
                }

                m.Show(dataGridView1, new Point(e.X, e.Y));

            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About.ActiveForm.Show();
        }

        private void dataGridView1_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            MessageBox.Show("TTT");
            
        }

        //private void dataGridView1_CellValueChanged(object sender, EventArgs e)
        //{
        //    MessageBox.Show("jajaja");
        //}

        //private void submitButton_Click(object sender, System.EventArgs e)
        //{
        //    // Update the database with the user's changes.
        //    dataAdapter.Update((DataTable)bindingSource1.DataSource);
        //}

    }
}
