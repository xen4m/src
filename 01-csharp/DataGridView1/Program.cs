﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace DataGridView1
{
    static class Program
    {
        public static DataTable GetTable()
        {
            DataTable dt = new DataTable();
            dt.Clear();

            dt.Columns.Add("Description Line 1", typeof(string));
            dt.Columns.Add("Description Line 2", typeof(string));
            dt.Columns.Add("Drawing No.", typeof(string));
            dt.Columns.Add("Revision", typeof(string));
            dt.Columns.Add("Date", typeof(string));

            dt.Rows.Add("Layout", "Groundfloor", "123456789", "A", "2014-01-01");
            dt.Rows.Add("Layout", "Machinefloor", "123456666", "-", "2012-07-05");
            dt.Rows.Add("Layout", "Groundfloor", "123444789", "B", "2014-05-01");
            dt.Rows.Add("Layout", "Groundfloor", "123333789", "C", "2014-10-10");


            return dt;
        }


       


        public static void FillData()
        { 
            
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            //DataSet set = new DataSet();
            //DataTable dt = set.Tables.Add("MyTable");

            //    dt.Columns.Add("Description Line 1", typeof(string));
            //    dt.Columns.Add("Description Line 2", typeof(string));
            //    dt.Columns.Add("Drawing No.", typeof(string));
            //    dt.Columns.Add("Revision", typeof(string));
            //    dt.Columns.Add("Date", typeof(string));

            //    dt.Rows.Add("Layout", "Groundfloor", "123456789", "A", "2014-01-01");
            //    dt.Rows.Add("Layout", "Machinefloor", "123456666", "-", "2012-07-05");
            //    dt.Rows.Add("Layout", "Groundfloor", "123444789", "B", "2014-05-01");
            //    dt.Rows.Add("Layout", "Groundfloor", "123333789", "C", "2014-10-10");
      


        }
    }
}
