﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Reflection;

//Aveva related usings
using CMD = Aveva.Pdms.Utilities.CommandLine.Command;

//Amv related usings
//using Amv.Pdms.PML;

namespace Amv.Pdms.Draft.PerfectBatchPlot
{
    class CommandLine
    {
        
        //CONSTANTS
        //string bw = "'COL BW'";
        //string colourGrayscale = "'COL GRAYSCALE'";
        //string colourPlus = "'COL COLOURPLUS'";
        //string imagePng = "'IMAGE TIF'";
        //string reduceSize = "REDUCETOFIT";
        //string pdf = "PDF";
        //string dxf = "DXF";
        //string overwrite = "OVERW";

        //PLOT CE PRINT 'COL COLOURPLUS' A0 
        //PLOT /PLAN1 PDF /plan.pdf 'COL BW,VIEW' A3 
        //PLOT /PLAN2 IMAGE /plan2.png 'IMAGE PNG' A4 
        //PLOT /PLAN3 METAFILE /plan3.pdf A3 OVER 




        //if (!!ce.type.eq(|SHEE|)) then
        // else
        // !!Alert.message('Pls Stay on Sheet')
        // return
        // endif
        // !drgnam = !!CE.namn.replace('/','-').replace('=','')
        //--DXFPATH needs to be defined in Evars
        // var !dxfdfltpath evar DXFPATH
        // !A = OBJECT DXFOUT()
        // !FILE = OBJECT FILE('%PDMSDFLTS%\dra-dxf-graphics.pmldat')
        // !A.LOAD(!FILE)
        // !A.CONFIG()
        // LIEXEC /Draft_DXF_LI 'DXFOUTR13' CONFIGDATA 'Graphics' OUTPUTFILENAME '$!dxfdfltpath$n\$!drgnam$n.dxf' 
        // /3D-AS-010001.DXF LIEXEC /Draft_DXF_LI 'DXFOUTR13' CONFIGDATA 'full_dxf' OUTPUTFILENAME 'C:\TEMP\3D-AS-010001.DXF'


  // LIEXEC /Draft_DWG_LI 'R15' ConfigData 'MyConfigData'
  // 'R15'  indicates the AutoCAD version number, and can be one of the following: 
  //R13
  //R14
  //R15 for 2000, 2002 
  //R18 for versions 2004, 2005, 2006 
  //R21 for versions 2007, 2008, 2009 (R22 and R23 may also be used) for these versions
  //R24 for AutoCAD 2010 
  //DWGOUT for a "default" DWG version, currently 2006, but this may change for future releases
  
        string pdmsUserPath = "/%PDMSUSER%/";
        private string AssembledCommand;





        public string DxfDwgPlot(string CE, PerfectBatchPlotControl c)
        {
            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            DirectoryInfo dir = new DirectoryInfo(assemblyFolder);

            //&$M/C:\AVEVA\AVEVA1214DLLs\andritz_dxf.mac
            //foreach (var item in comboBox1.Items)
            //    textBox2.Text += item.ToString();

            //        foreach (DataRowView item in cmbUser.Items)
            //        {
            //            if (item.Row[0].ToString() == "something")\\gets value displayed
            //{
            //                //do something
            //            }
            //        }
            foreach (var file in c.comboBoxConfigFile.Items)
            { 
                try
                {
                    CMD command = CMD.CreateCommand("$M/" + dir.ToString() + "\\" + file.ToString());
                    //bool sucess = command.RunInPdms();
                    command.RunInPdms();

                    //if (sucess == true)
                    //{
                    //    string info = "Export of " + filename + " done.";
                    //    Console.WriteLine(info);
                    //    CMD commInfo = CMD.CreateCommand("$P " + info);
                    //    commInfo.RunInPdms();
                    //}
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine("Error: {0}", ex.ToString());
                }
            }



            Dictionary<string, string> DwgOutputDict = new Dictionary<string, string>();
            DwgOutputDict.Add("Config. DWG (Default)", "DWGOUT");
            DwgOutputDict.Add("Config. DWG (v2000)", "R15");
            DwgOutputDict.Add("Config. DWG (v2004)", "R18");
            DwgOutputDict.Add("Config. DWG (v2007)", "R21");
            DwgOutputDict.Add("Config. DWG (v2010)", "R24");

            // See whether Dictionary contains this string.
            //if (dictionary.ContainsKey("apple"))

            //    int value = dictionary["apple"];
            //    Console.WriteLine(value);
            
            AssembledCommand = "LIEXEC";
            AssembledCommand += " ";

            if (c.comboBoxDxfDwgFileType.SelectedItem.ToString() == "DXF" &&
                c.comboBoxDxfDwgConfiguration.SelectedItem.ToString() == "Config. DXF")
            {
                AssembledCommand += "/Draft_DXF_LI";
                AssembledCommand += " '";
                AssembledCommand += "DXFOUT";
                AssembledCommand += "' ";
                AssembledCommand += "CONFIGDATA";
                AssembledCommand += " '";
                // DLICON zuerst ausführen und dann referenzieren, anstatt Text !!!!
                AssembledCommand += c.comboBoxConfigFile.Text;

                AssembledCommand += "' ";
                AssembledCommand += "OUTPUTFILENAME";

            }

            else if (c.comboBoxDxfDwgFileType.SelectedItem.ToString() == "DWG")
            {
                if (DwgOutputDict.ContainsKey(c.comboBoxDxfDwgConfiguration.SelectedItem.ToString()))
                {
                    string dwgVersion = DwgOutputDict[c.comboBoxDxfDwgConfiguration.SelectedItem.ToString()];

                    AssembledCommand += "/Draft_DWG_LI";
                    AssembledCommand += " '";
                    AssembledCommand += dwgVersion;
                    AssembledCommand += "' ";
                    AssembledCommand += "CONFIGDATA";
                    AssembledCommand += " '";
                    // DLICON zuerst ausführen und dann referenzieren, anstatt Text !!!!
                    AssembledCommand += c.comboBoxConfigFile.Text;

                    AssembledCommand += "' ";
                    AssembledCommand += "OUTPUTFILENAME";
                }
                else
                {
                    MessageBox.Show("WTF");
                }
            }


            return AssembledCommand;
        }

        public void DoCommandLine(string CE, PerfectBatchPlotControl c)
        {

            //CmdString = "PLOT " + CE + " PDF /%PDMSUSER%/" + filename + " 'COL BW' OVERW";
            string outputType = "";
            string outputArguments = "";
            string filename = CE.TrimStart('/');
            filename = filename.Replace("/", ".");

            //============================================================
            //If statements for Prefix and Suffix options
            //============================================================
            if (c.OptionSuffixSelected == true)
            {
                filename += c.textBoxSuffix.Text.ToString();
            }

            if (c.OptionPrefixSelected == true)
            {
                filename = c.textBoxPrefix.Text.ToString() + filename;
            }

            //============================================================
            //If statements for Output selection
            //============================================================
            if (c.comboBoxOutputType.SelectedItem.ToString() == "PDF")
            {
                filename += ".pdf";

                outputType = c.comboBoxOutputType.SelectedItem.ToString();

                //outputArguments += " '";
                outputArguments = c.comboBoxOutputColor.SelectedItem.ToString();
                //outputArguments += "' ";


            }

            else if (c.comboBoxOutputType.SelectedItem.ToString() == "TIF")
            {
                filename += ".tif";

                outputType = "IMAGE";

                //outputArguments += " 'IMAGE ";
                outputArguments += "IMAGE ";
                outputArguments = c.comboBoxOutputColor.SelectedItem.ToString();
                //outputArguments += "' ";

            }

            else if (c.comboBoxOutputType.SelectedItem.ToString() == "EMF")
            {
                filename += ".emf";

                outputType = "METAFILE";

                //outputArguments += " '";
                outputArguments = c.comboBoxOutputColor.SelectedItem.ToString();
                //outputArguments += "' ";
            }

            else if (c.comboBoxOutputType.SelectedItem.ToString() == "DXF/DWG" &&
                     c.comboBoxDxfDwgFileType.SelectedItem.ToString() == "DXF" &&
                     c.comboBoxDxfDwgConfiguration.SelectedItem.ToString() == "Standard DXF")
            {
                filename += ".dxf";
                outputType = "DXF";
            }

            else if (c.comboBoxOutputType.SelectedItem.ToString() == "DXF/DWG" &&
                     c.comboBoxDxfDwgFileType.SelectedItem.ToString() == "DXF" &&
                     c.comboBoxDxfDwgConfiguration.SelectedItem.ToString() == "Config. DXF")
            {
                filename += ".dxf";
                DxfDwgPlot(CE, c);
            }

            else if (c.comboBoxOutputType.SelectedItem.ToString() == "DXF/DWG" &&
                     c.comboBoxDxfDwgFileType.SelectedItem.ToString() == "DWG")
            {
                filename += ".dwg";
                DxfDwgPlot(CE, c);
            }


            //============================================================
            //Assembling cmd string
            //============================================================
            // PDF, TIF, EMF: PLOT CE outputType pdmsUserPath filename ' outputArguments '
            // DXF, DWG: PLOT CE outputType pdmsUserPath filename


            string CmdString = "PLOT";
            CmdString += " ";
            CmdString += CE;
            CmdString += " ";
            CmdString += outputType;
            CmdString += " ";
            CmdString += pdmsUserPath;
            CmdString += filename;


            if (c.comboBoxOutputType.SelectedItem.ToString() == "DXF/DWG" &&
                c.comboBoxDxfDwgFileType.SelectedItem.ToString() == "DXF" &&
                c.comboBoxDxfDwgConfiguration.SelectedItem.ToString() == "Standard DXF")
            {
                //do nothing
            }
            else if (c.comboBoxOutputType.SelectedItem.ToString() == "DXF/DWG" &&
                     c.comboBoxDxfDwgFileType.SelectedItem.ToString() == "DXF" &&
                     c.comboBoxDxfDwgConfiguration.SelectedItem.ToString() == "Config. DXF")
            {
                CmdString = AssembledCommand;
                CmdString += "' ";
                CmdString += pdmsUserPath;
                CmdString += filename;
                CmdString += " '";
            }

            else if (c.comboBoxOutputType.SelectedItem.ToString() == "DXF/DWG" &&
                     c.comboBoxDxfDwgFileType.SelectedItem.ToString() == "DWG")
            {
                CmdString = AssembledCommand;
                CmdString += "' ";
                CmdString += pdmsUserPath;
                CmdString += filename;
                CmdString += " '";
            }

            else
            {
                //MessageBox.Show("in der else schleife");
                CmdString += " '";
                CmdString += outputArguments;
                CmdString += "' ";

                if (c.OptionOverwriteSelected == true)
                {
                    CmdString += " ";
                    CmdString += "OVER";
                }

            }



            //============================================================
            //Try PDMS command, otherwise exeption
            //============================================================
            MessageBox.Show("CmdString = " + CmdString);
            try
            {

                CMD command = CMD.CreateCommand(CmdString);
                bool sucess = command.RunInPdms();

                //MessageBox.Show(CmdString);

                if (sucess == true)
                {
                    string info = "Export of " + filename + " done.";
                    Console.WriteLine(info);
                    CMD commInfo = CMD.CreateCommand("$P " + info);
                    commInfo.RunInPdms();
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }            
        }

      


    }
}
