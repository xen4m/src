﻿/*  ----------------------------------------------------------------------------
 *  Ambros Markus Ing.
 *  ----------------------------------------------------------------------------
 *  Draft Purpose for PDMS Design
 *  ----------------------------------------------------------------------------
 *  File:           DraPurpAddin.cs
 *  Author:         Markus Ambros
 *  Initial Date:   01.06.2010
 *  ----------------------------------------------------------------------------
 *  Version:        PDMS 12 SP5.2
 *  Namespace:      Ami.Design.DraPurpAddin
 *  ----------------------------------------------------------------------------
 */

using System;
using System.Text;
using Aveva.ApplicationFramework;
using Aveva.ApplicationFramework.Presentation;

namespace Ami.Design.DraPurpAddin
{
    public class DraPurpAddin : IAddin
    {
        private DockedWindow DraPurpWindow;
        private DraPurpControl DraPurpControl;

        #region IAddin Members

        void Aveva.ApplicationFramework.IAddin.Stop()
        {

        }

        public string Description
        {
            get
            {
                return "Provides the Draft Purpose Addin";
            }
        }

        public string Name
        {
            get
            {
                return "DraPurpAddin";
            }
        }

        public void Start(ServiceManager serviceManager)
        {
            /// <summary>
            // Create Addins Windows
            // Get the WindowManager service
            /// </summary>
            /// 
            WindowManager windowManager = (WindowManager)serviceManager.GetService(typeof(WindowManager));
            DraPurpControl = new DraPurpControl();

            // Create a docked window
            DraPurpWindow = windowManager.CreateDockedWindow("Ami.DraPurp.List", "Draft Purpose", DraPurpControl, DockedPosition.Right);
            DraPurpWindow.Width = 400;

            // Docked windows created at addin start should ensure their layout is saved between sessions.
            DraPurpWindow.SaveLayout = true;

            // Create and register addins commands
            // Get the CommandManager
            CommandManager commandManager = (CommandManager)serviceManager.GetService(typeof(CommandManager));
            ShowDraPurpCommand showCommand = new ShowDraPurpCommand(DraPurpWindow);
            commandManager.Commands.Add(showCommand);

            // Load a UIC file for the DraPurpAddin.
            CommandBarManager commandBarManager = (CommandBarManager)serviceManager.GetService(typeof(CommandBarManager));
            commandBarManager.AddUICustomizationFile("DraPurpAddin.uic", "DraPurpAddin");
        }
        #endregion    
    }
}