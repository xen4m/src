﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ami.AcadPdmsAddins
{
public class Exploding

  {

    [CommandMethod("EXP", CommandFlags.UsePickSet)]

    public static void ExplodeEntities()

    {

      Document doc = Application.DocumentManager.MdiActiveDocument;
      Database db = doc.Database;
      Editor ed = doc.Editor;

      // Ask user to select entities

      PromptSelectionOptions pso = new PromptSelectionOptions();

      pso.MessageForAdding = "\nSelect objects to explode: ";

      pso.AllowDuplicates = false;
      pso.AllowSubSelections = true;
      pso.RejectObjectsFromNonCurrentSpace = true;
      pso.RejectObjectsOnLockedLayers = false;

      PromptSelectionResult psr = ed.GetSelection(pso);

      if (psr.Status != PromptStatus.OK)
        return;

      // Check whether to erase the original(s)

      bool eraseOrig = false;

      if (psr.Value.Count > 0)

      {

        PromptKeywordOptions pko = new PromptKeywordOptions("\nErase original objects?");

        pko.AllowNone = true;
        pko.Keywords.Add("Yes");
        pko.Keywords.Add("No");
        pko.Keywords.Default = "No";

        PromptResult pkr = ed.GetKeywords(pko);

        if (pkr.Status != PromptStatus.OK)
            return;

        eraseOrig = (pkr.StringResult == "Yes");

      }


      Transaction tr = db.TransactionManager.StartTransaction();

      using (tr)

      {

        // Collect our exploded objects in a single collection


        DBObjectCollection objs = new DBObjectCollection();

        // Loop through the selected objects


        foreach (SelectedObject so in psr.Value)

        {

          // Open one at a time


          Entity ent = (Entity)tr.GetObject( so.ObjectId, OpenMode.ForRead );

          // Explode the object into our collection

          ent.Explode(objs);

          // Erase the original, if requested

          if (eraseOrig)

          {

            ent.UpgradeOpen();
            ent.Erase();

          }

        }

 

        // Now open the current space in order to
        // add our resultant objects

 

        BlockTableRecord btr = (BlockTableRecord)tr.GetObject( db.CurrentSpaceId, OpenMode.ForWrite);

 

        // Add each one of them to the current space

        // and to the transaction

        foreach (DBObject obj in objs)

        {

          Entity ent = (Entity)obj;
          btr.AppendEntity(ent);
          tr.AddNewlyCreatedDBObject(ent, true);

        }


        // And then we commit

        tr.Commit();

      }
    }
  }
}
