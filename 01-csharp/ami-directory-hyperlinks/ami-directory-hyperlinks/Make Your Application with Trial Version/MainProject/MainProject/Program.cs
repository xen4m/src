﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using SecureApp;

namespace MainProject
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            string abc = @"Software\Tanmay\Protection";
            Secure scr = new Secure();
            bool logic = scr.Algorithm("tanmay", abc);
            if(logic ==true)
                Application.Run(new Form1());
        }
    }
}
