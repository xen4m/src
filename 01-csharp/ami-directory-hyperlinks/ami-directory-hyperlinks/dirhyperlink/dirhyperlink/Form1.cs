﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.CSharp;

using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
//using System.IO;
using System.Threading;
using System.Globalization;

// Specifically adding the Delimon.Win32.IO Library to use in the current Code Page 
using Delimon.Win32.IO;


namespace dirhyperlink
{
    public partial class Form1 : Form
    {
        // Creates and initializes a DateTimeFormatInfo associated with the en-US culture.
        DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;

        int glFileCount = 0;

        string CustomPath;
        string[,] fileDirectoryMarray = new string[1, 1];

        int walkCounter = 0;
        FileInfo[] aReadFiles = new FileInfo[1];
        int counter = 0;
        int columnsDetail = 0;

        public Form1()
        {
            InitializeComponent();
            dataGridView1.ColumnHeadersBorderStyle = ProperColumnHeadersBorderStyle;
        }
        static DataGridViewHeaderBorderStyle ProperColumnHeadersBorderStyle
        {
            get
            {
                return (SystemFonts.MessageBoxFont.Name == "Segoe UI") ?
                DataGridViewHeaderBorderStyle.None :
                DataGridViewHeaderBorderStyle.Raised;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog objDialog = new FolderBrowserDialog();

            objDialog.Description = "Please select a directory";
            objDialog.SelectedPath = @"C:\";       // Vorgabe Pfad (und danach der gewählte Pfad)
            DialogResult objResult = objDialog.ShowDialog(this);

            if (objResult == DialogResult.OK)
                MessageBox.Show("New selected path : " + objDialog.SelectedPath);
            else
                MessageBox.Show("Quit");

            textBox1.Text = objDialog.SelectedPath;
            CustomPath = textBox1.Text;
            label1.Text = "working";

            glFileCount = Program.CountFiles(CustomPath);
            label1.Text = Convert.ToString(glFileCount) + " Selected Items";
        }

        private void WalkDirectory(DirectoryInfo directory)
        {
            progressBar1.Maximum = glFileCount;

            // Scan all files in the current path
            foreach (FileInfo file in directory.GetFiles())
            {
                aReadFiles[walkCounter] = file;
                Array.Resize(ref aReadFiles, aReadFiles.Length + 1);
                progressBar1.Value = walkCounter;
                walkCounter++;
                
            }

            DirectoryInfo[] subDirectories = directory.GetDirectories();

            // Scan the directories in the current directory and call this method 
            // again to go one level into the directory tree
            foreach (DirectoryInfo subDirectory in subDirectories)
            {
                WalkDirectory(subDirectory);
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {

            DirectoryInfo dir = new DirectoryInfo(CustomPath);
            walkCounter = 0;
            WalkDirectory(dir);
            Array.Resize(ref aReadFiles, aReadFiles.Length - 1);

            send_FilesToDatagrid(aReadFiles);

        }

        private void textBox1_KeyDown(object sender, EventArgs e)
        {
            // Event definieren
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            CustomPath = textBox1.Text;
            if (e.KeyCode == Keys.Enter)
            {

                label1.Text = "working";

                glFileCount = Program.CountFiles(CustomPath);
                label1.Text = Convert.ToString(glFileCount) + " Selected Items";
                

                //MessageBox.Show("Enter");
            }
        }


        private void send_FilesToDatagrid(FileInfo[] f)
        {
            columnsDetail = 7;
            counter = 0;

            //Slashes count for datagrid
            for (int i = 0; i < f.Length; i++)
            {
                int count = f[i].DirectoryName.Split('\\').Length;
                if (count > counter)
                {
                    counter = count + 1;
                }
            }

            ResizeArray(ref fileDirectoryMarray, counter, f.Length);

            string[] tempDir = new string[counter];

            for (int y = 0; y < f.Length; y++)
            {
                tempDir = f[y].DirectoryName.Split('\\').ToArray();

                for (int x = 0; x < tempDir.Length; x++)
                {
                    fileDirectoryMarray[x, y] = tempDir[x];
                }
            }


            dataGridView1.ColumnCount = counter + columnsDetail;

            for (int i = 0; i < counter; i++)
            {
                dataGridView1.Columns[i].Name = "Directory " + (i + 1);
            }


            //dataGridView1.Columns[0].Name           = "Directories";
            dataGridView1.Columns[0 + counter].Name = "Filename";
            dataGridView1.Columns[1 + counter].Name = "Ext.";
            dataGridView1.Columns[2 + counter].Name = "Date";

            dataGridView1.Columns[3 + counter].Name = "Hyperlink 1";
            dataGridView1.Columns[4 + counter].Name = "Hyperlink 2";

            dataGridView1.Columns[5 + counter].Name = "Chars";
            dataGridView1.Columns[6 + counter].Name = "Full Path";
            
            //dataGridView1.Columns[3 + counter].Name = "Hyperlink 1";
            //dataGridView1.Columns[4 + counter].Name = "Hyperlink 2";
            //dataGridView1.Columns[5 + counter].Name = "Full Path";





            //int r = dataGridView1.Rows.Add();

            //fileDatum[i] = Convert.ToString(dt.ToString("u", myDTFI));

            //int i = 0;
            for (int i = 0; i < f.Length; i++)
            {

                dataGridView1.Rows.Add();

                //dataGridView1.Rows[i].Cells[0].Value = f[i].DirectoryName;
                dataGridView1.Rows[i].Cells[0 + counter].Value = f[i].Name;

                //tempDir = f[y].DirectoryName.Split('\\').ToArray();
                string[] tempExt = f[i].Name.Split('.').ToArray();

                dataGridView1.Rows[i].Cells[1 + counter].Value = tempExt[tempExt.Length - 1];
                dataGridView1.Rows[i].Cells[2 + counter].Value = Convert.ToString(f[i].LastWriteTime.ToString("u", myDTFI));
                dataGridView1.Rows[i].Cells[5 + counter].Value = f[i].FullName.Length;
                dataGridView1.Rows[i].Cells[6 + counter].Value = f[i].FullName;

                //dataGridView1.Rows[i].Cells[6 + counter].Value = GenConversion.LongToShort(f[i].FullName);
                


                for (int j = 0; j < counter; j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = fileDirectoryMarray[j, i];
                }

            }

        }

        public void ResizeArray(ref string[,] original, int cols, int rows)
        {
            //create a new 2 dimensional array with
            //the size we want
            string[,] newArray = new string[cols, rows];
            //copy the contents of the old array to the new one
            Array.Copy(original, newArray, original.Length);
            //set the original to the new array
            original = newArray;
        }

        private void button3_Click(object sender, EventArgs e)
        {

            Excel.Application oXL;
            Excel.Workbook oWB;
            Excel.Worksheet oSheet;
            Excel.Range oRng;
            Excel.Range oRng2;

            // capture current Culture settings    
            System.Globalization.CultureInfo systemCultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;

            try
            {
                // temporarily change CultureInfo to en-US
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                //Start Excel and get Application object.
                oXL = new Excel.Application();
                //oXL.Visible = true;

                //Get a new workbook.

                oWB = (Excel.Workbook)(oXL.Workbooks.Add(Missing.Value));
                oSheet = (Excel.Worksheet)oWB.ActiveSheet;



                //Microsoft.Office.Interop.Excel.ApplicationClass ExcelApp = new Microsoft.Office.Interop.Excel.ApplicationClass();

                //ExcelApp.Application.Workbooks.Add(Type.Missing);


                //STORE HEADER PART OF DATAGRIDVIEW
                //==================================

                for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
                {
                    oSheet.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
                }

                //STORE BODY PART OF DATAGRIDVIEW
                //==================================

                progressBar2.Maximum = dataGridView1.Rows.Count - 1;

                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                        for (int j = 0; j < dataGridView1.Columns.Count; j++)
                        {

                            if (dataGridView1.Rows[i].Cells[j].Value == null)
                            {
                                oSheet.Cells[i + 2, j + 1] = "-";
                            }
                            else
                            {
                                oSheet.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                            }
                        }

                        string tempDirectory = aReadFiles[i].DirectoryName;
                        string tempFullname = aReadFiles[i].FullName;

                        oSheet.Hyperlinks.Add(oSheet.Cells[i + 2, 4 + counter], Convert.ToString(tempDirectory), Type.Missing, "Jump to directory...", "Directory");
                        oSheet.Hyperlinks.Add(oSheet.Cells[i + 2, 5 + counter], Convert.ToString(tempFullname), Type.Missing, "Jump to file...", "Open");

                        progressBar2.Value = i;
                }


                // Fix first row 
                oSheet.Application.ActiveWindow.SplitRow = 1;
                oSheet.Application.ActiveWindow.FreezePanes = true;
                // Now apply autofilter 
                Excel.Range firstRow = (Excel.Range)oSheet.Rows[1];
                firstRow.Activate();
                firstRow.Select();
                firstRow.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

                //AutoFit columns.



                string rowHeaderEnd = IntConvertToExcelHeadLine(counter + columnsDetail);
                rowHeaderEnd += "1";

                oRng = oSheet.get_Range("A1", rowHeaderEnd);
                //oRng = oSheet.get_Range("A1", IntConvertToExcelHeadLine(counter + columnsDetail));
                oRng.EntireColumn.AutoFit();

                oRng2 = oSheet.get_Range(rowHeaderEnd);
                oRng2.EntireColumn.Hidden = true;

                //Make sure Excel is visible and give the user control
                //of Microsoft Excel's lifetime.
                oXL.Visible = true;
                oXL.UserControl = true;

                // oWB.Close(true, "c:\\temp\\test.xls", System.Reflection.Missing.Value);
                // oXL.Quit();
            }
            catch (Exception theException)
            {
                String errorMessage;
                errorMessage = "Error: ";
                errorMessage = String.Concat(errorMessage, theException.Message);
                errorMessage = String.Concat(errorMessage, " Line: ");
                errorMessage = String.Concat(errorMessage, theException.Source);

                MessageBox.Show(errorMessage, "Error");
            }
            finally
            {
                // put CultureInfo back to original
                System.Threading.Thread.CurrentThread.CurrentCulture = systemCultureInfo;
            }

        }

        private string IntConvertToExcelHeadLine(int colNumber)
        {
            string colBez = "";
            int rest = 0;

            // Ist die Zahl größer als 26 ist das ergebniss mindestens 2 Stellig
            // also muß eine Schleifen verarbeitung her
            if (colNumber > 26)
            {
                do
                {
                    // Ganzzahl ermitteln für den nächsten durchlauf
                    colNumber = Math.DivRem(colNumber, 26, out rest);
                    if (rest == 0)
                    {
                        colNumber -= 1;
                        rest = 26;
                    }
                    //Umwandlung in einen Buchstaben (die + 64 sollten jedem klar)
                    colBez = (char)(rest + 64) + colBez;
                } while (colNumber > 26);
            }
            colBez = (char)(colNumber + 64) + colBez;

            return colBez;
        }




    }

}
