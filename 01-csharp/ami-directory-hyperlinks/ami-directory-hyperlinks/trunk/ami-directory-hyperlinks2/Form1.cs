﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.CSharp;

using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.IO;
using System.Threading;
using System.Globalization;



namespace ami_directory_hyperlinks2
{       

    public partial class Form1 : Form
    {

       //private Action<float> updateProgMethod; 
        // Creates and initializes a DateTimeFormatInfo associated with the en-US culture.
        DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;


        string CustomPath;
        //string[,] array2Db = new string[3, 2]
        string[] files;
        string[] fileDirectory;
        string[, ] fileDirectoryMarray = new string[1,1];
        string[] fileDatum;
        string[] fileExtension;
        string[] fileName;

        int countSlashes;

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, System.EventArgs e)
	    {
	        // Start the BackgroundWorker.
            //updateProgMethod = UpdateProgress;
	        //backgroundWorker1.RunWorkerAsync();
	    }

        private void button2_Click(object sender, EventArgs e)
        {
            countSlashes = 0;

            label3.Text = Convert.ToString(Program.CountFiles(CustomPath)) + " Selected Items";

            files = Directory.GetFiles((CustomPath),"*.*",SearchOption.AllDirectories);
            //Directory.GetLastAccessTime

            Array.Resize(ref fileDirectory, files.Length);
            Array.Resize(ref fileDatum, files.Length);
            Array.Resize(ref fileExtension, files.Length);
            Array.Resize(ref fileName, files.Length);
            

            int i = 0;
            foreach (string sFileName in files)
            {
                fileDirectory[i] = Path.GetDirectoryName(sFileName);
                fileExtension[i] = Path.GetExtension(sFileName);
                fileName[i] = Path.GetFileNameWithoutExtension(sFileName);
                
                DateTime dt = Directory.GetCreationTime(files[i]);
                fileDatum[i] = Convert.ToString(dt.ToString("u", myDTFI));

                i++;
            }




            i = 0;
            foreach (string directory in fileDirectory)
            {
                int count = directory.Split('\\').Length - 1;

                if (count > countSlashes)
                {
                    countSlashes = count;
                }
            }

            ResizeArray(ref fileDirectoryMarray, countSlashes, files.Length);
            //Array.Resize(ref fileDirectoryMarray, [countSlashes,files.Length]);
            //Array
            //MessageBox.Show(Convert.ToString(countSlashes));


            //////////////////////////////////////////////////////////////////

            //i = 0;
            //foreach (string directory in fileDirectory)
            //string[] backslashsep = {"\\"};
            
            for (int x = 0; x < fileDirectory.Length; x++)
            {
                
                string[] temp = fileDirectory[x].Split('\\').ToArray();

                for (i = 0; i < countSlashes; i++)
                {                    
                    if (i >= Convert.ToInt32(temp.Length))
                    {
                        fileDirectoryMarray[x, i] = "-";
                    }
                    fileDirectoryMarray[x, i] = temp[i];

                }


                //string temp = fileDirectory[z];
                //string[] dirs = fileDirectory[x].Split('\\');

                //int y = 0;
                //foreach (string dir in dirs)
                //{
                //    fileDirectoryMarray[x, y] = dir;
                //    y++;
                //}
            }

            


            //////////////////////////////////////////////////////////////////

            //String s = "1:2;1:3;1:4";
            //String[][] f = s.Split(';').Select(t => t.Split(':')).ToArray(); 



            //string s = "there is a cat";
            
            // Split string on spaces.
            // ... This will separate all the words.
            
            //string[] wordddds = s.Split(' ');
            //foreach (string word in wordddds)
            //{
            //    Console.WriteLine(word);
            //}





            i = 0;
            foreach (string name in fileExtension)
            {
                fileExtension[i] = fileExtension[i].TrimStart(new Char[] { '.' });
                i++;
            }



            //listBox1.Items.Add(fileName[0]);
            //listBox1.Items.Add(fileDirectory[0]);
            //listBox1.Items.Add(fileDatum[0]);
            listBox1.Items.Clear();
            listBox1.Items.AddRange(files);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            CustomPath = textBox1.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Excel.Application oXL;
	        Excel.Workbook oWB;
	        Excel.Worksheet oSheet;
	        Excel.Range oRng;
            Excel.Range oRng2;

            // capture current Culture settings    
            System.Globalization.CultureInfo systemCultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;

	        try
	        {
                // temporarily change CultureInfo to en-US
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

		        //Start Excel and get Application object.
		        oXL = new Excel.Application();
		        oXL.Visible = true;

		        //Get a new workbook.
		        oWB = (Excel.Workbook)(oXL.Workbooks.Add( Missing.Value ));
  		        oSheet = (Excel.Worksheet)oWB.ActiveSheet;

                //MessageBox.Show("File creation in progress...");
                //Message1 m2 = new Message1();
                //m2.Show();
                //m2.BringToFront();
                

                

		        //Add table headers going cell by cell.
		        oSheet.Cells[1, 1] = "Directory";
                oSheet.Cells[1, 2 + countSlashes] = "Filename";
                oSheet.Cells[1, 3 + countSlashes] = "Ext.";
                oSheet.Cells[1, 4 + countSlashes] = "Date";
                oSheet.Cells[1, 5 + countSlashes] = "Hyperlink 1";
                oSheet.Cells[1, 6 + countSlashes] = "Hyperlink 2";
                oSheet.Cells[1, 7 + countSlashes] = "Full Path";
                oSheet.get_Range("A1", "G1").Font.Bold = true;
                oSheet.get_Range("A1", "G1").Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);

                //Put Full filename into Excel
                int j = 0;
                for (int i = 2; j < files.Length; i++)
                {
                    oSheet.Cells[i, 1] = Convert.ToString(fileDirectory[j]);
                    oSheet.Cells[i, 2 + countSlashes] = Convert.ToString(fileName[j]);
                    oSheet.Cells[i, 3 + countSlashes] = Convert.ToString(fileExtension[j]);
                    oSheet.Cells[i, 4 + countSlashes] = Convert.ToString(fileDatum[j]);

                    oSheet.Hyperlinks.Add(oSheet.Cells[i, 5 + countSlashes], Convert.ToString(fileDirectory[j]), Type.Missing, "Jump to directory...", "Directory");
                    oSheet.Hyperlinks.Add(oSheet.Cells[i, 6 + countSlashes], Convert.ToString(files[j]), Type.Missing, "Jump to file...", "Open");
                    oSheet.Cells[i, 7 + countSlashes] = Convert.ToString(files[j]);
                    j++;
                }

                // Fix first row 
                oSheet.Application.ActiveWindow.SplitRow = 1;
                oSheet.Application.ActiveWindow.FreezePanes = true;
                // Now apply autofilter 
                Excel.Range firstRow = (Excel.Range)oSheet.Rows[1];
                firstRow.Activate();
                firstRow.Select();
                firstRow.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true); 

		        //AutoFit columns A:D.
		        oRng = oSheet.get_Range("A1", "G1");
		        oRng.EntireColumn.AutoFit();

                oRng2 = oSheet.get_Range("G1");
                oRng2.EntireColumn.Hidden = true;


                //m2.Close();


		        //Make sure Excel is visible and give the user control
		        //of Microsoft Excel's lifetime.
		        oXL.Visible = true;
		        oXL.UserControl = true;

               // oWB.Close(true, "c:\\temp\\test.xls", System.Reflection.Missing.Value);
               // oXL.Quit();
	        }
	        catch( Exception theException ) 
	        {
		        String errorMessage;
		        errorMessage = "Error: ";
		        errorMessage = String.Concat( errorMessage, theException.Message );
		        errorMessage = String.Concat( errorMessage, " Line: " );
		        errorMessage = String.Concat( errorMessage, theException.Source );

		        MessageBox.Show( errorMessage, "Error" );
	        }
            finally
            {
                // put CultureInfo back to original
                System.Threading.Thread.CurrentThread.CurrentCulture = systemCultureInfo;
            }
        }

        private void ResizeArray(ref string[,] original, int cols, int rows)
        {
            //create a new 2 dimensional array with
            //the size we want
            string[,] newArray = new string[rows, cols];
            //copy the contents of the old array to the new one
            Array.Copy(original, newArray, original.Length);
            //set the original to the new array
            original = newArray;
        }




        //private void backgroundWorker1_DoWork_1(object sender, DoWorkEventArgs e)
        //{
        //    for (int i = 1; i <= 100; i++)
        //    {
        //        // Wait 100 milliseconds.
        //        Thread.Sleep(100);
        //        // Report progress.
        //        backgroundWorker1.ReportProgress(i);
        //    }
        //}

        //private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //{
        //    // Change the value of the ProgressBar to the BackgroundWorker progress.
        //    progressBar1.Value = e.ProgressPercentage;
        //    // Set the text.
        //    //this.Text = e.ProgressPercentage.ToString();
        //}



        //private void GetDirectorySizeAsync(string path)
        //{
        //    backgroundWorker1.RunWorkerAsync(path);
        //}

        //private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    DirectoryInfo di = new DirectoryInfo((string)e.Argument);
        //    di.GetTotalSize(ProgressCallback);
        //}

        //// Takes callbacks from the GetTotalSize() method 
        //private void ProgressCallback(float p)
        //{
        //    // Invokes update progress bar on GUI thread: 
        //    this.BeginInvoke(updateProgMethod, new object[] { p });
        //}

        //// Actually updates the progress bar: 
        //private void UpdateProgress(float p)
        //{
        //    progressBar1.Value = (int)(p * (progressBar1.Maximum - progressBar1.Minimum)) + progressBar1.Minimum;
        //} 
    }
}
