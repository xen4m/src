﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

//AVEVA related usings
using Aveva.ApplicationFramework;
using Aveva.ApplicationFramework.Presentation;
using Aveva.Pdms.Shared;
using Aveva.Pdms.Utilities.Messaging;
using Aveva.Pdms.Database;
using Aveva.PDMS.Database.Filters;
using PML = Aveva.Pdms.Utilities.CommandLine;
using ATT = Aveva.Pdms.Database.DbAttributeInstance;
using NOUN = Aveva.Pdms.Database.DbElementTypeInstance;
using Ps = Aveva.Pdms.Database.DbPseudoAttribute;

//Excel related usings
using Microsoft.Office.Interop;
using System.Threading;     // For setting the Localization of the thread to fit
using System.Globalization; // the of the MS Excel localization, because of the MS bug


namespace Pdmsat.Pdms.Draft.MultiPdfPrint
{
    public partial class MultiPdfPrintControl : UserControl
    {
        public string[,] DataArray = new string[1,2];

        public MultiPdfPrintControl()
        {
            InitializeComponent();
            dataGridView1.DataSource = null;

            dataGridView1.Columns.Add("RefNo","RefNo");
            dataGridView1.Columns.Add("sheetName", "Sheet Name");
            dataGridView1.Columns.Add("drawingNumber", "Drw. No.");
            dataGridView1.Columns.Add("revision", "Rev.");
            dataGridView1.Columns.Add("descL1EN", "Descr. L1 EN");
            dataGridView1.Columns.Add("descL2EN", "Descr. L2 EN");

            //dataGridView1.Columns.Add("Value", ":SHEE", "");

            comboBoxOutputColor.Items.Add("COL BW");
            comboBoxOutputColor.Items.Add("COL GRAYSCALE");
            comboBoxOutputColor.Items.Add("COL COLOURPLUS");
            comboBoxOutputColor.SelectedIndex = 0;

            comboBoxOutputType.Items.Add("PDF");
            comboBoxOutputType.Items.Add("TIF");
            comboBoxOutputType.Items.Add("EMF");
            comboBoxOutputType.SelectedIndex = 0;

            textBoxPrefix.Enabled = false;

            textBoxSuffix.Enabled = false;
        }

        public DbElement ceName;

        public void DataGridClear()
        {
            this.dataGridView1.DataSource = null;
            dataGridView1.DataSource = null;
        }

        //============================================================
        //Public Booleans for options
        //============================================================
        public bool OptionOverwriteSelected
        {
            get { return checkBoxOverwrite.Checked; }
        }

        public bool OptionPrefixSelected
        {
            get { return checkBoxPrefix.Checked; }
        }

        public bool OptionSuffixSelected
        {
            get { return checkBoxSuffix.Checked; }
        }



        public void AddCollectedCE(string refno, string name, string namn, string value1, string value2, string value3, string value4)
        {
            // string namn not used at the moment
            int i = dataGridView1.RowCount;
                dataGridView1.Rows.Add(new object[] { refno, name, value1, value2, value3, value4 });

        }

        public static DataSet MultidimensionalArrayToDataSet(string[,] input)
        {
            var dataSet = new DataSet();
            var dataTable = dataSet.Tables.Add();
            var iFila = input.GetLongLength(0);
            var iCol = input.GetLongLength(1);

            //Fila
            for (var f = 1; f < iFila; f++)
            {
                var row = dataTable.Rows.Add();
                //Columna
                for (var c = 0; c < iCol; c++)
                {
                    if (f == 1) dataTable.Columns.Add(input[0, c]);
                    row[c] = input[f, c];
                }
            }
            return dataSet;
        }

        private void buttonGetCE_Click(object sender, EventArgs e)
        {
            textBoxCE.Clear();
            this.dataGridView1.Rows.Clear();
            
            string ce = CurrentElement.Element.GetAsString(DbAttributeInstance.NAME);
            this.textBoxCE.Text = ce;
            ceName = CurrentElement.Element;
            
            MultiPdfPrintColletion.Run(ceName, this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.dataGridView1.SelectAll();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.dataGridView1.ClearSelection();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            foreach(DataGridViewRow row in dataGridView1.SelectedRows)
            {
                //column index is 0 for first row (name of SHEEt)
                string name = row.Cells[0].Value.ToString();

                CommandLine command1 = new CommandLine();
                command1.DoCommandLine(name, this);
            }
        }

        private void checkBoxPrefix_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPrefix.Checked == true)
                textBoxPrefix.Enabled = true;
            else
                textBoxPrefix.Enabled = false;
        }

        private void checkBoxSuffix_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSuffix.Checked == true)
                textBoxSuffix.Enabled = true;
            else
                textBoxSuffix.Enabled = false;
        }

        private void buttonAddCE_Click(object sender, EventArgs e)
        {
            textBoxCE.Clear();
            //this.dataGridView1.Rows.Clear();

            string ce = CurrentElement.Element.GetAsString(DbAttributeInstance.NAME);
            this.textBoxCE.Text = ce;
            ceName = CurrentElement.Element;

            MultiPdfPrintColletion.Run(ceName, this);
        }




        private void exportListToxlsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            //excelFileName = System.IO.Path.Combine(excelPath, "Ziperty Buy Model for Web 11_11_2011.xlsm");


            // In try catch finally Anweisung integrieren!!

            //MessageBox.Show("Start Export");
                // creating Excel Application 
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                //MessageBox.Show("Starting Excel");  
            app.Visible = false;
                // creating new WorkBook within Excel application 
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add();

                // creating new Excelsheet in workbook 
                Microsoft.Office.Interop.Excel._Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.ActiveSheet;
                //MessageBox.Show("Sheet selected");
                // see the excel sheet behind the program 
                

                // get the reference of first sheet. By default its name is Sheet1. 
                // store its reference to worksheet 

                worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets["Sheet1"];
                worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.ActiveSheet;


                //MessageBox.Show("Start Export 2");
                // changing the name of active sheet 
                worksheet.Name = "Exported from gridview";


                // storing header part in Excel 
                for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
                {
                    worksheet.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
                }


                // storing Each row and column value to excel sheet 

                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < dataGridView1.Columns.Count; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                    }
                }

                // save the application 
                workbook.SaveAs("c:\\temp\\output.xlsx", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                app.Quit();
                workbook = null;
                app = null;
                //MessageBox.Show("Export done!");
                if (MessageBox.Show("Export has been done! Open the list now?", "List Export", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start("c:\\temp\\output.xlsx");
                    }
                //} 


        }
     
        private void buttonClearDataGridView_Click(object sender, EventArgs e)
        {
            textBoxCE.Clear();
            this.dataGridView1.Rows.Clear();
        }


        private void dataGridView1_MouseDown(object sender, System.Windows.Forms.DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    //MessageBox.Show("Right mouse down!!");

                    contextMenuStrip1.Show(MousePosition);
                    //row.Selected = false;
                }
            }
        }

        private void gotoRefNoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            try
            {
                Int32 selectedRowCount = dataGridView1.Rows.GetRowCount(DataGridViewElementStates.Selected);
                
                //Check if only one row is selected
                if (selectedRowCount == 1)
                {
                    //Find out what refno is in current selected list
                    int rowIndex = dataGridView1.CurrentCell.RowIndex;
                    int cellIndex = 0;
                    
                    string CmdString = dataGridView1.Rows[rowIndex].Cells[cellIndex].Value.ToString();
                    MessageBox.Show(CmdString);

                    //Goto selected refno
                    
                    try
                    {
                        PML.Command PmlCommand = PML.Command.CreateCommand(CmdString);
                        PmlCommand.RunInPdms();

                    }
                    catch (System.Exception ex)
                    {
                        Console.WriteLine("Error: {0}", ex.ToString());
                    }
                }
            }
            catch
            {
                MessageBox.Show("More than one row is selected. Cannot goto RefNo.");
            }
        }
   }
}




