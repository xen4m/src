﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

//AVEVA related usings
using PML = Aveva.Pdms.Utilities.CommandLine;


namespace Pdmsat.Pdms.Draft.MultiPdfPrint
{
    class CommandLine
    {
        

        //CONSTANTS
        //string bw = "'COL BW'";
        //string colourGrayscale = "'COL GRAYSCALE'";
        //string colourPlus = "'COL COLOURPLUS'";
        //string imagePng = "'IMAGE TIF'";
        //string reduceSize = "REDUCETOFIT";
        //string pdf = "PDF";
        //string dxf = "DXF";
        //string overwrite = "OVERW";

        //PLOT CE PRINT 'COL COLOURPLUS' A0 
        //PLOT /PLAN1 PDF /plan.pdf 'COL BW,VIEW' A3 
        //PLOT /PLAN2 IMAGE /plan2.png 'IMAGE PNG' A4 
        //PLOT /PLAN3 METAFILE /plan3.pdf A3 OVER 


        string pdmsUserPath = "/%PDMSUSER%/";


        public void DoCommandLine(string CE, MultiPdfPrintControl c)
        {
            
            //CmdString = "PLOT " + CE + " PDF /%PDMSUSER%/" + filename + " 'COL BW' OVERW";
            string outputType = "";
            string outputArguments = "";
            string filename = CE.TrimStart('/');
            filename = filename.Replace("/", ".");

            //============================================================
            //If statements for Prefix and Suffix options
            //============================================================
            if (c.OptionSuffixSelected == true)
            {
                filename += c.textBoxSuffix.Text.ToString();
            }

            if (c.OptionPrefixSelected == true)
            {
                filename = c.textBoxPrefix.Text.ToString() + filename;
            }

            //============================================================
            //If statements for Output selection
            //============================================================
            if (c.comboBoxOutputType.SelectedItem.ToString() == "PDF")
            {
                filename += ".pdf";

                outputType = c.comboBoxOutputType.SelectedItem.ToString();

                //outputArguments += " '";
                outputArguments = c.comboBoxOutputColor.SelectedItem.ToString();
                //outputArguments += "' ";


            }
            else if (c.comboBoxOutputType.SelectedItem.ToString() == "TIF")
            {
                filename += ".tif";

                outputType = "IMAGE";

                //outputArguments += " 'IMAGE ";
                outputArguments += "IMAGE ";
                outputArguments = c.comboBoxOutputColor.SelectedItem.ToString();
                //outputArguments += "' ";

            }

            else if (c.comboBoxOutputType.SelectedItem.ToString() == "EMF")
            {
                filename += ".emf";

                outputType = "METAFILE";

                //outputArguments += " '";
                outputArguments = c.comboBoxOutputColor.SelectedItem.ToString();
                //outputArguments += "' ";

            }


            //============================================================
            //Assembling cmd string
            //============================================================
            string CmdString = "PLOT";
            CmdString += " ";
            CmdString += CE;
            CmdString += " ";
            CmdString += outputType;
            CmdString += " ";
            CmdString += pdmsUserPath;
            CmdString += filename;
            CmdString += " '"; 
            CmdString += outputArguments;
            CmdString += "' "; 

            if (c.OptionOverwriteSelected == true)
            {
                CmdString += " ";
                CmdString += "OVER";
            }

            Console.WriteLine(CmdString);

            //PMLCommand pml = new PMLCommand();
            try
            {
                
                //pml.CreatePmlCommand(CmdString);
                PML.Command PmlCommand = PML.Command.CreateCommand(CmdString);
                PmlCommand.RunInPdms();


            }
            catch (System.Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
        }
    }
}
