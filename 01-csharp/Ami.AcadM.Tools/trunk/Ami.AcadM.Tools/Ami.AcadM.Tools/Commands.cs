﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Windows;
using System.Reflection;

using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;


// This line is not mandatory, but improves loading performances
[assembly: CommandClass(typeof(Ami.AcadM.Tools.NewCommands))]

namespace Ami.AcadM.Tools
{

    // This class is instantiated by AutoCAD for each document when
    // a command is called by the user the first time in the context
    // of a given document. In other words, non static data in this class
    // is implicitly per-document!
    public class NewCommands
    {
        // The CommandMethod attribute can be applied to any public  member 
        // function of any public class.
        // The function should take no arguments and return nothing.
        // If the method is an intance member then the enclosing class is 
        // intantiated for each document. If the member is a static member then
        // the enclosing class is NOT intantiated.
        //
        // NOTE: CommandMethod has overloads where you can provide helpid and
        // context menu.

        // Modal Command with localized name
        //[CommandMethod("MyGroup", "MyCommand", "MyCommandLocal", CommandFlags.Modal)]
        //public void MyCommand() // This method can have any name
        //{
        //    // Put your command code here
        //    Document doc = Application.DocumentManager.MdiActiveDocument;
        //    Editor ed;
        //    if (doc != null)
        //    {
        //        ed = doc.Editor;
        //        ed.WriteMessage("Hello, this is your first command.");

        //    }
        //}

        //public static MainWindows mWindow;

        //[CommandMethod("PDMS Tools", "PDMSTOOLPALETTE", "PDMSTOOLPALETTE", CommandFlags.Modal)]
        //public void StartPdmsToolPalette()
        //{
        //    Plugin.PdmsPalette();
        //}

        //[CommandMethod("PDMS Tools", "PDMSTESTRIBBON", "PDMSTESTRIBBON", CommandFlags.Modal)]
        //public void ShowPdmsTestRibbon()
        //{
        //    Plugin.PdmsTestRibbon();
        //}
        
        [CommandMethod("PDMS Tools", "PDMSEXPPANEL", "PDMSEXPPANEL", CommandFlags.Modal)]
        public void ExportPanelToPDMS()
        {
            //settings.GetXmlSettings();
            PolyLineExport.ExportAsPanel();
        }

        [CommandMethod("PDMS Tools", "PDMSEXPEXTR", "PDMSEXPEXTR", CommandFlags.Modal)]
        public void ExportExtrusionToPDMS()
        {
            //MessageBox.Show("ich bin da");
            //settings.GetXmlSettings();
            //MessageBox.Show("XML File gelesen");
            PolyLineExport.ExportAsExtrusion();
        }

        [CommandMethod("PDMS Tools", "PDMSEXPFLOOR", "PDMSEXPFLOOR", CommandFlags.Modal)]
        public void ExportFloorToPDMS()
        {
            //settings.GetXmlSettings();
            PolyLineExport.ExportAsFloor();
        }

        [CommandMethod("PDMS Tools", "PDMSEXPREVOL", "PDMSEXPREVOL", CommandFlags.Modal)]
        public void ExportRevolToPDMS()
        {
            //settings.GetXmlSettings();
            PolyLineExport.ExportAsRevol();
        }

        [CommandMethod("PDMS Tools", "PDMSEXPNREVOL", "PDMSEXPNREVOL", CommandFlags.Modal)]
        public void ExportNegativRevolToPDMS()
        {
            //settings.GetXmlSettings();
            PolyLineExport.ExportAsNegativRevol();
        }

        [CommandMethod("PDMS Tools", "PDMSEXPNEXTR", "PDMSEXPNEXTR", CommandFlags.Modal)]
        public void ExportNegativExtrusionToPDMS()
        {
            //settings.GetXmlSettings();
            PolyLineExport.ExportAsNegativExtrusion();
        }

        //[CommandMethod("PDMS Tools", "PDMSSETTINGS", "PDMSSETTINGS", CommandFlags.Modal)]
        //public void ManagePDMSSettings()
        //{
            
        //    //mWindow = new MainWindows();
        //    //Application.ShowModalWindow(mWindow);


        //}


        [CommandMethod("AMIEXPLODE", "AMIEXPLODE", "AMIEXPLODE", CommandFlags.Modal)]
        public void AmiExplodeTest()
        {
            //Exploding.ExplodeEntities();
        }

















        // Modal Command with pickfirst selection
        //[CommandMethod("MyGroup", "MyPickFirst", "MyPickFirstLocal", CommandFlags.Modal | CommandFlags.UsePickSet)]
        //public void MyPickFirst() // This method can have any name
        //{
        //    PromptSelectionResult result = Application.DocumentManager.MdiActiveDocument.Editor.GetSelection();
        //    if (result.Status == PromptStatus.OK)
        //    {
        //        // There are selected entities
        //        // Put your command using pickfirst set code here
        //    }
        //    else
        //    {
        //        // There are no selected entities
        //        // Put your command code here
        //    }
        //}

        // Application Session Command with localized name
        //[CommandMethod("MyGroup", "MySessionCmd", "MySessionCmdLocal", CommandFlags.Modal | CommandFlags.Session)]
        //public void MySessionCmd() // This method can have any name
        //{
        //    // Put your command code here
        //}

        // LispFunction is similar to CommandMethod but it creates a lisp 
        // callable function. Many return types are supported not just string
        // or integer.
        //[LispFunction("MyLispFunction", "MyLispFunctionLocal")]
        //public int MyLispFunction(ResultBuffer args) // This method can have any name
        //{
        //    // Put your command code here

        //    // Return a value to the AutoCAD Lisp Interpreter
        //    return 1;
        //}

    }

}
