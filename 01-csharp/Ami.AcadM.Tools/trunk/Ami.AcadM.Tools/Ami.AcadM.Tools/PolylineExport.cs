﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

using acadApp = Autodesk.AutoCAD.ApplicationServices.Application;


[assembly: CommandClass(typeof(Ami.AcadM.Tools.PolyLineExport))]
namespace Ami.AcadM.Tools
{
    class PolyLineExport
    {
        //public static string exportPath = @"C:\temp\";
        public static string exportPath = Constants.EXPORTPATH;
        public static StringBuilder sbData = new StringBuilder();



        public static string HeaderForExport()
        {
            DateTime today = DateTime.Now;
            Database db = HostApplicationServices.WorkingDatabase;

            string expfile = db.Filename.ToString();


            StringBuilder header = new StringBuilder();
            header.Append("$* (c) Acad PDMS Tools 1.1.100 by Ambros M. (2013)\n");
            header.Append("$* Export out of file: " + expfile + "\n");
            header.Append("$* Time: " + today.ToString("yyyy-MM-dd, HH:mm:ss") + "\n");
            header.Append("$*---------------------------------------------\n\n\n");
            return header.ToString();
        }

        public static double Atan2(double y, double x)
        {

            if (x > 0)
                return Math.Atan(y / x);
            else if (x < 0)
                return Math.Atan(y / x) - Math.PI;
            else  // x == 0
            {
                if (y > 0)
                    return Math.PI;
                else if (y < 0)
                    return -Math.PI;
                else // if (y == 0) theta is undefined
                    return 0.0;
            }
        }

        //public static Vector3d GetDistance3D(Point3d pt1,  Point3d pt2)
        //{
        //    Vector3d vec1 = new Vector3d();

        //    //Vector3d _mVector3d = new Vector3d();
        //    //return pt1.GetVectorTo(pt2).Length.ToString();
        //    //vec1.
        //    return vec1.Length;
        //}


  //Point2d pt1 = new Point2d(2, 5);
  //Point2d pt2 = new Point2d(5, 2);
 
  //Application.ShowAlertDialog("Angle from XAxis: " +
  //                            pt1.GetVectorTo(pt2).Angle.ToString());



        static public void ExportPolyline(Document doc, Database db, Editor ed, StringBuilder sb, string typePrimitive, string wrt)
        {
            // set counter
            int cnt = 0;

            PromptEntityOptions opt = new PromptEntityOptions("\nSelect a polyline object: ");

            opt.SetRejectMessage("\nObject must be LWPolyline, Polyline2d or Polyline3d only.");
            opt.AddAllowedClass(typeof(Polyline), false);
            opt.AddAllowedClass(typeof(Polyline2d), false);
            opt.AddAllowedClass(typeof(Polyline3d), false);

            PromptEntityResult res = ed.GetEntity(opt);

            if (res.Status != PromptStatus.OK) return;
            //set precision
            int prec = 0;

            PromptIntegerOptions pio = new PromptIntegerOptions("\nEnter number of decimals <0>: ");
            pio.AllowNone = true;
            PromptIntegerResult pir = ed.GetInteger(pio);

            if (pir.Status != PromptStatus.None && pir.Status != PromptStatus.OK) return;
            if (pir.Status == PromptStatus.OK) prec = pir.Value;

            Transaction tr = db.TransactionManager.StartTransaction();


            //Database db = HostApplicationServices.WorkingDatabase;
            //Editor ed = MgdAcApplication.DocumentManager.MdiActiveDocument.Editor;
            //Matrix3d ucs = ed.CurrentUserCoordinateSystem;

            using (tr)
            {
                DBObject obj = tr.GetObject(res.ObjectId, OpenMode.ForRead);
                ed.WriteMessage("\n >>>   {0}", obj.GetRXClass().Name);

                if (obj == null) return;

                switch (obj.GetRXClass().Name)
                {
                    case "AcDbPolyline":
                        {
                            Polyline lwpoly = obj as Polyline;

                            if (lwpoly != null)
                            {
                                for (int i = 0; i < lwpoly.NumberOfVertices; i++)
                                {
                                    cnt += 1;
                                    Point3d pt = lwpoly.GetPoint3dAt(i);
                                    //Point3d pt2 = lwpoly.GetPoint3dAt(i + 1);

                                    if (lwpoly.GetBulgeAt(i) != 0 && i + 2 < lwpoly.NumberOfVertices)
                                    {
                                        Point3d pt2 = lwpoly.GetPoint3dAt(i + 1);
                                        Point3d pt3 = lwpoly.GetPoint3dAt(i + 2);




                                    }

                                    //Point3d ptBulge = Math.Atan(Math.Abs(pt2.));

                                    

                                    //MessageBox.Show(lwpoly.GetBulgeAt(i).ToString());
                                    //Point3d pt2 = lwpoly.GetPoint3dAt(i+1);
                                    
                                    //if(lwpoly.GetBulgeAt(i) != 0)
                                    //{
                                        //double bulge = lwpoly.GetBulgeAt(i);

                                        //double chord = Math.Sqrt(Math.Pow(Math.Abs(pt2.X - pt.X), 2) + Math.Pow(Math.Abs(pt2.Y - pt.Y), 2));
                                        //double s = chord / 2 * bulge;
                                        //double radius = (Math.Pow(chord / 2, 2) + Math.Pow(s, 2)) / (2 * s);
                                        //double frad = (4 * Math.Atan(bulge));
                                        //MessageBox.Show(bulge.ToString());
                                        //MessageBox.Show(chord.ToString());
                                        //MessageBox.Show(radius.ToString());
                                        //MessageBox.Show(frad.ToString());
                                    //}
                                    //double bulge = lwpoly.GetBulgeAt(i);
                                    //double chord = Math.Sqrt(Math.Pow(Math.Abs(pt.X - pt2.X), 2) + Math.Pow(Math.Abs(pt.Y - pt2.Y), 2));
                                    //double s = chord / 2 * bulge;
                                    //double radius = (Math.Pow(chord / 2, 2) + Math.Pow(s, 2)) / (2 * s);

                                    //double frad = ( 4 * Math.Atan(bulge));


                                    ed.WriteMessage("\nX = {0}; Y = {1}; Z = {2}", Math.Round(pt.X, prec), Math.Round(pt.Y, prec), Math.Round(pt.Z, prec));

                                    sb.Append("NEW " + typePrimitive + " POS"
                                        + " E " +
                                        Math.Round(pt.X, prec).ToString()
                                        + " N " +
                                        Math.Round(pt.Y, prec).ToString()
                                        + " U " +
                                        Math.Round(pt.Z, prec).ToString()
                                        + " " + wrt + " END \n");
                                }
                            }
                            break;
                        }

                    case "AcDb2dPolyline":
                        {
                            Polyline2d poly2d = obj as Polyline2d;

                            if (poly2d != null)
                            {
                                foreach (ObjectId ix in poly2d)
                                {
                                    cnt += 1;

                                    Vertex2d vex = (Vertex2d)ix.GetObject(OpenMode.ForRead);
                                    Point3d pt = vex.Position;
                                    ed.WriteMessage("\nX = {0}; Y = {1}; Z = {2}", Math.Round(pt.X, prec), Math.Round(pt.Y, prec), Math.Round(pt.Z, prec));

                                    sb.Append("NEW " + typePrimitive + " POS"
                                        + " E " +
                                        Math.Round(pt.X, prec).ToString()
                                        + " N " +
                                        Math.Round(pt.Y, prec).ToString()
                                        + " U " +
                                        Math.Round(pt.Z, prec).ToString()
                                        + " " + wrt + " END \n");
                                }
                            }
                            break;
                        }

                    case "AcDb3dPolyline":
                        {
                            Polyline3d poly3d = obj as Polyline3d;

                            if (poly3d != null)
                            {
                                foreach (ObjectId ix in poly3d)
                                {
                                    cnt += 1;
                                    PolylineVertex3d vex = (PolylineVertex3d)ix.GetObject(OpenMode.ForRead);
                                    Point3d pt = vex.Position;

                                    ed.WriteMessage("\nX = {0}; Y = {1}; Z = {2}", Math.Round(pt.X, prec), Math.Round(pt.Y, prec), Math.Round(pt.Z, prec));
                                    sb.Append("NEW " + typePrimitive + " POS"
                                        + " E " +
                                        Math.Round(pt.X, prec).ToString()
                                        + " N " +
                                        Math.Round(pt.Y, prec).ToString()
                                        + " U " +
                                        Math.Round(pt.Z, prec).ToString()
                                        + " " + wrt + " END \n");
                                }
                            }
                            break;
                        }
                    default:
                        break;
                }
                //sbData.Append(sb.ToString());
                tr.Commit();
            }
        }





        public static int nextFileNumber = 0;

        public static string GetNextFileName(string FileName)
        {
            DateTime today = DateTime.Now;
            //today.ToString("yyyy-MM-dd, HH:mm:ss")
            return String.Format("{0}-{1}-{2}.txt", FileName, today.ToString("yyyyMMdd-HH-mm-ss"), nextFileNumber++);
        }


        static public void ExportAsPanel()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = doc.Editor;
            StringBuilder sb = new StringBuilder();

            string filename = "Panel";
            string typePrimitiv = "PAVERT";
            string wrt = Constants.PANELWRT;
            string position = Constants.PANELPOSITION;
            string orientation = Constants.PANELORIENTATION;

            sb.Append(HeaderForExport());

            sb.Append("NEW PANEL \n");
            sb.Append("POS " + position + " " + wrt + " \n");
            sb.Append(orientation + " \n\n");
            sb.Append("NEW PLOOP\n");


            int heig = Int32.Parse(Constants.PANELHEIGHT);
            string intOption = String.Format("\nEnter height of panel <{0}>: ", heig);
            PromptIntegerOptions pio = new PromptIntegerOptions(intOption);
            pio.AllowNone = true;
            PromptIntegerResult pir = ed.GetInteger(pio);

            if (pir.Status != PromptStatus.None && pir.Status != PromptStatus.OK) return;
            if (pir.Status == PromptStatus.OK) heig = pir.Value;
            sb.Append("HEIG " + heig.ToString() + "\n");

            sb.Append(Constants.PANELJUSTIFICATION + " \n\n");


            ExportPolyline(doc, db, ed, sb, typePrimitiv, wrt);
            sb.Append(sbData);

            string FileName = GetNextFileName(filename);
            string FullPath = exportPath + FileName;
            System.IO.StreamWriter sw = new StreamWriter(FullPath, false);

            using (sw)
            {
                sw.Write(sb.ToString());
            }
        }


        static public void ExportAsExtrusion()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = doc.Editor;
            StringBuilder sb = new StringBuilder();

            string filename = "Extrusion";
            string typePrimitiv = "VERTEX";
            string wrt = Constants.EXTRWRT;
            string position = Constants.EXTRPOSITION;
            string orientation = Constants.EXTRORIENTATION;

            sb.Append(HeaderForExport());

            sb.Append("NEW EXTRUSION \n");
            sb.Append("POS " + position + " " + wrt + " \n");
            sb.Append(orientation + " \n\n");
            //sb.Append("NEW PLOOP\n");


            int heig = Int32.Parse(Constants.EXTRHEIGHT);
            string intOption = String.Format("\nEnter height of extrusion <{0}>: ", heig);
            PromptIntegerOptions pio = new PromptIntegerOptions(intOption);
            pio.AllowNone = true;
            PromptIntegerResult pir = ed.GetInteger(pio);

            if (pir.Status != PromptStatus.None && pir.Status != PromptStatus.OK) return;
            if (pir.Status == PromptStatus.OK) heig = pir.Value;
            sb.Append("HEIG " + heig.ToString() + "\n");

            //sb.Append(Constants.PANELJUSTIFICATION + " \n\n");
            sb.Append("NEW LOOP\n");

            ExportPolyline(doc, db, ed, sb, typePrimitiv, wrt);
            sb.Append(sbData);

            string FileName = GetNextFileName(filename);
            string FullPath = exportPath + FileName;
            System.IO.StreamWriter sw = new StreamWriter(FullPath, false);

            using (sw)
            {
                sw.Write(sb.ToString());
            }
        }

        static public void ExportAsFloor()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = doc.Editor;
            StringBuilder sb = new StringBuilder();

            string filename = "Floor";
            string typePrimitiv = "PAVERT";
            string wrt = Constants.FLOORWRT;
            string position = Constants.FLOORPOSITION;
            string orientation = Constants.FLOORORIENTATION;


            sb.Append(HeaderForExport());

            sb.Append("NEW FLOOR \n");
            sb.Append("POS " + position + " " + wrt + " \n");
            sb.Append(orientation + " \n\n");
            sb.Append("NEW PLOOP\n");

            //int heig = 100;
            int heig = Int32.Parse(Constants.FLOORHEIGHT);
            string intOption = String.Format("\nEnter height of floor <{0}>: ", heig);
            PromptIntegerOptions pio = new PromptIntegerOptions(intOption);
            pio.AllowNone = true;
            PromptIntegerResult pir = ed.GetInteger(pio);

            if (pir.Status != PromptStatus.None && pir.Status != PromptStatus.OK) return;
            if (pir.Status == PromptStatus.OK) heig = pir.Value;
            sb.Append("HEIG " + heig.ToString() + "\n");

            sb.Append(Constants.FLOORJUSTIFICATION + " \n\n");


            ExportPolyline(doc, db, ed, sb, typePrimitiv, wrt);
            sb.Append(sbData);

            string FileName = GetNextFileName(filename);
            string FullPath = exportPath + FileName;
            System.IO.StreamWriter sw = new StreamWriter(FullPath, false);

            using (sw)
            {
                sw.Write(sb.ToString());
            }
        }


        static public void ExportAsRevol()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = doc.Editor;
            StringBuilder sb = new StringBuilder();

            string filename = "Revol";
            string typePrimitiv = "VERTEX";
            string wrt = Constants.REVOLUTIONWRT;
            string position = Constants.REVOLUTIONPOSITION;
            string orientation = Constants.REVOLUTIONORIENTATION;

            sb.Append(HeaderForExport());

            sb.Append("NEW REVOLUTION \n");
            sb.Append("POS " + position + " " + wrt + " \n");
            sb.Append(orientation + " \n\n");

            //int angle = 360;
            int angle = Int32.Parse(Constants.REVOLUTIONANGLE);
            string intOption = String.Format("\nEnter angle of revolution <{0}>: ", angle);
            PromptIntegerOptions pio = new PromptIntegerOptions(intOption);
            pio.AllowNone = true;
            PromptIntegerResult pir = ed.GetInteger(pio);

            if (pir.Status != PromptStatus.None && pir.Status != PromptStatus.OK) return;
            if (pir.Status == PromptStatus.OK) angle = pir.Value;
            sb.Append("ANGLE " + angle.ToString() + "\n");

            sb.Append("NEW LOOP\n");



            ExportPolyline(doc, db, ed, sb, typePrimitiv, wrt);
            sb.Append(sbData);

            string FileName = GetNextFileName(filename);
            string FullPath = exportPath + FileName;
            System.IO.StreamWriter sw = new StreamWriter(FullPath, false);

            using (sw)
            {
                sw.Write(sb.ToString());
            }
        }


        static public void ExportAsNegativRevol()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = doc.Editor;
            StringBuilder sb = new StringBuilder();

            string filename = "NegRevol";
            string typePrimitiv = "VERTEX";
            string wrt = Constants.NREVOLUTIONWRT;
            string position = Constants.NREVOLUTIONPOSITION;
            string orientation = Constants.NREVOLUTIONORIENTATION;

            sb.Append(HeaderForExport());

            sb.Append("NEW NREVOLUTION \n");
            sb.Append("POS " + position + " " + wrt + " \n");
            sb.Append(orientation + " \n\n");

            //int angle = 360;
            int angle = Int32.Parse(Constants.NREVOLUTIONANGLE);
            string intOption = String.Format("\nEnter angle of negative revolution <{0}>: ", angle);
            PromptIntegerOptions pio = new PromptIntegerOptions(intOption);
            pio.AllowNone = true;
            PromptIntegerResult pir = ed.GetInteger(pio);

            if (pir.Status != PromptStatus.None && pir.Status != PromptStatus.OK) return;
            if (pir.Status == PromptStatus.OK) angle = pir.Value;
            sb.Append("ANGLE " + angle.ToString() + "\n");

            sb.Append("NEW LOOP\n");



            ExportPolyline(doc, db, ed, sb, typePrimitiv, wrt);
            sb.Append(sbData);

            string FileName = GetNextFileName(filename);
            string FullPath = exportPath + FileName;
            System.IO.StreamWriter sw = new StreamWriter(FullPath, false);

            using (sw)
            {
                sw.Write(sb.ToString());
            }
        }



        static public void ExportAsNegativExtrusion()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = doc.Editor;
            StringBuilder sb = new StringBuilder();

            string filename = "NegExtr";
            string typePrimitiv = "VERTEX";
            string wrt = Constants.NEGEXTRWRT;
            string position = Constants.NEGEXTRPOSITION;
            string orientation = Constants.NEGEXTRORIENTATION;

            sb.Append(HeaderForExport());

            sb.Append("NEW NXTRUSION \n");
            sb.Append("POS " + position + " " + wrt + " \n");
            sb.Append(orientation + " \n\n");
            
            //int heig = 100;
            int heig = Int32.Parse(Constants.NEGEXTRHEIGHT);
            string intOption = String.Format("\nEnter height of negative extrusion <{0}>: ", heig);
            PromptIntegerOptions pio = new PromptIntegerOptions(intOption);
            pio.AllowNone = true;
            PromptIntegerResult pir = ed.GetInteger(pio);

            if (pir.Status != PromptStatus.None && pir.Status != PromptStatus.OK) return;
            if (pir.Status == PromptStatus.OK) heig = pir.Value;
            sb.Append("HEIG " + heig.ToString() + "\n");

            sb.Append("NEW LOOP\n");
            //sb.Append("SJUS UTOP\n\n");


            ExportPolyline(doc, db, ed, sb, typePrimitiv, wrt);
            sb.Append(sbData);

            string FileName = GetNextFileName(filename);
            string FullPath = exportPath + FileName;
            System.IO.StreamWriter sw = new StreamWriter(FullPath, false);

            using (sw)
            {
                sw.Write(sb.ToString());
            }
            notifcation.Commands DoNotification = new notifcation.Commands();
            DoNotification.statusBarBalloon();

        }
    }
}


//Polyline2d 
//List bulges = new List();
// foreach (ObjectId id in pl2d)
// {
// Vertex2d vtx = (Vertex2d)id.GetObject(OpenMode.ForRead);
// bulges.Add(vtx.Bulge); 
//}
