﻿
using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Windows;
using System.Reflection;




using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

using acadApp = Autodesk.AutoCAD.ApplicationServices.Application;


namespace Ami.AcadM.Tools
{


    class Constants
    {
        public static string strFilename;

        public static string EXPORTPATH = "c:\\temp\\";

        public static string PANELHEIGHT = "100";
        public static string PANELWRT = "";
        public static string PANELORIENTATION = "";
        public static string PANELPOSITION = "E 0 N 0 U 0";
        public static string PANELJUSTIFICATION = "SJUST BOTT";

        public static string EXTRHEIGHT = "100";
        public static string EXTRWRT = "";
        public static string EXTRORIENTATION = "";
        public static string EXTRPOSITION = "E 0 N 0 U 0";

        public static string FLOORHEIGHT = "100";
        public static string FLOORWRT = "";
        public static string FLOORORIENTATION = "";
        public static string FLOORPOSITION = "E 0 N 0 U 0";
        public static string FLOORJUSTIFICATION = "SJUS UTOP";

        public static string NEGEXTRHEIGHT = "100";
        public static string NEGEXTRWRT = "";
        public static string NEGEXTRORIENTATION = "";
        public static string NEGEXTRPOSITION = "E 0 N 0 U 0";

        public static string REVOLUTIONANGLE = "360";
        public static string REVOLUTIONWRT = "";
        public static string REVOLUTIONORIENTATION = "";
        public static string REVOLUTIONPOSITION = "E 0 N 0 U 0";

        public static string NREVOLUTIONANGLE = "360";
        public static string NREVOLUTIONWRT = "";
        public static string NREVOLUTIONORIENTATION = "";
        public static string NREVOLUTIONPOSITION = "E 0 N 0 U 0";

    }

    public class settings
    {
        static public void GetXmlSettings()
        {

            Constants.strFilename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Ami.AcadM.Tools.xml";
            //MessageBox.Show(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            //MessageBox.Show(Constants.strFilename);

            XmlDocument doc = new XmlDocument();
            XmlDocument XmlHandler = new XmlDocument();

            if (File.Exists(Constants.strFilename))
            {
                XmlHandler.Load(Constants.strFilename);
                XmlNode Settings = XmlHandler.SelectSingleNode("/Settings");
                //MessageBox.Show("XmlNode Loaded");

                foreach (XmlNode chldNode in Settings.ChildNodes)
                {
                    //MessageBox.Show(chldNode.Name);
                    if (chldNode.Name == "Path")
                    {
                        Constants.EXPORTPATH = chldNode.Attributes["PATH"].InnerText;
                        //MessageBox.Show(Constants.EXPORTPATH);
                    }

                    if (chldNode.Name == "Panel")
                    {
                        Constants.PANELHEIGHT = chldNode.Attributes["HEIGHT"].InnerText;
                        Constants.PANELWRT = chldNode.Attributes["WRT"].InnerText;
                        Constants.PANELORIENTATION = chldNode.Attributes["ORIENTATION"].InnerText;
                        Constants.PANELPOSITION = chldNode.Attributes["POSITION"].InnerText;
                        Constants.PANELJUSTIFICATION = chldNode.Attributes["JUSTIFICATION"].InnerText;
                        //MessageBox.Show(Constants.PANELHEIGHT);
                    }

                    if (chldNode.Name == "Extrusion")
                    {
                        Constants.EXTRHEIGHT = chldNode.Attributes["HEIGHT"].InnerText;
                        Constants.EXTRWRT = chldNode.Attributes["WRT"].InnerText;
                        Constants.EXTRORIENTATION = chldNode.Attributes["ORIENTATION"].InnerText;
                        Constants.EXTRPOSITION = chldNode.Attributes["POSITION"].InnerText;
                        //MessageBox.Show(Constants.PANELHEIGHT);
                    }

                    if (chldNode.Name == "Floor")
                    {
                        Constants.FLOORHEIGHT = chldNode.Attributes["HEIGHT"].InnerText;
                        Constants.FLOORWRT = chldNode.Attributes["WRT"].InnerText;
                        Constants.FLOORORIENTATION = chldNode.Attributes["ORIENTATION"].InnerText;
                        Constants.FLOORPOSITION = chldNode.Attributes["POSITION"].InnerText;
                        Constants.FLOORJUSTIFICATION = chldNode.Attributes["JUSTIFICATION"].InnerText;
                        //MessageBox.Show(Constants.FLOORHEIGHT);
                    }

                    if (chldNode.Name == "NegExtrusion")
                    {
                        Constants.NEGEXTRHEIGHT = chldNode.Attributes["HEIGHT"].InnerText;
                        Constants.NEGEXTRWRT = chldNode.Attributes["WRT"].InnerText;
                        Constants.NEGEXTRORIENTATION = chldNode.Attributes["ORIENTATION"].InnerText;
                        Constants.NEGEXTRPOSITION = chldNode.Attributes["POSITION"].InnerText;

                        //MessageBox.Show(Constants.NEGEXTRHEIGHT);
                    }

                    if (chldNode.Name == "Revolution")
                    {
                        Constants.REVOLUTIONANGLE = chldNode.Attributes["ANGLE"].InnerText;
                        Constants.REVOLUTIONWRT = chldNode.Attributes["WRT"].InnerText;
                        Constants.REVOLUTIONORIENTATION = chldNode.Attributes["ORIENTATION"].InnerText;
                        Constants.REVOLUTIONPOSITION = chldNode.Attributes["POSITION"].InnerText;

                        //MessageBox.Show(Constants.REVOLUTIONANGLE);
                    }

                    if (chldNode.Name == "NegRevolution")
                    {
                        Constants.NREVOLUTIONANGLE = chldNode.Attributes["ANGLE"].InnerText;
                        Constants.NREVOLUTIONWRT = chldNode.Attributes["WRT"].InnerText;
                        Constants.NREVOLUTIONORIENTATION = chldNode.Attributes["ORIENTATION"].InnerText;
                        Constants.NREVOLUTIONPOSITION = chldNode.Attributes["POSITION"].InnerText;

                        //MessageBox.Show(Constants.REVOLUTIONANGLE);
                    }

                }

            }
            else
            {
                MessageBox.Show("Sorry, no XML-File found, load defaults...");
            }
        }

        public void SetXmlSettings()
        {
            Constants.strFilename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/ami-acad-tools.xml";
            //MessageBox.Show(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            //MessageBox.Show(Constants.strFilename);

            //XmlDocument doc = new XmlDocument();
            XmlDocument XmlHandler = new XmlDocument();

            if (File.Exists(Constants.strFilename))
            {
                XmlHandler.Load(Constants.strFilename);

                //XmlWriter XMLWriteSettings = new XmlWriter;

                XmlNode Settings = XmlHandler.SelectSingleNode("/Settings");
                //MessageBox.Show("XmlNode Loaded");

                foreach (XmlNode chldNode in Settings.ChildNodes)
                {
                    //MessageBox.Show(chldNode.Name);
                    if (chldNode.Name == "Path")
                    {
                        chldNode.Attributes["PATH"].InnerText = Constants.EXPORTPATH;
                        //MessageBox.Show(Constants.EXPORTPATH);
                    }

                    if (chldNode.Name == "Panel")
                    {
                        chldNode.Attributes["HEIGHT"].InnerText = Constants.PANELHEIGHT;
                        chldNode.Attributes["WRT"].InnerText = Constants.PANELWRT;
                        chldNode.Attributes["ORIENTATION"].InnerText = Constants.PANELORIENTATION;
                        chldNode.Attributes["POSITION"].InnerText = Constants.PANELPOSITION;
                        chldNode.Attributes["JUSTIFICATION"].InnerText = Constants.PANELJUSTIFICATION;
                        //MessageBox.Show(Constants.PANELHEIGHT);
                    }

                    if (chldNode.Name == "Extrusion")
                    {
                        chldNode.Attributes["HEIGHT"].InnerText = Constants.EXTRHEIGHT;
                        chldNode.Attributes["WRT"].InnerText = Constants.EXTRWRT;
                        chldNode.Attributes["ORIENTATION"].InnerText = Constants.EXTRORIENTATION;
                        chldNode.Attributes["POSITION"].InnerText = Constants.EXTRPOSITION;
                        //MessageBox.Show(Constants.PANELHEIGHT);
                    }

                    if (chldNode.Name == "Floor")
                    {
                        chldNode.Attributes["HEIGHT"].InnerText = Constants.FLOORHEIGHT;
                        chldNode.Attributes["WRT"].InnerText = Constants.FLOORWRT;
                        chldNode.Attributes["ORIENTATION"].InnerText = Constants.FLOORORIENTATION;
                        chldNode.Attributes["POSITION"].InnerText = Constants.FLOORPOSITION;
                        chldNode.Attributes["JUSTIFICATION"].InnerText = Constants.FLOORJUSTIFICATION;
                        //MessageBox.Show(Constants.FLOORHEIGHT);
                    }

                    if (chldNode.Name == "NegExtrusion")
                    {
                        chldNode.Attributes["HEIGHT"].InnerText = Constants.NEGEXTRHEIGHT;
                        chldNode.Attributes["WRT"].InnerText = Constants.NEGEXTRWRT;
                        chldNode.Attributes["ORIENTATION"].InnerText = Constants.NEGEXTRORIENTATION;
                        chldNode.Attributes["POSITION"].InnerText = Constants.NEGEXTRPOSITION;

                        //MessageBox.Show(Constants.NEGEXTRHEIGHT);
                    }

                    if (chldNode.Name == "Revolution")
                    {
                        chldNode.Attributes["ANGLE"].InnerText = Constants.REVOLUTIONANGLE;
                        chldNode.Attributes["WRT"].InnerText = Constants.REVOLUTIONWRT;
                        chldNode.Attributes["ORIENTATION"].InnerText = Constants.REVOLUTIONORIENTATION;
                        chldNode.Attributes["POSITION"].InnerText = Constants.REVOLUTIONPOSITION;

                        //MessageBox.Show(Constants.REVOLUTIONANGLE);
                    }

                    if (chldNode.Name == "NegRevolution")
                    {
                        chldNode.Attributes["ANGLE"].InnerText = Constants.NREVOLUTIONANGLE;
                        chldNode.Attributes["WRT"].InnerText = Constants.NREVOLUTIONWRT;
                        chldNode.Attributes["ORIENTATION"].InnerText = Constants.NREVOLUTIONORIENTATION;
                        chldNode.Attributes["POSITION"].InnerText = Constants.NREVOLUTIONPOSITION;

                        //MessageBox.Show(Constants.REVOLUTIONANGLE);
                    }

                    //XmlHandler.Save(Constants.strFilename);
                    //XmlHandler.WriteTo();

                }

            }
            else
            {
                MessageBox.Show("Ups, no XML-File found, cannot store values");
            }
        }
    }
}

