﻿/*=================================================*/
/*(c) Acad PDMS Tools by Ambros M. (2012)
/*=================================================*/

using System;
using System.Text;
using System.IO;
using System.Collections;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

using acadApp = Autodesk.AutoCAD.ApplicationServices.Application;


[assembly: CommandClass(typeof(Ami.AcadPdmsAddins.PolyLineExport))]
namespace Ami.AcadPdmsAddins
{
    class PolyLineExport
    {    
        public static string exportPath = @"C:\temp\";
        public static StringBuilder sbData = new StringBuilder();



        public static string HeaderForExport()
        {
            DateTime today = DateTime.Now;
            Database db = HostApplicationServices.WorkingDatabase;

            string expfile = db.Filename.ToString();


            
            StringBuilder header = new StringBuilder();
            header.Append("$* (c) Acad PDMS Tools by Ambros M. (2012)\n");
            header.Append("$* Export out of file: " + expfile + "\n");
            header.Append("$* Time: " + today.ToString("yyyy-MM-dd, HH:mm:ss") + "\n");
            header.Append("$*---------------------------------------------\n\n\n");
            return header.ToString();
        }


        static public void ExportPolyline(Document doc, Database db, Editor ed, StringBuilder sb, string typePrimitive)
        {         
            // set counter
            int cnt = 0;

            PromptEntityOptions opt = new PromptEntityOptions("\nSelect a polyline object: ");

            opt.SetRejectMessage("\nObject must be LWPolyline, Polyline2d or Polyline3d only.");
            opt.AddAllowedClass(typeof(Polyline), false);
            opt.AddAllowedClass(typeof(Polyline2d), false);
            opt.AddAllowedClass(typeof(Polyline3d), false);

            PromptEntityResult res = ed.GetEntity(opt);

            if (res.Status != PromptStatus.OK) return;
            //set precision
            int prec = 0;

            PromptIntegerOptions pio = new PromptIntegerOptions("\nEnter number of decimals <0>: ");
            pio.AllowNone = true;
            PromptIntegerResult pir = ed.GetInteger(pio);

            if (pir.Status != PromptStatus.None && pir.Status != PromptStatus.OK) return;
            if (pir.Status == PromptStatus.OK) prec = pir.Value;

            Transaction tr = db.TransactionManager.StartTransaction();

            using (tr)
            {
                DBObject obj = tr.GetObject(res.ObjectId, OpenMode.ForRead);
                 ed.WriteMessage("\n >>>   {0}", obj.GetRXClass().Name);

                if (obj == null) return;

                switch (obj.GetRXClass().Name)
                {
                    case "AcDbPolyline":
                        {
                            Polyline lwpoly = obj as Polyline;

                            if (lwpoly != null)
                            {
                                for (int i = 0; i < lwpoly.NumberOfVertices; i++)
                                {
                                    cnt += 1;
                                    Point3d pt = lwpoly.GetPoint3dAt(i);
                                    //Point3d pt2 = lwpoly.GetPoint3dAt(i+1);

                                    //double bulge = lwpoly.GetBulgeAt(i);
                                    //double chord = Math.Sqrt(Math.Pow(Math.Abs(pt.X - pt2.X), 2) + Math.Pow(Math.Abs(pt.Y - pt2.Y), 2));
                                    //double s = chord / 2 * bulge;
                                    //double radius = (Math.Pow(chord / 2, 2) + Math.Pow(s, 2)) / (2 * s);

                                    //double frad = ( 4 * Math.Atan(bulge));


                                    ed.WriteMessage("\nX = {0}; Y = {1}; Z = {2}", pt.X, pt.Y, pt.Z);

                                    sb.Append("NEW " + typePrimitive + " POS" 
                                        + " E " +
                                        Math.Round(pt.X, prec).ToString()
                                        + " N " + 
                                        Math.Round(pt.Y, prec).ToString()
                                        + " U " +
                                        Math.Round(pt.Z, prec).ToString()
                                        + " END \n");
                                }
                            }
                            break;
                        }

                    case "AcDb2dPolyline":
                        {
                            Polyline2d poly2d = obj as Polyline2d;

                            if (poly2d != null)
                            {
                                foreach (ObjectId ix in poly2d)
                                {
                                    cnt += 1;

                                    Vertex2d vex = (Vertex2d)ix.GetObject(OpenMode.ForRead);
                                    Point3d pt = vex.Position;
                                    ed.WriteMessage("\nX = {0}; Y = {1}; Z = {2}", pt.X, pt.Y, pt.Z);

                                    sb.Append("NEW " + typePrimitive + " POS"
                                        + " E " +
                                        Math.Round(pt.X, prec).ToString()
                                        + " N " +
                                        Math.Round(pt.Y, prec).ToString()
                                        + " U " +
                                        Math.Round(pt.Z, prec).ToString()
                                        + " END \n");
                                }
                            }
                            break;
                        }

                    case "AcDb3dPolyline":
                        {
                            Polyline3d poly3d = obj as Polyline3d;

                            if (poly3d != null)
                            {
                                foreach (ObjectId ix in poly3d)
                                {
                                    cnt += 1;
                                    PolylineVertex3d vex = (PolylineVertex3d)ix.GetObject(OpenMode.ForRead);
                                    Point3d pt = vex.Position;

                                    ed.WriteMessage("\nX = {0}; Y = {1}; Z = {2}", pt.X, pt.Y, pt.Z);
                                    sb.Append("NEW " + typePrimitive + " POS"
                                        + " E " +
                                        Math.Round(pt.X, prec).ToString()
                                        + " N " +
                                        Math.Round(pt.Y, prec).ToString()
                                        + " U " +
                                        Math.Round(pt.Z, prec).ToString()
                                        + " END \n");
                                }
                            }
                            break;
                        }
                    default:
                        break;
                }
                //sbData.Append(sb.ToString());
                tr.Commit();
            }
        }


        public static int nextFileNumber = 0;

        public static string GetNextFileName(string FileName)
        {
            DateTime today = DateTime.Now;
            //today.ToString("yyyy-MM-dd, HH:mm:ss")
            return String.Format("{0}-{1}-{2}.txt", FileName, today.ToString("yyyyMMdd"), nextFileNumber++);
        }

        static public void ExportAsPanel()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = doc.Editor;
            StringBuilder sb = new StringBuilder();

            string filename = "Panel";
            string typePrimitiv = "PAVERT";

            sb.Append(HeaderForExport());

            sb.Append("NEW PANEL \n");
            sb.Append("POS E 0 N 0 U 0 \n\n");
            sb.Append("NEW PLOOP\n");

            int heig = 100;
            PromptIntegerOptions pio = new PromptIntegerOptions("\nEnter height of panel <100>: ");
            pio.AllowNone = true;
            PromptIntegerResult pir = ed.GetInteger(pio);

            if (pir.Status != PromptStatus.None && pir.Status != PromptStatus.OK) return;
            if (pir.Status == PromptStatus.OK) heig = pir.Value;
            sb.Append("HEIG " + heig.ToString() + "\n");

            sb.Append("SJUST BOTT\n\n");


            ExportPolyline(doc, db, ed, sb, typePrimitiv);
            sb.Append(sbData);

            string FileName = GetNextFileName(filename);
            string FullPath = exportPath + FileName;
            System.IO.StreamWriter sw = new StreamWriter(FullPath, false);

            using (sw)
            {
                sw.Write(sb.ToString());
            }
        }

        static public void ExportAsFloor()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = doc.Editor;
            StringBuilder sb = new StringBuilder();

            string filename = "Floor";
            string typePrimitiv = "PAVERT";

            sb.Append(HeaderForExport());

            sb.Append("NEW FLOOR \n");
            sb.Append("POS E 0 N 0 U 0 \n\n");
            sb.Append("NEW PLOOP\n");

            int heig = 100;
            PromptIntegerOptions pio = new PromptIntegerOptions("\nEnter height of floor <100>: ");
            pio.AllowNone = true;
            PromptIntegerResult pir = ed.GetInteger(pio);

            if (pir.Status != PromptStatus.None && pir.Status != PromptStatus.OK) return;
            if (pir.Status == PromptStatus.OK) heig = pir.Value;
            sb.Append("HEIG " + heig.ToString() + "\n");

            sb.Append("SJUS UTOP\n\n");


            ExportPolyline(doc, db, ed, sb, typePrimitiv);
            sb.Append(sbData);

            string FileName = GetNextFileName(filename);
            string FullPath = exportPath + FileName;
            System.IO.StreamWriter sw = new StreamWriter(FullPath, false);

            using (sw)
            {
                sw.Write(sb.ToString());
            }
        }


        static public void ExportAsRevol()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = doc.Editor;
            StringBuilder sb = new StringBuilder();

            string filename = "Revol";
            string typePrimitiv = "VERTEX";

            sb.Append(HeaderForExport());

            sb.Append("NEW REVOLUTION \n");
            sb.Append("POS E 0 N 0 U 0 \n\n");

            int angle = 360;
            PromptIntegerOptions pio = new PromptIntegerOptions("\nEnter angle of revolution <360>: ");
            pio.AllowNone = true;
            PromptIntegerResult pir = ed.GetInteger(pio);

            if (pir.Status != PromptStatus.None && pir.Status != PromptStatus.OK) return;
            if (pir.Status == PromptStatus.OK) angle = pir.Value;
            sb.Append("ANGLE " + angle.ToString() + "\n");

            //sb.Append("ANGLE 360\n");
            sb.Append("NEW LOOP\n");



            ExportPolyline(doc, db, ed, sb, typePrimitiv);
            sb.Append(sbData);

            string FileName = GetNextFileName(filename);
            string FullPath = exportPath + FileName;
            System.IO.StreamWriter sw = new StreamWriter(FullPath, false);

            using (sw)
            {
                sw.Write(sb.ToString());
            }
        }
    }
}


//Polyline2d 
//List bulges = new List();
// foreach (ObjectId id in pl2d)
// {
// Vertex2d vtx = (Vertex2d)id.GetObject(OpenMode.ForRead);
// bulges.Add(vtx.Bulge); 
//}