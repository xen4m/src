﻿/*=================================================*/
/*(c) Acad PDMS Tools by Ambros M. (2012)
/*=================================================*/

using System;
using System.Text;
using System.IO;
using System.Xml;

using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

using acadApp = Autodesk.AutoCAD.ApplicationServices.Application;




// This line is not mandatory, but improves loading performances
[assembly: ExtensionApplication(typeof(Ami.AcadPdmsAddins.AcadPlugin))]

namespace Ami.AcadPdmsAddins
{
    
    // This class is instantiated by AutoCAD once and kept alive for the 
    // duration of the session. If you don't do any one time initialization 
    // then you should remove this class.
    public class AcadPlugin : IExtensionApplication
    {

        void IExtensionApplication.Initialize()
        {
            // Add one time initialization here
            // One common scenario is to setup a callback function here that 
            // unmanaged code can call. 
            // To do this:
            // 1. Export a function from unmanaged code that takes a function
            //    pointer and stores the passed in value in a global variable.
            // 2. Call this exported function in this function passing delegate.
            // 3. When unmanaged code needs the services of this managed module
            //    you simply call acrxLoadApp() and by the time acrxLoadApp 
            //    returns  global function pointer is initialized to point to
            //    the C# delegate.
            // For more info see: 
            // http://msdn2.microsoft.com/en-US/library/5zwkzwf4(VS.80).aspx
            // http://msdn2.microsoft.com/en-us/library/44ey4b32(VS.80).aspx
            // http://msdn2.microsoft.com/en-US/library/7esfatk4.aspx
            // as well as some of the existing AutoCAD managed apps.

            // Initialize your plug-in application here
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;

            settings.GetXmlSettings();

            ed.WriteMessage("(c) Acad PDMS Tools 1.1.100 by Ambros M. (2013)...loaded\n");
            ed.WriteMessage("Use PDMSEXPPANEL for Panel Export\n");
            ed.WriteMessage("Use PDMSEXPEXTR for Extrusion Export\n");
            ed.WriteMessage("Use PDMSEXPFLOOR for Floor Export\n");
            ed.WriteMessage("Use PDMSEXPREVOL for Revolution Export\n");
            ed.WriteMessage("Use PDMSEXPNEXTR for Negativ Extrusion Export\n");
            //ed.WriteMessage("You can find the export file under temp-folder\n\n");

            ed.WriteMessage("Good Luck !\n");

            

            
        }

        void IExtensionApplication.Terminate()
        {

        }

    }

}
