﻿namespace Ami.AcadPdmsAddins
{
    partial class utilSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonSaveSettings = new System.Windows.Forms.Button();
            this.ButtonSetDefault = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.ButtonBrowse = new System.Windows.Forms.Button();
            this.ButtonExit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.PanelJUStextbox = new System.Windows.Forms.TextBox();
            this.PanelHEItextbox = new System.Windows.Forms.TextBox();
            this.PanelORItextbox = new System.Windows.Forms.TextBox();
            this.PanelPOStextbox = new System.Windows.Forms.TextBox();
            this.PanelWRTtextbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.FloorJUStextbox = new System.Windows.Forms.TextBox();
            this.FloorHEItextbox = new System.Windows.Forms.TextBox();
            this.FloorORItextbox = new System.Windows.Forms.TextBox();
            this.FloorPOStextbox = new System.Windows.Forms.TextBox();
            this.FloorWRTtextbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RevolHEItextbox = new System.Windows.Forms.TextBox();
            this.RevolORItextbox = new System.Windows.Forms.TextBox();
            this.RevolPOStextbox = new System.Windows.Forms.TextBox();
            this.RevolWRTtextbox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.NegExtrHEItextbox = new System.Windows.Forms.TextBox();
            this.NegExtrORItextbox = new System.Windows.Forms.TextBox();
            this.NegExtrPOStextbox = new System.Windows.Forms.TextBox();
            this.NegExtrWRTtextbox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.ExtrusionHEItextbox = new System.Windows.Forms.TextBox();
            this.ExtrusionORItextbox = new System.Windows.Forms.TextBox();
            this.ExtrusionPOStextbox = new System.Windows.Forms.TextBox();
            this.ExtrusionWRTtextbox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonSaveSettings
            // 
            this.ButtonSaveSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonSaveSettings.Location = new System.Drawing.Point(552, 624);
            this.ButtonSaveSettings.Name = "ButtonSaveSettings";
            this.ButtonSaveSettings.Size = new System.Drawing.Size(90, 25);
            this.ButtonSaveSettings.TabIndex = 1;
            this.ButtonSaveSettings.Text = "Save Settings";
            this.ButtonSaveSettings.UseVisualStyleBackColor = true;
            this.ButtonSaveSettings.Click += new System.EventHandler(this.ButtonSaveSettings_Click);
            // 
            // ButtonSetDefault
            // 
            this.ButtonSetDefault.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ButtonSetDefault.Location = new System.Drawing.Point(12, 624);
            this.ButtonSetDefault.Name = "ButtonSetDefault";
            this.ButtonSetDefault.Size = new System.Drawing.Size(90, 25);
            this.ButtonSetDefault.TabIndex = 2;
            this.ButtonSetDefault.Text = "Set Default";
            this.ButtonSetDefault.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Export Path";
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(80, 10);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(450, 20);
            this.textBox2.TabIndex = 4;
            // 
            // ButtonBrowse
            // 
            this.ButtonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonBrowse.Location = new System.Drawing.Point(536, 8);
            this.ButtonBrowse.Name = "ButtonBrowse";
            this.ButtonBrowse.Size = new System.Drawing.Size(106, 23);
            this.ButtonBrowse.TabIndex = 5;
            this.ButtonBrowse.Text = "Browse";
            this.ButtonBrowse.UseVisualStyleBackColor = true;
            this.ButtonBrowse.Click += new System.EventHandler(this.ButtonBrowse_Click);
            // 
            // ButtonExit
            // 
            this.ButtonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonExit.Location = new System.Drawing.Point(456, 624);
            this.ButtonExit.Name = "ButtonExit";
            this.ButtonExit.Size = new System.Drawing.Size(90, 25);
            this.ButtonExit.TabIndex = 6;
            this.ButtonExit.Text = "Exit";
            this.ButtonExit.UseVisualStyleBackColor = true;
            this.ButtonExit.Click += new System.EventHandler(this.ButtonExit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PanelJUStextbox);
            this.groupBox1.Controls.Add(this.PanelHEItextbox);
            this.groupBox1.Controls.Add(this.PanelORItextbox);
            this.groupBox1.Controls.Add(this.PanelPOStextbox);
            this.groupBox1.Controls.Add(this.PanelWRTtextbox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(310, 166);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Panel";
            // 
            // PanelJUStextbox
            // 
            this.PanelJUStextbox.Location = new System.Drawing.Point(100, 120);
            this.PanelJUStextbox.Name = "PanelJUStextbox";
            this.PanelJUStextbox.Size = new System.Drawing.Size(200, 20);
            this.PanelJUStextbox.TabIndex = 9;
            // 
            // PanelHEItextbox
            // 
            this.PanelHEItextbox.Location = new System.Drawing.Point(100, 94);
            this.PanelHEItextbox.Name = "PanelHEItextbox";
            this.PanelHEItextbox.Size = new System.Drawing.Size(200, 20);
            this.PanelHEItextbox.TabIndex = 8;
            // 
            // PanelORItextbox
            // 
            this.PanelORItextbox.Location = new System.Drawing.Point(100, 68);
            this.PanelORItextbox.Name = "PanelORItextbox";
            this.PanelORItextbox.Size = new System.Drawing.Size(200, 20);
            this.PanelORItextbox.TabIndex = 7;
            // 
            // PanelPOStextbox
            // 
            this.PanelPOStextbox.Location = new System.Drawing.Point(100, 42);
            this.PanelPOStextbox.Name = "PanelPOStextbox";
            this.PanelPOStextbox.Size = new System.Drawing.Size(200, 20);
            this.PanelPOStextbox.TabIndex = 6;
            // 
            // PanelWRTtextbox
            // 
            this.PanelWRTtextbox.Location = new System.Drawing.Point(100, 17);
            this.PanelWRTtextbox.Name = "PanelWRTtextbox";
            this.PanelWRTtextbox.Size = new System.Drawing.Size(200, 20);
            this.PanelWRTtextbox.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 123);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Justification";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Height";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Orientation";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Position";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "With Relation To";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.FloorJUStextbox);
            this.groupBox2.Controls.Add(this.FloorHEItextbox);
            this.groupBox2.Controls.Add(this.FloorORItextbox);
            this.groupBox2.Controls.Add(this.FloorPOStextbox);
            this.groupBox2.Controls.Add(this.FloorWRTtextbox);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(12, 381);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(310, 166);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Floor";
            // 
            // FloorJUStextbox
            // 
            this.FloorJUStextbox.Location = new System.Drawing.Point(100, 120);
            this.FloorJUStextbox.Name = "FloorJUStextbox";
            this.FloorJUStextbox.Size = new System.Drawing.Size(200, 20);
            this.FloorJUStextbox.TabIndex = 9;
            // 
            // FloorHEItextbox
            // 
            this.FloorHEItextbox.Location = new System.Drawing.Point(100, 94);
            this.FloorHEItextbox.Name = "FloorHEItextbox";
            this.FloorHEItextbox.Size = new System.Drawing.Size(200, 20);
            this.FloorHEItextbox.TabIndex = 8;
            // 
            // FloorORItextbox
            // 
            this.FloorORItextbox.Location = new System.Drawing.Point(100, 68);
            this.FloorORItextbox.Name = "FloorORItextbox";
            this.FloorORItextbox.Size = new System.Drawing.Size(200, 20);
            this.FloorORItextbox.TabIndex = 7;
            // 
            // FloorPOStextbox
            // 
            this.FloorPOStextbox.Location = new System.Drawing.Point(100, 42);
            this.FloorPOStextbox.Name = "FloorPOStextbox";
            this.FloorPOStextbox.Size = new System.Drawing.Size(200, 20);
            this.FloorPOStextbox.TabIndex = 6;
            // 
            // FloorWRTtextbox
            // 
            this.FloorWRTtextbox.Location = new System.Drawing.Point(100, 17);
            this.FloorWRTtextbox.Name = "FloorWRTtextbox";
            this.FloorWRTtextbox.Size = new System.Drawing.Size(200, 20);
            this.FloorWRTtextbox.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Justification";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Height";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Orientation";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Position";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "With Relation To";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.RevolHEItextbox);
            this.groupBox3.Controls.Add(this.RevolORItextbox);
            this.groupBox3.Controls.Add(this.RevolPOStextbox);
            this.groupBox3.Controls.Add(this.RevolWRTtextbox);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Location = new System.Drawing.Point(332, 37);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(310, 166);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Revolution";
            // 
            // RevolHEItextbox
            // 
            this.RevolHEItextbox.Location = new System.Drawing.Point(100, 94);
            this.RevolHEItextbox.Name = "RevolHEItextbox";
            this.RevolHEItextbox.Size = new System.Drawing.Size(200, 20);
            this.RevolHEItextbox.TabIndex = 8;
            // 
            // RevolORItextbox
            // 
            this.RevolORItextbox.Location = new System.Drawing.Point(100, 68);
            this.RevolORItextbox.Name = "RevolORItextbox";
            this.RevolORItextbox.Size = new System.Drawing.Size(200, 20);
            this.RevolORItextbox.TabIndex = 7;
            // 
            // RevolPOStextbox
            // 
            this.RevolPOStextbox.Location = new System.Drawing.Point(100, 42);
            this.RevolPOStextbox.Name = "RevolPOStextbox";
            this.RevolPOStextbox.Size = new System.Drawing.Size(200, 20);
            this.RevolPOStextbox.TabIndex = 6;
            // 
            // RevolWRTtextbox
            // 
            this.RevolWRTtextbox.Location = new System.Drawing.Point(100, 17);
            this.RevolWRTtextbox.Name = "RevolWRTtextbox";
            this.RevolWRTtextbox.Size = new System.Drawing.Size(200, 20);
            this.RevolWRTtextbox.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 97);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Angle";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 71);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Orientation";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Position";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "With Relation To";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.NegExtrHEItextbox);
            this.groupBox4.Controls.Add(this.NegExtrORItextbox);
            this.groupBox4.Controls.Add(this.NegExtrPOStextbox);
            this.groupBox4.Controls.Add(this.NegExtrWRTtextbox);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Location = new System.Drawing.Point(332, 381);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(310, 166);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Negative Extrusion";
            // 
            // NegExtrHEItextbox
            // 
            this.NegExtrHEItextbox.Location = new System.Drawing.Point(100, 94);
            this.NegExtrHEItextbox.Name = "NegExtrHEItextbox";
            this.NegExtrHEItextbox.Size = new System.Drawing.Size(200, 20);
            this.NegExtrHEItextbox.TabIndex = 8;
            // 
            // NegExtrORItextbox
            // 
            this.NegExtrORItextbox.Location = new System.Drawing.Point(100, 68);
            this.NegExtrORItextbox.Name = "NegExtrORItextbox";
            this.NegExtrORItextbox.Size = new System.Drawing.Size(200, 20);
            this.NegExtrORItextbox.TabIndex = 7;
            // 
            // NegExtrPOStextbox
            // 
            this.NegExtrPOStextbox.Location = new System.Drawing.Point(100, 42);
            this.NegExtrPOStextbox.Name = "NegExtrPOStextbox";
            this.NegExtrPOStextbox.Size = new System.Drawing.Size(200, 20);
            this.NegExtrPOStextbox.TabIndex = 6;
            // 
            // NegExtrWRTtextbox
            // 
            this.NegExtrWRTtextbox.Location = new System.Drawing.Point(100, 17);
            this.NegExtrWRTtextbox.Name = "NegExtrWRTtextbox";
            this.NegExtrWRTtextbox.Size = new System.Drawing.Size(200, 20);
            this.NegExtrWRTtextbox.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 97);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "Height";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 71);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Orientation";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 45);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(44, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Position";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(7, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(87, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "With Relation To";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.ExtrusionHEItextbox);
            this.groupBox5.Controls.Add(this.ExtrusionORItextbox);
            this.groupBox5.Controls.Add(this.ExtrusionPOStextbox);
            this.groupBox5.Controls.Add(this.ExtrusionWRTtextbox);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Location = new System.Drawing.Point(15, 209);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(310, 166);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Extrusion";
            // 
            // ExtrusionHEItextbox
            // 
            this.ExtrusionHEItextbox.Location = new System.Drawing.Point(100, 94);
            this.ExtrusionHEItextbox.Name = "ExtrusionHEItextbox";
            this.ExtrusionHEItextbox.Size = new System.Drawing.Size(200, 20);
            this.ExtrusionHEItextbox.TabIndex = 8;
            // 
            // ExtrusionORItextbox
            // 
            this.ExtrusionORItextbox.Location = new System.Drawing.Point(100, 68);
            this.ExtrusionORItextbox.Name = "ExtrusionORItextbox";
            this.ExtrusionORItextbox.Size = new System.Drawing.Size(200, 20);
            this.ExtrusionORItextbox.TabIndex = 7;
            // 
            // ExtrusionPOStextbox
            // 
            this.ExtrusionPOStextbox.Location = new System.Drawing.Point(100, 42);
            this.ExtrusionPOStextbox.Name = "ExtrusionPOStextbox";
            this.ExtrusionPOStextbox.Size = new System.Drawing.Size(200, 20);
            this.ExtrusionPOStextbox.TabIndex = 6;
            // 
            // ExtrusionWRTtextbox
            // 
            this.ExtrusionWRTtextbox.Location = new System.Drawing.Point(100, 17);
            this.ExtrusionWRTtextbox.Name = "ExtrusionWRTtextbox";
            this.ExtrusionWRTtextbox.Size = new System.Drawing.Size(200, 20);
            this.ExtrusionWRTtextbox.TabIndex = 5;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 97);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "Height";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 71);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(58, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "Orientation";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 45);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Position";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 20);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "With Relation To";
            // 
            // utilSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 662);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ButtonExit);
            this.Controls.Add(this.ButtonBrowse);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ButtonSetDefault);
            this.Controls.Add(this.ButtonSaveSettings);
            this.MaximumSize = new System.Drawing.Size(670, 700);
            this.MinimumSize = new System.Drawing.Size(670, 700);
            this.Name = "utilSettings";
            this.Text = "Settings for Acad Tools";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonSaveSettings;
        private System.Windows.Forms.Button ButtonSetDefault;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button ButtonBrowse;
        private System.Windows.Forms.Button ButtonExit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PanelJUStextbox;
        private System.Windows.Forms.TextBox PanelHEItextbox;
        private System.Windows.Forms.TextBox PanelORItextbox;
        private System.Windows.Forms.TextBox PanelPOStextbox;
        private System.Windows.Forms.TextBox PanelWRTtextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox FloorJUStextbox;
        private System.Windows.Forms.TextBox FloorHEItextbox;
        private System.Windows.Forms.TextBox FloorORItextbox;
        private System.Windows.Forms.TextBox FloorPOStextbox;
        private System.Windows.Forms.TextBox FloorWRTtextbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox RevolHEItextbox;
        private System.Windows.Forms.TextBox RevolORItextbox;
        private System.Windows.Forms.TextBox RevolPOStextbox;
        private System.Windows.Forms.TextBox RevolWRTtextbox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox NegExtrHEItextbox;
        private System.Windows.Forms.TextBox NegExtrORItextbox;
        private System.Windows.Forms.TextBox NegExtrPOStextbox;
        private System.Windows.Forms.TextBox NegExtrWRTtextbox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox ExtrusionHEItextbox;
        private System.Windows.Forms.TextBox ExtrusionORItextbox;
        private System.Windows.Forms.TextBox ExtrusionPOStextbox;
        private System.Windows.Forms.TextBox ExtrusionWRTtextbox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
    }
}