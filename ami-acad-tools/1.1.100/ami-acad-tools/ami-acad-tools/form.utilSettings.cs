﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Ami.AcadPdmsAddins
{
    public partial class utilSettings : Form
    {
        public utilSettings()
        {
            InitializeComponent();

            this.textBox2.Text = Constants.EXPORTPATH;

            this.PanelHEItextbox.Text = Constants.PANELHEIGHT;
            this.PanelWRTtextbox.Text = Constants.PANELWRT;
            this.PanelORItextbox.Text = Constants.PANELORIENTATION;
            this.PanelPOStextbox.Text = Constants.PANELPOSITION;
            this.PanelJUStextbox.Text = Constants.PANELJUSTIFICATION;

            this.ExtrusionHEItextbox.Text = Constants.EXTRHEIGHT;
            this.ExtrusionWRTtextbox.Text = Constants.EXTRWRT;
            this.ExtrusionORItextbox.Text = Constants.EXTRORIENTATION;
            this.ExtrusionPOStextbox.Text = Constants.EXTRPOSITION;

            this.FloorHEItextbox.Text = Constants.FLOORHEIGHT;
            this.FloorWRTtextbox.Text = Constants.FLOORWRT;
            this.FloorORItextbox.Text = Constants.FLOORORIENTATION;
            this.FloorPOStextbox.Text = Constants.FLOORPOSITION;
            this.FloorJUStextbox.Text = Constants.FLOORJUSTIFICATION;

            this.RevolHEItextbox.Text = Constants.REVOLUTIONANGLE;
            this.RevolWRTtextbox.Text = Constants.REVOLUTIONWRT;
            this.RevolORItextbox.Text = Constants.REVOLUTIONORIENTATION;
            this.RevolPOStextbox.Text = Constants.REVOLUTIONPOSITION;

            this.NegExtrHEItextbox.Text = Constants.NEGEXTRHEIGHT;
            this.NegExtrWRTtextbox.Text = Constants.NEGEXTRWRT;
            this.NegExtrORItextbox.Text = Constants.NEGEXTRORIENTATION;
            this.NegExtrPOStextbox.Text = Constants.NEGEXTRPOSITION;

                    //public static string EXPORTPATH = "c:\\temp\\";

                    //public static string PANELHEIGHT = "100";
                    //public static string PANELWRT = "";
                    //public static string PANELORIENTATION = "";
                    //public static string PANELPOSITION = "E 0 N 0 U 0";
                    //public static string PANELJUSTIFICATION = "SJUST BOTT";

                    //public static string FLOORHEIGHT = "100";
                    //public static string FLOORWRT = "";
                    //public static string FLOORORIENTATION = "";
                    //public static string FLOORPOSITION = "E 0 N 0 U 0";
                    //public static string FLOORJUSTIFICATION = "SJUS UTOP";

                    //public static string NEGEXTRHEIGHT = "100";
                    //public static string NEGEXTRWRT = "";
                    //public static string NEGEXTRORIENTATION = "";
                    //public static string NEGEXTRPOSITION = "E 0 N 0 U 0";

                    //public static string REVOLUTIONANGLE = "360";
                    //public static string REVOLUTIONWRT = "";
                    //public static string REVOLUTIONORIENTATION = "";
                    //public static string REVOLUTIONPOSITION = "E 0 N 0 U 0";

        }

        private void ButtonBrowse_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                this.textBox2.Text = folderBrowserDialog1.SelectedPath;
                //Constants.EXPORTPATH = textBox2.Text;
            }
        }

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonSaveSettings_Click(object sender, EventArgs e)
        {
            Constants.EXPORTPATH = this.textBox2.Text;

            Constants.PANELHEIGHT           = this.PanelHEItextbox.Text;
            Constants.PANELWRT              = this.PanelWRTtextbox.Text;
            Constants.PANELORIENTATION      = this.PanelORItextbox.Text; 
            Constants.PANELPOSITION         = this.PanelPOStextbox.Text;
            Constants.PANELJUSTIFICATION    = this.PanelJUStextbox.Text;

            Constants.EXTRHEIGHT            = this.ExtrusionHEItextbox.Text;
            Constants.EXTRWRT               = this.ExtrusionWRTtextbox.Text;
            Constants.EXTRORIENTATION        = this.ExtrusionORItextbox.Text;


            Constants.FLOORHEIGHT          = this.FloorHEItextbox.Text;  
            Constants.FLOORWRT             = this.FloorWRTtextbox.Text;
            Constants.FLOORORIENTATION     = this.FloorORItextbox.Text; 
            Constants.FLOORPOSITION        = this.FloorPOStextbox.Text; 
            Constants.FLOORJUSTIFICATION   = this.FloorJUStextbox.Text;  

            Constants.REVOLUTIONANGLE        = this.RevolHEItextbox.Text;  
            Constants.REVOLUTIONWRT          = this.RevolWRTtextbox.Text; 
            Constants.REVOLUTIONORIENTATION  = this.RevolORItextbox.Text;
            Constants.REVOLUTIONPOSITION     = this.RevolPOStextbox.Text;

            Constants.NEGEXTRHEIGHT         = this.NegExtrHEItextbox.Text;
            Constants.NEGEXTRWRT            = this.NegExtrWRTtextbox.Text;
            Constants.NEGEXTRORIENTATION    = this.NegExtrORItextbox.Text;
            Constants.NEGEXTRPOSITION       = this.NegExtrPOStextbox.Text;

            MessageBox.Show("Values are stored to XML-File");
        }

    }
}
