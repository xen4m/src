﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.AcInfoCenterConn;
using Autodesk.AutoCAD.Windows;



namespace Ami.AcadPdmsAddins
{
    public class notifcation
    {
        public class Commands
        {
            const string title =
              "PDMS Export";
            const string hlink =
              "file://c:\\temp\\";
            const string htext =
              "Link to Export File";
            const string msg =
              "Export has been created under Temp-Folder";
            const string msg2 =
              "Some additional text";


            //[CommandMethod("sbb")]

            public void statusBarBalloon()
            {
                const string appName =
                  "PDMS Applications";

                Document doc =
                  Application.DocumentManager.MdiActiveDocument;

                TrayItem ti = new TrayItem();
                ti.ToolTipText = appName;
                //ti.Icon = doc.StatusBar.TrayItems[0].Icon;

                Application.StatusBar.TrayItems.Add(ti);

                TrayItemBubbleWindow bw = new TrayItemBubbleWindow();
                bw.Title = title;
                bw.HyperText = htext;
                bw.HyperLink = hlink;
                bw.Text = msg;
                bw.Text2 = msg2;
                //bw.IconType = IconType.Information;
                //null // Don't provide an icon

                ti.ShowBubbleWindow(bw);
                Application.StatusBar.Update();

                bw.Closed +=
                  delegate(
                    object o,
                    TrayItemBubbleWindowClosedEventArgs args
                  )
                  {
                      // Use a try-catch block, as an exception
                      // will occur when AutoCAD is closed with
                      // one of our bubbles open
                      try
                      {
                          Application.StatusBar.TrayItems.Remove(ti);
                          Application.StatusBar.Update();
                      }
                      catch
                      { }
                  };
            }
        }

    }
}
