﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;

namespace OutlookAddInTest
{
    public partial class ThisAddIn
    {
        private Microsoft.Office.Interop.Outlook.Application applicationObject;

        private object addInInstance;

        private Office.CommandBarButton toolbarButton;

        public void OnConnection(object application, Extensibility.ext_ConnectMode connectMode, object addInInst, ref System.Array custom)
        {

            applicationObject = (Microsoft.Office.Interop.Outlook.Application)application;

            addInInstance = addInInst;



            if (connectMode != Extensibility.ext_ConnectMode.ext_cm_Startup)
            {

                OnStartupComplete(ref custom);

            }

        }

        public void OnDisconnection(Extensibility.ext_DisconnectMode disconnectMode, ref System.Array custom)
        {

            if (disconnectMode != Extensibility.ext_DisconnectMode.ext_dm_HostShutdown)
            {

                OnBeginShutdown(ref custom);

            }

            applicationObject = null;

        }

        public void OnStartupComplete(ref System.Array custom)
        {

            Office.CommandBars commandBars = applicationObject.ActiveExplorer().CommandBars;



            // Create a toolbar button on the standard toolbar that calls ToolbarButton_Click when clicked

            try
            {

                // See if it already exists

                this.toolbarButton = (Office.CommandBarButton)commandBars["Standard"].Controls["Hello"];

            }

            catch (Exception)
            {

                // Create it

                this.toolbarButton = (Office.CommandBarButton)commandBars["Standard"].Controls.Add(1, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value);

                this.toolbarButton.Caption = "Hello";

                this.toolbarButton.Style = Office.MsoButtonStyle.msoButtonCaption;

            }

            this.toolbarButton.Tag = "Hello Button";

            this.toolbarButton.OnAction = "!<MyAddin1.Connect>";

            this.toolbarButton.Visible = true;

            this.toolbarButton.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(this.OnToolbarButtonClick);

        }

        public void OnBeginShutdown(ref System.Array custom)
        {

            this.toolbarButton.Delete(System.Reflection.Missing.Value);

            this.toolbarButton = null;

        }

        private void OnToolbarButtonClick(Office.CommandBarButton cmdBarbutton, ref bool cancel)
        {

            System.Windows.Forms.MessageBox.Show("Hello World", "My Addin");

        }

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("Add-In is working", "My Addin");
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region Von VSTO generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }

}
