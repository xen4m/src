﻿/*  ----------------------------------------------------------------------------
 *  Ambros Markus Ing.
 *  ----------------------------------------------------------------------------
 *  Draft Purpose for PDMS Design
 *  ----------------------------------------------------------------------------
 *  File:           DraPurpControl.cs
 *  Author:         Markus Ambros
 *  Initial Date:   01.06.2010
 *  ----------------------------------------------------------------------------
 *  Version:        PDMS 12 SP5.2
 *  Namespace:      Ami.Design.DraPurpAddin
 *  ----------------------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Aveva.ApplicationFramework;
using Aveva.ApplicationFramework.Presentation;
using Aveva.Pdms.Shared;
using Aveva.Pdms.Utilities.Messaging;
using Aveva.Pdms.Database;
using Aveva.PDMS.Database.Filters;
using ATT = Aveva.Pdms.Database.DbAttributeInstance;
using NOUN = Aveva.Pdms.Database.DbElementTypeInstance;
using Ps = Aveva.Pdms.Database.DbPseudoAttribute;

namespace Ami.Design.DraPurpAddin
{       
    
    public partial class DraPurpControl : UserControl
    {
        public DbElement ceName;

        public DraPurpControl()
        {
            InitializeComponent();

            comboBoxPurpose.Items.Add("unset");
            comboBoxPurpose.Items.Add("CIVIL");
            comboBoxPurpose.Items.Add("CIVIL-CENL");
            comboBoxPurpose.Items.Add("CIVIL-AUXILIARY");
            comboBoxPurpose.Items.Add("EQUI");
            comboBoxPurpose.Items.Add("EQUI-ANDRITZ");
            comboBoxPurpose.Items.Add("EQUI-STRU");
            comboBoxPurpose.Items.Add("PIPING");
            comboBoxPurpose.Items.Add("PIPING-EXIST");
            comboBoxPurpose.Items.Add("EXISTING");
            comboBoxPurpose.Items.Add("GROUTING");
            comboBoxPurpose.Items.Add("FOUNDATION");
            comboBoxPurpose.Items.Add("STEELWORK");
            comboBoxPurpose.Items.Add("SUPPORT");
            comboBoxPurpose.Items.Add("GRATING");
            comboBoxPurpose.Items.Add("ELECTRIC");
            comboBoxPurpose.Items.Add("CABLE");
            comboBoxPurpose.Items.Add("HVAC");
        }

        public void ListViewClear()
        {
            this.listViewZones.Items.Clear();
        }

        public void AddZones(string name, string value)
        { 
            listViewZones.Items.Add(new ListViewItem(new string[2] { name, value }));
        }

        private void buttonGetCE_Click(object sender, EventArgs e)
        {
            textBox1.Clear();

            string ce = CurrentElement.Element.GetAsString(DbAttributeInstance.NAME);
            this.textBox1.Text = ce;
            ceName = CurrentElement.Element;
            DraPurpCollections.Run(ceName, this);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ListViewClear();
        }

        private void buttonSet_Click(object sender, EventArgs e)
        {
            string selectedDraPurp = comboBoxPurpose.SelectedItem.ToString();

            foreach (ListViewItem lvi in listViewZones.SelectedItems)
            {
                DbAttribute uda = DbAttribute.GetDbAttribute(":DRAPURP");
                string zone = lvi.SubItems[0].Text;
                DbElement DbZone = DbElement.GetElement(zone);
                DbZone.SetAttribute(uda, selectedDraPurp);
                lvi.SubItems[1].Text = selectedDraPurp;
            }
        }
    }
}