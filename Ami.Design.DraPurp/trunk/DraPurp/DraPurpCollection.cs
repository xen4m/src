﻿/*  ----------------------------------------------------------------------------
 *  Ambros Markus Ing.
 *  ----------------------------------------------------------------------------
 *  Draft Purpose for PDMS Design
 *  ----------------------------------------------------------------------------
 *  File:           DraPurpCollection.cs
 *  Author:         Markus Ambros
 *  Initial Date:   01.06.2010
 *  ----------------------------------------------------------------------------
 *  Version:        PDMS 12 SP5.2
 *  Namespace:      Ami.Design.DraPurpAddin
 *  ----------------------------------------------------------------------------
 */

using System;
using System.Text;
using System.Windows.Forms;
using Aveva.Pdms.Database;
using Aveva.PDMS.Database.Filters;
using ATT = Aveva.Pdms.Database.DbAttributeInstance;
using NOUN = Aveva.Pdms.Database.DbElementTypeInstance;
using Ps = Aveva.Pdms.Database.DbPseudoAttribute;

namespace Ami.Design.DraPurpAddin
{
    public class DraPurpCollections
    {
        public static DraPurpCollections Instance = new DraPurpCollections();

        //Create and iterate through collections
        public static void Run(DbElement mySite, DraPurpControl DraPurpControl)
        {
            DraPurpControl.ListViewClear();

            //Scan Zones below Sites
            TypeFilter filter = new TypeFilter(NOUN.ZONE);
            DBElementCollection collection = new DBElementCollection(mySite, filter);
            DbAttribute att = DbAttributeInstance.NAME;
            DbAttribute uda = DbAttribute.GetDbAttribute(":DRAPURP");
            
            foreach (DbElement ele in collection)
            {
                DraPurpControl.AddZones(ele.GetAsString(att), ele.GetAsString(uda));              
           } 
        }        
    }
}


