﻿namespace Ami.Design.DraPurpAddin
{
    partial class DraPurpControl
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGetCE = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listViewZones = new System.Windows.Forms.ListView();
            this.Zones = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DraPurpOfZones = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonClear = new System.Windows.Forms.Button();
            this.comboBoxPurpose = new System.Windows.Forms.ComboBox();
            this.buttonSet = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonGetCE
            // 
            this.buttonGetCE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGetCE.Location = new System.Drawing.Point(277, 3);
            this.buttonGetCE.Name = "buttonGetCE";
            this.buttonGetCE.Size = new System.Drawing.Size(64, 20);
            this.buttonGetCE.TabIndex = 1;
            this.buttonGetCE.Text = "Get CE";
            this.buttonGetCE.UseVisualStyleBackColor = true;
            this.buttonGetCE.Click += new System.EventHandler(this.buttonGetCE_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(30, 3);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(241, 20);
            this.textBox1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "CE";
            // 
            // listViewZones
            // 
            this.listViewZones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewZones.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Zones,
            this.DraPurpOfZones});
            this.listViewZones.FullRowSelect = true;
            this.listViewZones.GridLines = true;
            this.listViewZones.HideSelection = false;
            this.listViewZones.Location = new System.Drawing.Point(6, 29);
            this.listViewZones.Name = "listViewZones";
            this.listViewZones.Size = new System.Drawing.Size(407, 331);
            this.listViewZones.TabIndex = 6;
            this.listViewZones.UseCompatibleStateImageBehavior = false;
            this.listViewZones.View = System.Windows.Forms.View.Details;
            // 
            // Zones
            // 
            this.Zones.Text = "Zones";
            this.Zones.Width = 164;
            // 
            // DraPurpOfZones
            // 
            this.DraPurpOfZones.Text = "Draft Purpose";
            this.DraPurpOfZones.Width = 216;
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonClear.Location = new System.Drawing.Point(347, 3);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(64, 20);
            this.buttonClear.TabIndex = 7;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // comboBoxPurpose
            // 
            this.comboBoxPurpose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxPurpose.FormattingEnabled = true;
            this.comboBoxPurpose.Location = new System.Drawing.Point(6, 366);
            this.comboBoxPurpose.Name = "comboBoxPurpose";
            this.comboBoxPurpose.Size = new System.Drawing.Size(334, 21);
            this.comboBoxPurpose.TabIndex = 10;
            this.comboBoxPurpose.Text = "unset";
            // 
            // buttonSet
            // 
            this.buttonSet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSet.Location = new System.Drawing.Point(347, 365);
            this.buttonSet.Name = "buttonSet";
            this.buttonSet.Size = new System.Drawing.Size(66, 21);
            this.buttonSet.TabIndex = 11;
            this.buttonSet.Text = "Set";
            this.buttonSet.UseVisualStyleBackColor = true;
            this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
            // 
            // DraPurpControl
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.comboBoxPurpose);
            this.Controls.Add(this.buttonGetCE);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listViewZones);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonSet);
            this.Name = "DraPurpControl";
            this.Size = new System.Drawing.Size(438, 453);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGetCE;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView listViewZones;
        private System.Windows.Forms.ColumnHeader Zones;
        private System.Windows.Forms.ColumnHeader DraPurpOfZones;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.ComboBox comboBoxPurpose;
        private System.Windows.Forms.Button buttonSet;



    }
}
